<?php

class proxy {

    var $id;
    var $ip;
    var $port;
    var $DB;

    function __construct(&$DB, $bInitRandom = true) {
        $this->DB = $DB;

        if ($bInitRandom) {
            $this->getRandom();
        }
    }

    function setProxy($id, $ip, $port) {
        $this->id = $id;
        $this->ip = $ip;
        $this->port = $port;
    }

    function getRandom($beforeUT=false) {
//        $res = $this->DB->select("SELECT id, ip, port FROM parser_proxy WHERE active='yes' AND used_last IS NULL ORDER BY RAND() LIMIT 1");
        $res = $this->DB->select("SELECT id, ip, port FROM parser_proxy WHERE active='yes' AND del='no' AND used_last IS NULL ORDER BY ut ASC LIMIT 1");
        if (!count($res)) {
            $utcheck = $beforeUT !== false ? " AND used_last <= '".$beforeUT."' " : "";
//            $res = $this->DB->select("SELECT id, ip, port FROM parser_proxy WHERE active='yes' ORDER BY RAND() LIMIT 1");
//            echo "SELECT id, ip, port FROM parser_proxy WHERE active='yes' AND del='no' ".$utcheck." ORDER BY used_last ASC LIMIT 1";
            $res = $this->DB->select("SELECT id, ip, port FROM parser_proxy WHERE active='yes' AND del='no' ".$utcheck." ORDER BY used_last ASC LIMIT 1");
            if (!count($res)) {
                die("NO ACTIVE PROXIES!!!\n");
            }
        }
        $this->setProxy($res[0]['id'], $res[0]['ip'], $res[0]['port']);
    }

    function setLastUsed() {
        $this->DB->query("UPDATE parser_proxy SET used_last=NOW() WHERE id=" . $this->id);
    }

    function checkAvailability() {
        if (!$this->id) {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.google.com");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, $this->ip . ":" . $this->port);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $content = curl_exec($ch);
        curl_close($ch);

        if (stripos($content, "google") !== false) {
            $this->DB->query("UPDATE parser_proxy SET active='yes', checked_last=NOW(), ut=NOW() WHERE id=" . $this->id);
            return true;
        } else {
            $this->DB->query("UPDATE parser_proxy SET active='no', error_count=error_count+1, checked_last=NOW(), ut=NOW() WHERE id=" . $this->id);
            return false;
        }
    }

    function markInactive() {
        $this->DB->query("UPDATE parser_proxy SET active='no', error_count=error_count+1, checked_last=NOW(), ut=NOW() WHERE id=" . $this->id);
    }

    function getCURLData($url, $host=false, $referer=false, $cookie=false, $postparams = false) {
        GLOBAL $proxy;

        
        
        $headers = array(
            'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.17) Gecko/20110121 Firefox/3.5.17 ( .NET CLR 3.5.30729)',
            'Accept	text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: en-us,en;q=0.5',
            'Accept-Encoding: gzip,deflate',
            'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Keep-Alive: 300',
            'Connection: keep-alive',
            'X-lori-time-1: ' . microtime(),
            'Cache-Control: max-age=0'
        );
        if ($host) {
            $headers[] = 'Host: ' . $host;
        }
        if ($referer) {
            $headers[] = 'Referer: ' . $host;
        }
        if ($cookie) {
            $headers[] = 'Cookie: ' . $host;
        }

//	$postparams = array('qid' => $iQuizNumber);
//	if (isset($params) && count($params)) {
//		$postparams = array_merge($postparams, $params);
//	}
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        //curl_setopt($ch, CURLOPT_PROXYPORT, "80");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($postparams) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postparams);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $iTryCount = 0;
        do {
            if ($iTryCount) {
                $this->setLastUsed();
                $this->markInactive();
                echo " - <b>failed</b> (" . $url . ")<br>";
                if ($iTryCount == 10) {
                    return false;
                }
            }
            $this->getRandom();
            curl_setopt($ch, CURLOPT_PROXY, $this->ip . ":" . $this->port);
            echo "Proxy: " . $this->id . ". " . $this->ip . ":" . $this->port ;
            $data = curl_exec($ch);
            $iTryCount++;
        } while (!strlen($data));
        echo " - <b>success</b> (" . $url . ")\n";

        curl_close($ch);

        return $data;
    }

}

?>