window.timer_classes = 'col-sm-3 col-xs-6';
$(function () {
    var endDate = "July 20, 2016 10:30:00";

    $('.countdown.simple').countdown({date: endDate});

    $('.countdown.styled').countdown({
        date: endDate,
        render: function (data) {
            $(this.el).html("<div><div class='row'><div class='"+window.timer_classes+"'>" + this.leadingZeros(data.days, 3) + " <span>дни</span></div><div class='"+window.timer_classes+"'>" + this.leadingZeros(data.hours, 2) + " <span>часа</span></div><div class='"+window.timer_classes+"'>" + this.leadingZeros(data.min, 2) + " <span>минути</span></div><div class='"+window.timer_classes+"'>" + this.leadingZeros(data.sec, 2) + " <span>секунди</span></div></div></div>");
        }
    });

    $('.countdown.callback').countdown({
        date: +(new Date) + 10000,
        render: function (data) {
            $(this.el).text(this.leadingZeros(data.sec, 2) + " sec");
        },
        onEnd: function () {
            $(this.el).addClass('ended');
        }
    }).on("click", function () {
        $(this).removeClass('ended').data('countdown').update(+(new Date) + 10000).start();
    });



});
   