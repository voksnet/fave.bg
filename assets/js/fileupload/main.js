/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
//http://www.fashiongonerogue.com/wp-content/uploads/2015/10/Toni-Garrn-HM-Lingerie-Pictures02.jpg
//http://stellyclothing.new.s3-website-ap-southeast-2.amazonaws.com/img/p/9/9/9/3/8/99938-thickbox.jpg
function showStandartImage() {
	$(".upload_request_holder .fileinput-button").removeClass("disabled");
	$(".upload_request_holder .fileinput-button input").attr("disabled", false);
	$(".upload_request_holder .upload_form_next").hide();
	$(".upload_request_holder .web_image_preview").html("");
	$("#web_image").removeClass("success");
	//$("#web_image_label").show();
	web_image = false;
	url_processing = false;
}
function resetServerError() {
	$("#server_error").html("");
	$("#server_error").hide();
}
function showRemoteImage(url) {
	if (url !== undefined && url != "") {
		var ajax_result;
		$.post({
			url: baseUrl + upload_url + '/remote_filesize',
			dataType: 'json',
			async: false,
			data: {"url": url},
			success: function (data) {
				ajax_result = data;
			}
		});
		if (ajax_result !== undefined && ajax_result.error === true) {
			showStandartImage();
			$(".upload_request_holder .web_image_error").html(ajax_result.error_message);
		} else if (ajax_result !== undefined) {
			$("<img/>", {
				load: function () {
					if (this.height >= my_imageupload_min_h && this.width >= my_imageupload_min_w) {
						$(".upload_request_holder .fileinput-button").addClass("disabled");
						$(".upload_request_holder .fileinput-button input").attr("disabled", true);
						resetServerError();
						$(".upload_request_holder .upload_form_next").show();
						$(".upload_request_holder .web_image_preview").html(this);
						$("#web_image").addClass("success");
						$(".upload_request_holder .web_image_error").html("");
						//$("#web_image_label").hide();
						web_image = true;
					} else {
						$(".upload_request_holder .web_image_error").html(langLine.fe_imageupload_small_image);
						showStandartImage();
					}
				},
				error: function () {
					showStandartImage();
					$(".upload_request_holder .web_image_error").html(langLine.fe_imgupload_invalid_url);
				},
				src: url
			});
		}
		url_processing = false;
	} else {
		showStandartImage();
	}

}



function process_remote_images(images) {
	var shown = false;
	if (images !== undefined && images.length > 0) {
		clearTimeout(stop_load);
		for (var i = 0; i < images.length; i++) {
			var img = $('<img id="tip_img_' + i + '">');
			img.attr('src', images[i]);
			img.attr('sequence', i);
			img.appendTo('.tip_images_store_img');
			$("#tip_img_" + i).on("load", function (e) {
				var that = e.currentTarget;
				processed++;
				$("#" + that.id).off("load");
				if (that.width >= my_imageupload_min_w && that.height >= my_imageupload_min_h) {
					$("#" + that.id).clone().appendTo(".tip_images_img");
					sequences.push($("#" + that.id).attr("sequence"));
					if (!shown) {
						$('.tip_images').removeClass("loading");
						$(".upload_image_selector .chouse").css("display", "inline-block");
						shown = true;
						current_sequence = sequences.length - 1;
						rotateSequences();
					}
				}
				$(".tip_images_store_img #" + that.id).remove();
				$(".total_sequences .total").html(sequences.length);
				clearTimeout(stop_load);
				if (!shown) {
					stop_load = setTimeout(function () {
						resetRemoteImages();
						$(".web_image_error").html(langLine.fe_imgupload_invalid_url_site);
					}, 5000);
				}
				if (processed == images.length) {
					clearTimeout(stop_load);
					$('.tip_images').removeClass("loading");
				}

//				showStandartImage();

			});

		}
	} else {
		resetRemoteImages();
	}
}

function rotateSequences() {
	$(".total_sequences .current").html(current_sequence + 1);
	$(".tip_images_img img").hide();
	$(".tip_images .tip_images_img img").eq(current_sequence).show();
	$(".tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
}

function rotateSequencesNext() {
	if (sequences.length > 0) {
		if (current_sequence == sequences.length - 1) {
			current_sequence = 0;
		} else {
			current_sequence++;
		}
		$(".total_sequences .current").html(current_sequence + 1);
		$(".tip_images_img img").hide();
		$(".tip_images_img img").eq(current_sequence).show();
		$(".tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
	}
}

function rotateSequencesPrevious() {
	if (sequences.length > 0) {
		if (current_sequence == 0) {
			current_sequence = sequences.length - 1;
		} else {
			current_sequence--;
		}
		$(".total_sequences .current").html(current_sequence + 1);
		$(".tip_images_img img").hide();
		$(".tip_images_img img").eq(current_sequence).show();
		$(".tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
	}
}

function resetRemoteImages() {
	$('.tip_images .right_sequence').off("click");
	$('.tip_images .left_sequence').off("click");
	$(".tip_images_img").html("");
	$(".tip_images_store_img").html("");
	$(".upload_image_selector").hide();
	$(".upload_image_selector .chouse").css("display", "none");
	$(".tip_images .current").html("0");
	$(".tip_images .total").html("0");
	$(".post_upload_btn").show();
	$(".ajax-loading-image").hide();
	sequences = [];
	processed = 0;
	clearTimeout(stop_load);
	$(".tip_images_img img").off("load");
	processing_url = "";
}

/* global $, window */
var form_data = "";
var web_image = false;
var last_image_url = "";
var canvasElement;
var data_to_post;
var disabledUpload = false;
var url_processing = false;
var processed = 0;
var sequences = [];
var current_sequence = 0;
var stop_load;

function getTags() {
	var tagsVals = [];
	$.each($(".tagit-choice"), function (index, value) {
		var tagValue = $(".tagit-choice").eq(index).find(".tagit-label").html();
		if (isUrlValid(tagValue)) {
			$(".tags_error").html(langLine.fe_imageupload_tag_link);
			submitError = true;
		} else if (tagValue.length < my_min_tag_length) {
			$(".tags_error").html(langLine.fe_upload_short_tag);
			submitError = true;
		} else if (tagValue.length > my_max_tag_length) {
			$(".tags_error").html(langLine.fe_upload_long_tag);
			submitError = true;
		}
		if (tagValue !== undefined) {
			tagsVals.push(tagValue)
		}
	});
	if (tagsVals.length > 0) {
		if (tagsVals.length > my_max_tags) {
			$(".tags_error").html(langLine.fe_upload_too_many_tags);
			return true;
		} else {
			$(".tags-selector").val(tagsVals);
		}
	} else {
		$(".tags_error").html(langLine.fe_imageupload_tag_none);
		return true;
	}
	return tagsVals.toString();
}

function censorship(text, selector, message) {
	//right async
	var result = false;
	if (cs) {
		var jsonProcess = function (callback) {
			$.ajax({
				url: 'censor.php',
				dataType: 'json',
				async: false,
				data: {text: text},
				success: callback
			});
		};
		jsonProcess(function (data) {
			if (data.found) {
				$(selector).html(message);
				result = data.found;
			}
		});
	}
	return result;
}

function showErrors() {
}

function disableUpload() {
	$(".btn-primary.start").addClass("disabled");
	disabledUpload = true;
}

function enableUpload() {
	$(".btn-primary.start").show();
	$(".btn-primary.start").removeClass("disabled");
	disabledUpload = false;
}
function removeUpload() {
	$(".btn-primary.start").hide();
	disabledUpload = true;
}
$(function () {
	$("body").on("click", ".post_upload_btn", function (e) {
		$(".post_upload_btn").hide();
		setTimeout(function () {
			formSubmit()
		}, 500);
	});
})
function formSubmit() {
	if (!disabledUpload) {
		submitError = false;
		$(".form_error").html("");
		$("#server_error").html("").hide();
		$('[name="comment"]').val($('[name="comment"]').val().slice(0, commentLenght - 1));
		if ($('[name="comment"]').val().trim() !== "") {
			if (isUrlValid($('[name="comment"]').val())) {
				$(".comments_error").html(langLine.fe_imageupload_comment_link);
				submitError = true;
			} else {
				submitError = censorship($('[name="comment"]').val(), ".comments_error", langLine.fe_imageupload_comment_censor);
			}
		}
		var allTags = getTags();
		if (allTags === true) {
			submitError = true;
		}

		if (allTags != "") {
			var cens = censorship(allTags, ".tags_error", langLine.fe_imageupload_tag_censor);
			if (cens) {
				submitError = true;
			}
		}
		if (!$("input[name='sex']:checked").val()) {
			$(".gender_error").html(langLine.fe_imageupload_gender_none);
			submitError = true;
		}
		if ((can_sponsor || is_brand) && $(".url-selector").val().trim() == "") {
			$(".url_error").html(langLine.fe_upload_error_url_must);
			submitError = true;
		}
		if ($(".url-selector").val().trim() != "") {
			// if it is brand 
			if ($(".post_price").val().trim() != "" && $(".post_price").val().trim() != 0 && $(".url-selector").val().trim() == "") {
				$(".price_error").html(langLine.fe_upload_error_price_url);
				submitError = true;
			}
			//$(".sponsored_selector").attr('checked');
			var url = $(".url-selector").val();
//				var check_url = isUrlValid(url, true);
//				if (!check_url) {
//					$(".gender_error").html(langLine.fe_upload_error_url);
//					submitError = true;
//				} else if (check_url.length > 0) {
//					url = check_url + url;
//					$(".url-selector").val(url);
//				}
			if (can_sponsor	&& $(".sponsored_selector").attr('checked') == undefined && $(".post_price").val().trim() == "" && $(".post_currency").val().trim() == ""	) {
				$(".url_error").html(langLine.fe_upload_error_url_must_sponsored);
				submitError = true;
			} else if ((($(".post_price").val().trim() != "" || $(".post_currency").val().trim() != "") && can_sponsor) || !can_sponsor) {
				
				
				if (!can_sponsor && $(".post_price").val().trim() == "" && $(".post_currency").val().trim() == "") {
					$(".price_error").html(langLine.fe_upload_error_must_price);
					submitError = true;
				}
				if ($(".post_price").val() != "" && $(".post_price").val() < 0) {
					$(".price_error").html(langLine.fe_upload_error_price);
					submitError = true;
				}
				if ($(".post_currency").val() == "" && $(".post_price").val() != "") {
					$(".currency_error").html(langLine.fe_upload_error_currency);
					submitError = true;
				}
				if ($(".post_currency").val() != "" && $(".post_price").val() == "") {
					$(".price_error").html(langLine.fe_upload_error_no_price);
					submitError = true;
				}
			}
		}
		if ($('[name="category"]').val() == -1 || $('[name="category"]').val() === "") {
			$(".cats_error").html(langLine.fe_imageupload_cat_none);
			submitError = true;
		}

		if (!submitError) {
			if (!web_image) {
				$("#web_image").val("");
				$(".upload_request_holder .fileupload-progress").show();
				$(".upload_request_holder .fileupload-buttonbar").hide();
				$(".btn-warning").remove();
				canvasElement = $(".preview canvas");
				if (form_data !== "" && !form_data.files.error) {
					form_data.submit();
				}
				return false;
			} else {
				disableUpload();
				data_to_post = $("#fileupload").serialize();
				$('#ajax-loading-image').show();
				$.ajax({
					type: "POST",
					url: baseUrl + $('#fileupload').attr("action"),
					data: data_to_post,
					dataType: "json",
					success: function (data)
					{
						if (!data.files[0].error) {
							$('#ajax-loading-image').hide();
							removeUpload();
							$("#fileupload").remove();
							window.location = data["redirect"];
							//TSM go here landig page after uploa hash
						} else {
							$("#server_error").show();
							$("#server_error").html(data.files[0].error);
							$('#ajax-loading-image').hide();
							$(".submit_upload .start").removeClass("disabled");
							disabledUpload = false;
							//show error
						}
					},
					complete: function (data) {
						//console.log(data);
						//$('#ajax-loading-image').hide();
						//removeUpload();
					}
				});
			}
		} else {
			$(".post_upload_btn").show();
			//show errors
		}
	}
}

function process_link(upload_url) {
	url_processing = true;
	$(".fileinput-button input").prop('disabled', true);
	$(".fileinput-button").addClass("disabled");

	upload_url = upload_url.trim();
	if (isUrlValid(upload_url) && last_image_url != upload_url) {
		last_image_url = upload_url;
		$(".upload_request_holder .web_image_error").html("");
		showRemoteImage();
		resetRemoteImages();
		if (upload_url != "") {
			if (stristr(upload_url, [".jpg", ".jpeg", ".png", ".gif"])) {
				//if it is image
				showRemoteImage(upload_url);
			} else {
				getRemoteImages(upload_url);
				$(".link_attr_wrapper").show();
				$(".url-selector").val(upload_url);
				// it is url
			}
		} else {
			url_processing = false;
		}
	} else {
//		$(".web_image_error").html(langLine.fe_upload_error_url);

		url_processing = false;
	}
}

function getRemoteImages(url) {
	$('.upload_image_selector').show();
	$('.tip_images').addClass("loading");
	$('.tip_images .left_sequence').on("click", function (e) {
		rotateSequencesPrevious();
	});
	$('.tip_images .right_sequence').on("click", function (e) {
		rotateSequencesNext();
	});
	var ajax_result;
	$.post({
		url: baseUrl + upload_url + '/remote_images',
		dataType: 'json',
//		async: false,
		data: {"url": url},
		success: function (data) {
			ajax_result = data;
			console.log(ajax_result);
			if (ajax_result.images.facebook_image) {
				var fb_image = [ajax_result.images.facebook_image.source];
				if (ajax_result.images.facebook_description) {
					$(".comment-selector").val(ajax_result.images.facebook_description);
				}
				process_remote_images(fb_image);
			} else if (ajax_result === undefined || ajax_result.error || ajax_result.images.length == 0) {
				showStandartImage();
				resetRemoteImages();
				$(".web_image_error").html(langLine.fe_upload_error_url);
			} else {
				//tip_images
				process_remote_images(ajax_result.images);
			}

		}
	});
	url_processing = false;
}


$(function () {
	'use strict';
	//TAGS
	$('#tag-input').tagit({
		//tagSource: availableTags,
		triggerKeys: ['enter', 'comma', 'tab'],
		maxLength: my_max_tag_length,
		minLength: my_min_tag_length,
		maxTags: my_max_tags,
		tagSource: function (request, response) {
			if (request.term.length >= 2) {
				$.ajax({
					url: baseUrl + upload_url + "/search_tags",
					data: {term: request.term},
					dataType: "json",
					method: "POST",
					success: function (data) {
						response($.map(data, function (item) {
							return {
								label: item.value,
								value: item.value
							};
						}));
					}
				});
			}
		},
		tagsChanged: function (tagValue) {
//			this.appendTo.find(".ui-autocomplete-loading").removeClass("ui-autocomplete-loading");
//			$(".ui-helper-hidden-accessible").remove();
			if (tagValue != null) {
				var new_value = tagValue.replace(/[^\w\wа-яА-Я;&-/\s]/g, '').replace(/[_]/g, '');
				if (new_value != tagValue) {
					$('#tag-input').tagit("remove", tagValue);
					if (new_value != "") {
						$('#tag-input').tagit("add", new_value);
					}
				}
			}
		}
	});


	//UPLOADER
	//BINDS
	$(".web_image_reset").on("click", function (e) {
		last_image_url = "";
		$("#web_image").val("");
		$(".upload_request_holder .web_image_error").html("");
		showRemoteImage();
		resetRemoteImages();
		$(".url-selector").val("");
		$(".comment-selector").val("");
		$(".link_attr_wrapper").hide();
		web_image = false;
	});

	$(".upload_image_selector .chouse").on("click", function (e) {
		$("#web_image").val($(".tip_image_url").val());
		$(".upload_image_selector").hide();
		var temp_url = $("#web_image").val();
		while (temp_url.indexOf(" ") != -1) {
			temp_url = temp_url.replace(" ", "%20");
		}
		$("#web_image").val(temp_url);
		process_link(temp_url);
	});

	$("#web_image").on("change blur", function (e) { //click keyup
		if (!url_processing) {
//			$(".url-selector").val("");
			if ($("#web_image").val() != "") {
				var temp_url = $("#web_image").val();
				while (temp_url.indexOf(" ") != -1) {
					temp_url = temp_url.replace(" ", "%20");
				}
				$("#web_image").val(temp_url);
				process_link(temp_url);
			}
		}
	});



//TODO: after fixing ^change and uncomment	
//	var timeoutShow;
//	$("#web_image").on("keyup", function (e) {
//		if ($("#web_image").val() != "") {
//			console.log("here upload 1");
//			clearTimeout(timeoutShow);
//			timeoutShow = setTimeout(function () {
//				//MYTODO: !!!! image scrapper for posts
//				showRemoteImage($("#web_image").val());
//			}, 400);
//		}
//	});


	$(".comment-selector").on("keyup click blur change", function () {
		if ($('[name="comment"]').val().length > commentLenght) {
			$('[name="comment"]').val($('[name="comment"]').val().slice(0, commentLenght - 1));
		}
	});
	$(".url-selector").on("change", function () {
		var url = $(".url-selector").val();
		if (url != "") {
			$(".link_attr_wrapper").show();
			var check_url = isUrlValid(url, true);
			if (!check_url) {
				$(".url_error").html(langLine.fe_upload_error_url);
			} else if (check_url.length > 0) {
				url = check_url + url;
				$(".link_attr_wrapper").show();
				$(".url-selector").val(url);
			}
		} else {
			$(".post_currency").val("");
			$(".post_price").val("");
			$(".sponsored_selector").prop("checked", false);
			$(".link_attr_wrapper").hide();
		}
	});


	$('#fileupload').fileupload({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		//url: 'server/php/'
	});
	// Enable iframe cross-domain access via redirect option:
	$('#fileupload').fileupload(
			'option',
			'redirect',
			window.location.href.replace(
					/\/[^\/]*$/,
					'/cors/result.html?%s'
					)
			);
	function data_submit(data) {
		data.submit();
	}
	function init_upload() {
		var openNextUpload;
		$('#fileupload').fileupload('option', {
			//url: '//jquery-file-upload.appspot.com/',
			// Enable image resizing, except for Android and Opera,
			// which actually support image resizing, but fail to
			// send Blob objects via XHR requests:
			disableImageResize: /Android(?!.*Chrome)|Opera/
					.test(window.navigator.userAgent),
			maxFileSize: maxFileUpload,
			loadImageMaxFileSize: maxFileUpload,
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			//maxNumberOfFiles: 2,
			previewMaxWidth: my_imageupload_min_w,
			previewMaxHeight: my_imageupload_min_h,
			imageMinWidth: my_imageupload_min_w,
			imageMinHeight: my_imageupload_min_h
					//,autoUpload: true
		});
		$('#fileupload').bind('fileuploadadd', function (e, data) {
			resetServerError();
			$(".upload_request_holder .fileinput-button").hide();
			$(".upload_request_holder .fileupload-webimage").hide();
			$(".text-danger").remove();
			openNextUpload = setTimeout(function () {
				$(".upload_request_holder .upload_form_next").show();
			}, 200);
			form_data = data;
		})
				.bind('fileuploaddone', function (e, data) {
					$(".upload_request_holder .fileupload-progress").hide();
					$(".after_upload").html(canvasElement);
					$(".after_upload").show();
					removeUpload();
					window.location = successPage;
				})
				.bind('fileuploadfail', function (e, data) {
					$(".upload_request_holder .fileinput-button").show();
					$(".upload_request_holder .table.table-striped .files").html("");
					$(".upload_request_holder .upload_form_next").hide();
					$(".upload_request_holder .fileupload-webimage").show();
				})
				.bind('fileuploadsubmit', function (e, data) {
					//return false;
				});
		$('#fileupload').on("my_error", function (e) {
			clearTimeout(openNextUpload);
			$(".upload_request_holder .upload_form_next").hide();
			//$(".btn-warning").remove();
			$(".btn-warning").css("margin-top", "15px");
			$(".preview").html("");
		});
		$('#fileupload').on("my_error_server", function (e, data) {
			enableUpload();
			$(".upload_request_holder .upload_form_next").hide();
			$(".upload_request_holder .fileupload-buttonbar").show();
			$(".upload_request_holder .fileinput-button").show();
			$(".upload_request_holder .fileupload-webimage").show();
			$(".upload_request_holder .after_upload").remove();
			$("#server_error").show();
		});
		$(".upload_request_holder .fileinput-button").on("click", function () {
			$(".upload_request_holder .fileinput-button").show();
			$(".upload_request_holder .table.table-striped .files").html("");
			$(".upload_request_holder .upload_form_next").hide();
			$(".upload_request_holder .fileupload-webimage").show();
			$("#web_image").val("");
		});
//		$('.btn-primary.start').on("click", function (e) {
//			
//		});
	}
	init_upload();
	$.ajax({
		// Uncomment the following to send cross-domain cookies:
		//xhrFields: {withCredentials: true},
		url: $('#fileupload').fileupload('option', 'url'),
		dataType: 'json',
		context: $('#fileupload')[0]
	}).always(function () {
		$(this).removeClass('fileupload-processing');
	}).done(function (result) {
		$(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});
	});

}
);
