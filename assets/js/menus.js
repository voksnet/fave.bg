//var jQuery = jQuery.noConflict();
$(document).ready(function () {
	var isOpen = false;
	$(document).on('click', '#click-menu', function () {
		//$('#responsive-menu').css('height', $(document).height());
		!isOpen ? openRM() : closeRM()
	});
	function openRM() {
//		$("body").css("overflow", "hidden");
//		$("body").css("position", "fixed");
//		var tc_width = $(".the_content").width();
//		$(".the_content").css("width", tc_width);

		$('#responsive-menu').css('display', 'block');
		$('#responsive-menu').addClass('RMOpened');
		$('#click-menu').addClass('click-menu-active');
		$('header').stop().animate({
			marginLeft: '280px'
		}, 200, 'linear');
		$('#responsive-menu').stop().animate({
			left: "000"
		}, 200, 'linear', function () {
			//$('#responsive-menu').css('height', $(document).height());
			isOpen = true
		})
		isOpen = true
		//$(".side-menu-wrapper").css("height",$(window).height() + "px");
	}

	function closeRM() {
		if (isOpen) {
			$('header').stop().animate({
				marginLeft: '0px'
			}, 200, 'linear');
			$('#responsive-menu').animate({
				left: "-260"
			}, 200, 'linear', function () {
				$('#responsive-menu').css('display', 'none');
				$('#responsive-menu').removeClass('RMOpened');
				$('#click-menu').removeClass('click-menu-active');
				isOpen = false;
//				$(".the_content").css("width", "");
			});
		}
	}
	$(window).resize(function () {
		//$('#responsive-menu').css('height', $(document).height());
		if ($(window).width() > 980) {
			if ($('#responsive-menu').css('left') != '-260') {
				closeRM();
			}
		}
	});
	$('#responsive-menu ul ul').css('display', 'none');
	clickLink = '<span class=\"appendLink\">▼</span>';
	clickedLink = '<span class=\"appendLink rm-append-active\">▲</span>';
	excludeList = '.current-menu-item, .current-menu-ancestor, .current_page_ancestor';
	$('#responsive-menu .menu-item-has-children').not(excludeList).prepend(clickLink);
	$('#responsive-menu .page_item_has_children.current-menu-item').prepend(clickedLink);
	$('#responsive-menu .page_item_has_children.current-menu-ancestor').prepend(clickedLink);
	$('#responsive-menu .menu-item-has-children.current-menu-item').prepend(clickedLink);
	$('#responsive-menu .menu-item-has-children.current-menu-ancestor').prepend(clickedLink);
	$('#responsive-menu .page_item_has_children').not(excludeList).prepend(clickLink);
	$('#responsive-menu .menu-item-has-children.current_page_ancestor').prepend(clickedLink);
	$('#responsive-menu .page_item_has_children.current_page_ancestor').prepend(clickedLink);
	$('.appendLink').on('click', function () {
		$(this).nextAll('#responsive-menu ul ul').slideToggle();
		$(this).html($(this).hasClass('rm-append-active') ? '▼' : '▲');
		$(this).toggleClass('rm-append-active');
		//$('#responsive-menu').css('height', $(document).height())
	});
	$('.rm-click-disabled').on('click', function () {
		$(this).nextAll('#responsive-menu ul ul').slideToggle();
		$(this).siblings('.appendLink').html($(this).hasClass('rm-append-active') ? '▼' : '▲');
		$(this).toggleClass('rm-append-active');
		//$('#responsive-menu').css('height', $(document).height())
	});
	$('#responsive-menu .current_page_ancestor.menu-item-has-children').children('ul').css('display', 'block');
	$('#responsive-menu .current-menu-ancestor.menu-item-has-children').children('ul').css('display', 'block');
	$('#responsive-menu .current-menu-item.menu-item-has-children').children('ul').css('display', 'block');
	$('#responsive-menu .current_page_ancestor.page_item_has_children').children('ul').css('display', 'block');
	$('#responsive-menu .current-menu-ancestor.page_item_has_children').children('ul').css('display', 'block');
	$('#responsive-menu .current-menu-item.page_item_has_children').children('ul').css('display', 'block')
});



var jquerycssmenu = {
	fadesettings: {
		overduration: 0,
		outduration: 100
	},
	buildmenu: function (b, a) {
		$(document).ready(function (e) {
			var c = e("#" + b + ">ul");
			var d = c.find("ul").parent();
			d.each(function (g) {
				var h = e(this);
				var f = e(this).find("ul:eq(0)");
				this._dimensions = {
					w: this.offsetWidth,
					h: this.offsetHeight,
					subulw: f.outerWidth(),
					subulh: f.outerHeight()
				};
				this.istopheader = h.parents("ul").length == 1 ? true : false;
				f.css({
					//top: this.istopheader ? this._dimensions.h + "px" : 0
				});
				h.children("a:eq(0)").css(this.istopheader ? {
					paddingRight: a.down[2]
				} : {}).append('<img src="' + (this.istopheader ? a.down[1] : a.right[1]) + '" class="' + (this.istopheader ? a.down[0] : a.right[0]) + '" style="border:0;" />');
				h.hover(function (j) {
					var i = e(this).children("ul:eq(0)");
					this._offsets = {
						left: e(this).offset().left,
						top: e(this).offset().top
					};
					var k = this.istopheader ? 0 : this._dimensions.w;
					k = (this._offsets.left + k + this._dimensions.subulw > e(window).width()) ? (this.istopheader ? -this._dimensions.subulw + this._dimensions.w : -this._dimensions.w) : k;
					i.css({
						left: k + "px"
					}).fadeIn(jquerycssmenu.fadesettings.overduration)
				}, function (i) {
					e(this).children("ul:eq(0)").fadeOut(jquerycssmenu.fadesettings.outduration)
				})
			});
			c.find("ul").css({
				display: "none",
				visibility: "visible"
			})
		})
	}
};
if (base_url === undefined) {
	var base_url = "";
}
var arrowimages = {
	down: ['downarrowclass', base_url + 'assets/images/arrow-down.png'],
	right: ['rightarrowclass', base_url + 'assets/images/arrow-right.png']
};
jquerycssmenu.buildmenu("myjquerymenu", arrowimages);
jquerycssmenu.buildmenu("myjquerymenu-cat", arrowimages);