//gmail login
var googleUser = {};
var gmailLogin = function (client_id, redirect_to) {
	gapi.load('auth2', function () {
		// Retrieve the singleton for the GoogleAuth library and set up the client.
		auth2 = gapi.auth2.init({
			client_id: client_id,
			cookiepolicy: 'single_host_origin',
			// Request scopes in addition to 'profile' and 'email'
			//scope: 'https://www.googleapis.com/auth/plus.me'
		});
		attachSignin(document.getElementById('gmailLogin'), redirect_to);
	});
};

function attachSignin(element, redirect_to) {
	auth2.attachClickHandler(element, {},
			function (googleUser) {
				//console.log(googleUser.getBasicProfile());
				var user_data = {
					'gmail_id': googleUser.getAuthResponse().id_token
				};
				jQuery.ajax({
					type: "post",
					data: user_data
				}).done(function (data) {
					if (data !== '')
					{
						var data_obj = jQuery.parseJSON(data);

						if (data_obj.error !== undefined) {
							jQuery(".error").html(data_obj.error);
						}
						if (data_obj.logged) {
							if (redirect_to != "") {
								window.location.href = redirect_to;
							} else {
								window.location.href = data_obj.redirect;
							}
						}
					} else {
						//window.location.href = redirect_to;
						//throw new Error('go');
					}
				});
			}, function (error) {
		alert(JSON.stringify(error, undefined, 2));
	});
}
