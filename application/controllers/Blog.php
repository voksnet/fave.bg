<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('blog_model');
	}

	function index($slug) {
		if ($slug == "") {
			redirect("blog");
		} else if (filter_var($slug, FILTER_VALIDATE_INT)) {
			$this->bloglist($slug);
		} else {
			$post = $this->blog_model->get_post($slug);
			if (!$post) {
				redirect("blog");
			}

			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$header_data["fb_pixel"] = $this->load->view("fb_pixel/event", ['events' => ["blogView"], 'title' => $post->title], true);
			$data_seo = array(
				"image" => base_url() . "cache/blog/" . $post->slug . ".jpg",
				"title" => $post->title . " " . $post->subtitle,
				"description" => mb_substr(str_replace("\n", " ", strip_tags($post->content)), 0, 160) . "...",
				"image:width" => 620,
				"image:height" => 310
			);

			$seo = $this->seo_model->make_seo($data_seo);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);

			$data = array(
				"post" => $post,
				"bIsLiked" => $this->blog_model->is_liked($post->id),
				"side_posts" => $this->blog_model->get_posts($post->id, 4)
			);

			$this->load->view('index_content');
			$this->load->view('blog/post', $data);
//            $this->load->view('main_templates/masonry', array());
			$this->load->view('main_templates/footer');
			$this->load->view('main_templates/common_footer');
		}
	}

	function bloglist($page = 1) {

//        if ($page == "") {
//            $page = 1;
//        }
		$post_per_page = 10;

		$this->load->library('pagination');

		$this->db->where("del", "no");
		$total_blogs = $this->db->count_all_results("blog");

		$blogs = $this->blog_model->get_posts(false, $post_per_page, ($page - 1) * $post_per_page);
		$config['base_url'] = base_url("blog");
		$config['total_rows'] = $total_blogs;
		$config['per_page'] = $post_per_page;
		$config['use_page_numbers'] = true;
//        $config['next_link'] = '&#10097;';
//        $config['prev_link'] = '&#10096;';

		$this->pagination->initialize($config);


		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
//        $header_data["fb_pixel"] = $this->load->view("fb_pixel/event", ['events' => ["blogView"], 'title' => $post->title], true);
//        $data_seo = array(
//                "image" => base_url() . "cache/blog/" . $post->slug . ".jpg",
//                "title" => $post->title . " " . $post->subtitle,
//                "description" => mb_substr(str_replace("\n", " ", strip_tags($post->content)), 0, 160) . "...",
//                "image:width" => 620,
//                "image:height" => 310
//        );

		$seo = $this->seo_model->make_seo();	// $data_seo
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);

		$data = array(
			"blogs" => $blogs,
			"pagination" => $this->pagination->create_links()
		);

		$this->load->view('index_content');
		$this->load->view('blog/list', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

}

?>