<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function ajax() {
		//count_post_tips($post_id)
		if (IS_AJAX) {
			$result = array();
			switch ($this->input->post("action")) {
				case "is_logged":
					if (!$this->session->userdata("logged_in")) {
						$result["login"] = true;
					} else {
						$result["ok"] = true;
					}
					break;
			}
			echo json_encode($result);
		}
	}

	public function index() {
		$this->users_model->is_logged(true);
		//AJAX OR NO AJAX
		if (IS_AJAX) {
			$ajax = array();
			$ajax["logged"] = false;
			if ($this->input->post("gmail_id") != "") {
				//check facebook info and validity
				$gmail_data = file_get_contents("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" . $this->input->post("gmail_id"));
				if (stristr($http_response_header[0], "200 OK")) {
					$gmail_data = json_decode($gmail_data, true);
					if ($gmail_data["email_verified"] == "true") {
						$ajax["logged"] = $this->users_model->gmail_connect($gmail_data);
						$ajax["redirect"] = base_url() . $this->config->item("my_urlparams_myaccount");
					} else {
						$ajax["error"] = $this->lang->line('fe_login_failed_gmail_notverified');
					}
				} else {
					$ajax["error"] = $this->lang->line('fe_login_failed_gmail');
				}
			} else {
				$ajax["logged"] = $this->users_model->login($this->input->post("email_address"), $this->input->post('password'));
			}
			if (!$ajax["logged"]) {
				$ajax["error"] = $this->lang->line('fe_login_failed');
			}
			//remember me
			die(json_encode($ajax));
		} else {
			//facebook login/signup
			$fb_login_url = $this->fb_login();
			if ($fb_login_url != "") {
				$data['login_url'] = $fb_login_url;
			}

			$title = $this->lang->line("fe_register");
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);
			$this->load->view("users/registration/welcome.php", $data);
			$this->load->view('main_templates/simple_footer', $data);
			$this->load->view('main_templates/common_footer', $data);
		}
	}

	public function login() {
		if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
			$redirect_to = $this->session->userdata("last_url");
		} else {
			$redirect_to = base_url();
		}
		if ($this->uri->uri_string() == "logout") {
			$this->logout();
		} else {
			$this->users_model->is_logged(true, $redirect_to);
			//$this->load->helper('cookie');
			if (!$this->session->userdata('logged_in')) {
				//AJAX OR NO AJAX
				if (IS_AJAX) {
					$ajax = array();
					$ajax["logged"] = false;
					if ($this->input->post("gmail_id") != "") {
						//check facebook info and validity
						//https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=XYZ123
						$gmail_data = file_get_contents("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" . $this->input->post("gmail_id"));
						if (stristr($http_response_header[0], "200 OK")) {
							$gmail_data = json_decode($gmail_data, true);
							if ($gmail_data["email_verified"] == "true") {

								$ajax["logged"] = $this->users_model->gmail_connect($gmail_data);
							} else {
								$ajax["error"] = $this->lang->line('fe_login_failed_gmail_notverified');
							}
						} else {
							$ajax["error"] = $this->lang->line('fe_login_failed_gmail');
						}
					} else {
						$ajax["logged"] = $this->users_model->login($this->input->post("email_address"), $this->input->post('password'));
						$ajax["redirect"] = $redirect_to;
					}
					if (!$ajax["logged"]) {
						$ajax["error"] = $this->lang->line('fe_login_failed');
					} else {
						$ajax["redirect"] = $redirect_to;
					}
					//remember me
					die(json_encode($ajax));
				} else {
					//facebook login/signup
					$fb_login_url = $this->fb_login();
					if ($fb_login_url != "") {
						$data['login_url'] = $fb_login_url;
					}
					$data["redirect_to"] = $redirect_to;






					$title = $this->lang->line("fe_login");
					$data_seo = array(
						"title" => $title
					);
					$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
					$seo = $this->seo_model->make_seo($data_seo, true);
					$header_data = array_merge($header_data, $seo);
					$this->load->view('main_templates/header', $header_data);


					$this->load->view("users/login.php", $data);
					$this->load->view('main_templates/simple_footer', $data);
					$this->load->view('main_templates/common_footer');
				}
			} else {
				die("already logged");
			}
		}
	}

	public function fb_login() {
		//facebook login
		$this->load->library('facebook');
		$user = $this->facebook->getUser();
		if ($user) {
			try {
				$fb_data = $this->facebook->api('/me?fields=id,name,email,gender');
				if (isset($fb_data["email"]) && $fb_data["email"] != "") {
					$this->users_model->facebook_connect($fb_data);
					//after login we destroy unnessesary facebook session
					$this->facebook->destroySession();
				} else {
					$this->facebook->destroySession();
					show_404();
				}
			} catch (FacebookApiException $e) {
				$user = null;
			}
		} else {
			// Solves first time login issue. (Issue: #10)
			//$this->facebook->destroySession();
		}

		if (!$user) {
			$fb_data['login_url'] = $this->facebook->getLoginUrl(array(
				'redirect_uri' => site_url('login'),
				//'redirect_uri' => site_url('welcome/login'),
				'scope' => array("email") // permissions here
			));
			return $fb_data['login_url'];
		} else {
			if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
				redirect($this->session->userdata("last_url"));
			} else {
				redirect('/', 'refresh');
			}
			//redirect('/', 'refresh');
		}
	}

	public function thank($brand = "") {
		if ($brand == "") {
			if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
				$redirect_to = $this->session->userdata("last_url");
			} else {
				$redirect_to = $this->session->userdata("profile_url");
			}
			redirect($redirect_to);
		} else if ($brand == "b") {
			$title = $this->lang->line("fe_register");
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);

			$this->load->view("users/thank.php");
			$this->load->view('main_templates/simple_footer');
			$this->load->view('main_templates/common_footer');
		}
	}

	public function registration() {
		if (!$this->users_model->is_logged(true)) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('sex', $this->lang->line('fe_register_error_sex'), 'required');
			$this->form_validation->set_rules('user_name', $this->lang->line('fe_register_error_name'), 'required|min_length[5]|max_length[25]');
			$this->form_validation->set_rules('email_address', $this->lang->line('fe_register_error_email'), 'trim|required|valid_email|is_unique[users.email]', array(
				'is_unique' => $this->lang->line('fe_register_error_email–exists')
				)
			);
			$this->form_validation->set_rules('password', $this->lang->line('fe_register_error_password'), 'trim|required|min_length[5]|max_length[32]');
			$this->form_validation->set_rules('con_password', $this->lang->line('fe_register_error_password-repeat'), 'trim|required|matches[password]');
			if ($this->input->post('sex') != "b") {
				$valid_date = false;
				if (trim($this->input->post('birth_date_month')) != '' && trim($this->input->post('birth_date_day')) != '' && trim($this->input->post('birth_date_year')) != '') {
					$valid_date = checkdate($this->input->post('birth_date_month'), $this->input->post('birth_date_day'), $this->input->post('birth_date_year'));
					if ($valid_date) {
						$_POST['birthdate'] = $this->input->post('birth_date_year') . "-" . $this->input->post('birth_date_month') . "-" . $this->input->post('birth_date_day');
						$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
						);
					} else {
						$_POST['birthdate'] = "";
						$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
						);
					}
				} else {
					$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
					);
				}
			} else {
				$this->form_validation->set_rules('brand_name', $this->lang->line('fe_register_error_brand'), 'trim|required|max_length[20]');
				$this->form_validation->set_rules('website', $this->lang->line('fe_register_error_website'), 'trim|required|max_length[40]');
				$this->form_validation->set_rules('phone', $this->lang->line('fe_register_error_phone'), 'trim|required|max_length[20]');
			}
			if ($this->form_validation->run() == FALSE) {
				$this->ml(true);
			} else {
				//process data
				if ($this->input->post('sex') != "b") {
					unset($_POST['brand_name']);
					unset($_POST['website']);
					unset($_POST['phone']);
				}
				unset($_POST["birth_date_day"]);
				unset($_POST["birth_date_month"]);
				unset($_POST["birth_date_year"]);
				unset($_POST["con_password"]);
				$user_data = $_POST;
				$user_data["password"] = password_hash($user_data["password"], PASSWORD_DEFAULT);
				$user_data["username"] = $user_data["user_name"];
				unset($user_data["user_name"]);
				$user_data["email"] = $user_data["email_address"];
				unset($user_data["email_address"]);
				if ($user_data['sex'] == "b") {
					$user_data['del'] = "0";
				}
				unset($user_data["submit"]);
				unset($user_data["avatar_orientation"]);
				unset($user_data["old_avatar"]);

				$this->users_model->add_user($user_data);
				$this->thank();
			}
		}
	}

	public function edit() {
		if ($this->users_model->is_logged()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('user_name', $this->lang->line('fe_register_error_name'), 'required|min_length[5]|max_length[25]');
			//TODO: email change with confirmation
//			$validation_email = "";
//			if ($this->session->userdata("email") != $this->input->post("email_address")) {
//				$validation_email = "|is_unique[users.email]";
//			}
//			$this->form_validation->set_rules('email_address', $this->lang->line('fe_register_error_email'), 'trim|required|valid_email' . $validation_email, array(
//				'is_unique' => $this->lang->line('fe_register_error_email–exists')
//					)
//			);
			$this->form_validation->set_rules('password', $this->lang->line('fe_register_error_password'), 'trim|min_length[5]|max_length[32]');
			if ($this->input->post("password") != null) {
				$this->form_validation->set_rules('con_password', $this->lang->line('fe_register_error_password-repeat'), 'trim|required|matches[password]');
			};
			if ($this->input->post('sex') != null) {
				$valid_date = false;
				$this->form_validation->set_rules('sex', $this->lang->line('fe_register_error_sex'), 'required');
				if (trim($this->input->post('birth_date_month')) != '' && trim($this->input->post('birth_date_day')) != '' && trim($this->input->post('birth_date_year')) != '') {
					$valid_date = checkdate($this->input->post('birth_date_month'), $this->input->post('birth_date_day'), $this->input->post('birth_date_year'));
					if ($valid_date) {
						$_POST['birthdate'] = $this->input->post('birth_date_year') . "-" . $this->input->post('birth_date_month') . "-" . $this->input->post('birth_date_day');
						$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
						);
					} else {
						$_POST['birthdate'] = "";
						$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
						);
					}
				} else {
					$this->form_validation->set_rules('birthdate', 'Birthdate', 'required', array('required' => $this->lang->line('fe_register_error_birthdate'))
					);
				}
			} else {
				$this->form_validation->set_rules('brand_name', $this->lang->line('fe_register_error_brand'), 'trim|required|max_length[20]');
				$this->form_validation->set_rules('website', $this->lang->line('fe_register_error_website'), 'trim|required|max_length[40]');
				$this->form_validation->set_rules('phone', $this->lang->line('fe_register_error_phone'), 'trim|required|max_length[20]');
			}
			$this->form_validation->set_rules('about', $this->lang->line('fe_register_error_description'), 'min_length[50]|max_length[500]');

			if ($this->form_validation->run() == FALSE) {
				$this->ed(true);
			} else {
				//process data

				if ($this->input->post('sex') != null) {
					unset($_POST['brand_name']);
					unset($_POST['website']);
					unset($_POST['phone']);
				}
				unset($_POST["birth_date_day"]);
				unset($_POST["birth_date_month"]);
				unset($_POST["birth_date_year"]);
				unset($_POST["con_password"]);
				$user_data = $_POST;
				if ($this->input->post("password") != null) {
					$user_data["password"] = password_hash($user_data["password"], PASSWORD_DEFAULT);
				}
				$user_data["username"] = $user_data["user_name"];
				unset($user_data["user_name"]);
				//MYTODO: email change with confirmation
				//$user_data["email"] = $user_data["email_address"];
				unset($user_data["email_address"]);
				unset($user_data["submit"]);
				$this->users_model->edit_user($user_data);
				$this->thank();
			}
		}
	}

	public function ml($has_error = false) { //comes from MAIL register :-)
		if (($this->session->userdata('user_name') != "")) {
			if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
				$redirect_to = $this->session->userdata("last_url");
			} else {
				$redirect_to = base_url();
			}
			redirect($redirect_to);
		} else {
			$sex_array = array("m" => "", "f" => "", "b" => "");

			if ($this->input->post("sex") != "") {
				$sex_array[$this->input->post("sex")] = "  checked=\"checked\"";
			}
			$data["sex_array"] = $sex_array;
			if ($this->input->post("sex") != "" && $this->input->post("sex") == "b") {
				$data["show_brand"] = true;
			} else {
				$data["show_brand"] = false;
			}

			$this->load->helper('birthdate');
			$data['birth_date_year'] = buildYearDropdown('birth_date_year', $this->input->post('birth_date_year'), 13, $this->lang->line('date_year'));
			$months_arr = array(
				'' => $this->lang->line('date_month'),
				'01' => $this->lang->line('cal_january'),
				'02' => $this->lang->line('cal_february'),
				'03' => $this->lang->line('cal_march'),
				'04' => $this->lang->line('cal_april'),
				'05' => $this->lang->line('cal_mayl'),
				'06' => $this->lang->line('cal_june'),
				'07' => $this->lang->line('cal_july'),
				'08' => $this->lang->line('cal_august'),
				'09' => $this->lang->line('cal_september'),
				'10' => $this->lang->line('cal_october'),
				'11' => $this->lang->line('cal_november'),
				'12' => $this->lang->line('cal_december')
			);
			$data['birth_date_month'] = buildMonthDropdown('birth_date_month', $this->input->post('birth_date_month'), $months_arr);
			$data['birth_date_day'] = buildDayDropdown('birth_date_day', $this->input->post('birth_date_day'), $this->lang->line('date_day'));
			$data["register_type"] = "register";
			$data["submit_button"] = $this->lang->line("fe_register_submit");

			if ($has_error) {
				$data["has_error"] = $this->lang->line("fe_register_has_error");
			}

			$title = $this->lang->line("fe_register");
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);

			$this->load->view("users/registration/register.php", $data);
			$this->load->view('main_templates/simple_footer');
			$this->load->view('main_templates/common_footer');
		}
	}

	public function ed($has_error = false) {
		if ($this->session->userdata('username') == null || $this->session->userdata('username') == "") {
			if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
				$redirect_to = $this->session->userdata("last_url");
			} else {
				$redirect_to = base_url();
			}
			redirect($redirect_to);
		} else {
			$from_post = false;
			if ($this->input->post("submit") == null) {
				$data["init"] = $this->users_model->get_user_edit("", $this->session->userdata("selector"));
				if ($data["init"]["avatar"] == "" && ( isset($data["init"]["fb_image"]) || isset($data["init"]["gm_image"]))) {
					if (isset($data["init"]["fb_image"]) && $data["init"]["fb_image"] != "") {
						$this->users_model->make_avatar($data["init"]["fb_image"]);
					} else if (isset($data["init"]["gm_image"]) && $data["init"]["gm_image"] != "") {
						$this->users_model->make_avatar($data["init"]["gm_image"]);
					} else {
						$source = "female";
						switch ($data["init"]["sex"]) {
							case "f":
								$source = "female";
								break;
							case "m":
								$source = "male";
								break;
							case "b":
								$source = "brand";
								break;
						}
						$this->users_model->make_avatar($source);
					}
					redirect(base_url() . $this->config->item("my_urlparams_myaccount"));
				} else {
					$data["avatar"] = $data["init"]["avatar"];
				}
			} else {


				$from_post = true;
				$data["init"] = $this->input->post();
//				die($data["init"]["avatar"]);
				//ata:image/png;base64

				if (!isset($data["init"]["avatar"]) || $data["init"]["avatar"] == "") {
					$data["init"]["avatar"] = $data["init"]["old_avatar"];
				}
			}
			$data["avatar_folder"] = "";


			if (!stristr($data["init"]["avatar"], "data:image/png;base64")) {
				$data["avatar_folder"] = base_url() . $this->config->item("my_avatars_folder");
			}
			if ($this->session->userdata("sex") == "b") {
				$data["is_brand"] = true;
				$data["show_brand"] = true;
			} else {
				$data["show_brand"] = false;
				$sex_array = array("m" => "", "f" => "");
				if ($data["init"]["sex"] != "") {
					$sex_array[$data["init"]["sex"]] = "  checked=\"checked\"";
				}
				$data["sex_array"] = $sex_array;
				$this->load->helper('birthdate');
				$data['birth_date_year'] = buildYearDropdown('birth_date_year', $data["init"]['birth_date_year'], 13, $this->lang->line('date_year'));
				$months_arr = array(
					'' => $this->lang->line('date_month'),
					'01' => $this->lang->line('cal_january'),
					'02' => $this->lang->line('cal_february'),
					'03' => $this->lang->line('cal_march'),
					'04' => $this->lang->line('cal_april'),
					'05' => $this->lang->line('cal_mayl'),
					'06' => $this->lang->line('cal_june'),
					'07' => $this->lang->line('cal_july'),
					'08' => $this->lang->line('cal_august'),
					'09' => $this->lang->line('cal_september'),
					'10' => $this->lang->line('cal_october'),
					'11' => $this->lang->line('cal_november'),
					'12' => $this->lang->line('cal_december')
				);
				$data['birth_date_month'] = buildMonthDropdown('birth_date_month', $data["init"]['birth_date_month'], $months_arr);
				$data['birth_date_day'] = buildDayDropdown('birth_date_day', $data["init"]['birth_date_day'], $this->lang->line('date_day'));
			}

			$data["register_type"] = "edit";
			if ($has_error) {
				$data["has_error"] = $this->lang->line("fe_register_has_error");
			}
			$data["submit_button"] = $this->lang->line("fe_profile_edit_submit");
			if ($from_post) {
//				unset($data["init"]);
			}

			$title = sprintf($this->lang->line("fe_register_edit_of"), $data["init"]["user_name"]);
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);
			$this->load->view("users/registration/register.php", $data);
			$this->load->view('main_templates/simple_footer');
			$this->load->view('main_templates/common_footer');
		}
	}

	public function logout() {
		$this->users_model->user_logout();
		$this->index();
	}

	public function testemailtemplate() {
		$this->load->view("email/tpl", array("content" => "lorem ipsum dolor sit amet"));
	}

	public function reset() {
		if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
			$redirect_to = $this->session->userdata("last_url");
		} else {
			$redirect_to = base_url();
		}
		if ($this->uri->uri_string() == "logout") {
			$this->logout();
		} else {
			$this->users_model->is_logged(true, $redirect_to);
			//$this->load->helper('cookie');
			if (!$this->session->userdata('logged_in')) {
				//AJAX OR NO AJAX
				if (IS_AJAX) {
					$ajax = array();
					$ajax["error"] = false;

					$email = filter_input(INPUT_POST, "email_address", FILTER_VALIDATE_EMAIL);

					if (!$email) {
						$ajax["error"] = true;
						$ajax["text"] = $this->lang->line('fe_contacts_error_email');
					} else {
						$ajax["error"] = $this->users_model->user_password_reset_init($email);
						if (!$ajax['error']) {
							$ajax["text"] = $this->lang->line('fe_reset_check_mail');
						} else {
							$ajax["text"] = $this->lang->line('fe_reset_mail_not_found');
						}
					}

					die(json_encode($ajax));
				} else {
					$data["redirect_to"] = $redirect_to;

					$title = $this->lang->line("fe_reset_pass");
					$data_seo = array(
						"title" => $title
					);
					$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
					$seo = $this->seo_model->make_seo($data_seo, true);
					$header_data = array_merge($header_data, $seo);
					$this->load->view('main_templates/header', $header_data);


					$this->load->view("users/reset", $data);
					$this->load->view('main_templates/simple_footer', $data);
					$this->load->view('main_templates/common_footer');
				}
			} else {
				die("already logged");
			}
		}
	}

	public function reset_do() {
		if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
			$redirect_to = $this->session->userdata("last_url");
		} else {
			$redirect_to = base_url("login");
		}
		if ($this->uri->uri_string() == "logout") {
			$this->logout();
		} else {
			$this->users_model->is_logged(true, $redirect_to);
			//$this->load->helper('cookie');
			if (!$this->session->userdata('logged_in')) {

				$this->load->model("Encrypt_model", "encr");

				//AJAX OR NO AJAX
				if (IS_AJAX) {
					$ajax = array();
					$ajax["error"] = false;

					$resethash = filter_input(INPUT_POST, "resethash");
					$password = filter_input(INPUT_POST, "password");
					$con_password = filter_input(INPUT_POST, "con_password");

					$resethash = $this->encr->decode($resethash);
					if (!$resethash) {
						$this->session->set_flashdata("reset_pass_error", $this->lang->line("fe_contacts_sent_error"));
						$ajax["error"] = true;
						$ajax["redirect"] = base_url("users/reset");
						die(json_encode($ajax));
					}
					$resethash = json_decode($resethash);   // id, email
					if (!$resethash) {
						$this->session->set_flashdata("reset_pass_error", $this->lang->line("fe_contacts_sent_error"));
						$ajax["error"] = true;
						$ajax["redirect"] = base_url("users/reset");
						die(json_encode($ajax));
					}

					$this->load->library('form_validation');
					$this->form_validation->set_rules('password', $this->lang->line('fe_register_error_password'), 'trim|required|min_length[5]|max_length[32]');
					$this->form_validation->set_rules('con_password', $this->lang->line('fe_register_error_password-repeat'), 'trim|required|matches[password]');
					if ($this->form_validation->run() == FALSE) {
						$ajax["error"] = true;
						$ajax["text"] = $this->lang->line("fe_reset_match_pass");
					} else {

						if (!$this->users_model->user_password_reset($resethash->id, $resethash->email, $password)) {
							$ajax["error"] = true;
							$ajax["text"] = $this->lang->line("fe_contacts_sent_error");
						} else {
							$this->session->set_flashdata("reset_pass_success", $this->lang->line("reset_pass_success"));
							$ajax["error"] = false;
							$ajax["redirect"] = base_url("login");
						}
					}
					die(json_encode($ajax));
				} else {
					//http://fave.voksnet.com/users/reset_do/2dMUuiKB.RYwS.V.ygms2V06N.bKNRivtoP86WKiN1hBb3yygm0r1XM0~6rR~A11fHKbEUCol7wpu8BvH.mk8Q--

					$encr_email = $this->uri->segment(3);
					$email = $this->encr->decode($encr_email);
					if (!$email) {
						$this->session->set_flashdata("reset_pass_error", $this->lang->line("fe_reset_invalid_link"));
						redirect("users/reset");
					}

					$resetinfo = $this->db->get_where("user_reset", array("email" => $email, "del" => "no", "expires > " => date("Y-m-d H:i:s")));
					if (!$resetinfo->num_rows()) {
						$this->session->set_flashdata("reset_pass_error", $this->lang->line("fe_reset_invalid_link"));
						redirect("users/reset");
					}

					$resetinfo = $resetinfo->row();

					$data["resethash"] = $this->encr->encode(json_encode(array("id" => $resetinfo->usersFK, "email" => $resetinfo->email)));
					$data["redirect_to"] = $redirect_to;

					$title = $this->lang->line("fe_reset_enter_new_pass");
					$data_seo = array(
						"title" => $title
					);
					$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
					$seo = $this->seo_model->make_seo($data_seo, true);
					$header_data = array_merge($header_data, $seo);
					$this->load->view('main_templates/header', $header_data);


					$this->load->view("users/reset_form", $data);
					$this->load->view('main_templates/simple_footer', $data);
					$this->load->view('main_templates/common_footer');
				}
			} else {
				die("already logged");
			}
		}
	}

//    password_hash($user_data["password"], PASSWORD_DEFAULT)
}

?>