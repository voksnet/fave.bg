<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct() {
		parent::__construct();
//$this->load->helper(array('form', 'url', 'file'));
		$this->load->model('post_model');
	} 

	public function index($tag = "") {
//load masonry data
//make html response for masonry
		if (trim($tag) != "" && strlen(trim($tag)) >= 3) {
			$tag = urldecode(trim($tag));
			$tag = mb_convert_case($tag, MB_CASE_LOWER, "UTF-8");
			$data["explore"] = $tag;
		} else {
			if (trim($tag) != "" && strlen(trim($tag)) < 3) {
				redirect("/", "refresh");
			}
			$data["explore"] = "";
		}
		$data["unique_id"] = uniqid();

		$featured = "";
//		$data["current_url"] = base_url(uri_string());

		$data["page_type"] = "main";
		$title = "";
		if ($tag != "") {
			$input_data["explore"] = $tag;
		}
//		$input_data["search"] = "";
//		$input_data["exclude"] = "";
//		$input_data["category"] = "";
//		$input_data["profile"] = "";
		$input_data["ui"] = $data["unique_id"];
//		$input_data["likes"] = false;
//		$input_data["my_likes"] = false;
//		$input_data["my_feed"] = false;
//		$input_data["button"] = false;
		$result = $this->post_model->get_msnry($input_data);
		$data["data"] = $result;

		if ($tag != "") {
//PENDING: tag profile is made put here SEO
			$title = "#" . $tag;
			$data_seo = array(
				"title" => $title,
//				"image" => $data["data"]["first_image"]
				"image" => base_url() . $this->lang->line("fe_seo_default_image"),
				"description" => $this->lang->line("fe_seo_site_description"),
				"image:width" => "1200",
				"image:height" => "630"
			);
//MYTODO: get tag data !!!!!!!!!!!!!!!!!!!!!!!
			$this->load->model('tags_model');
			$tag_data = $this->tags_model->get($tag);
			if ($tag_data->num_rows()) {
				$tag_data = $tag_data->row();
				if (!empty($tag_data->image)) {
					$data_seo['image'] = base_url() . "tags/" . $tag_data->image;
					$data_seo['image:width'] = "300";
					$data_seo['image:height'] = "300";
					$data['tags']['image'] = base_url() . "tags/" . $tag_data->image;
				}
				if (!empty($tag_data->description)) {
					$data_seo['description'] = $tag_data->description;
					$data['tags']['description'] = $tag_data->description;
				}
			}
		} else {
//			$input_data["explore"] = "";
			$data_seo = array(
				"image" => base_url() . $this->lang->line("fe_seo_default_image"),
				"title" => $this->lang->line("fe_seo_site_name") . " - " . $this->lang->line("fe_seo_short_description"),
				"description" => $this->lang->line("fe_seo_site_description"),
				"image:width" => "1200",
				"image:height" => "630"
			);
		}

//		if (!isset($data["data"]["none"])) {
//			$description = $data["data"]["first_short_description"];
//			$data_seo = array(
//				"image" => $data["data"]["first_image"],
//				"title" => $title,
//				"description" => $description
//			);
//		} else {
//			$data_seo = array(
//				"title" => $title
//			);
//		}

		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
		$seo = $this->seo_model->make_seo($data_seo);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);
		if (!isset($data["data"]["none"])) {
			if ($tag == "") {
				$this->make_featured();
			}
		}

		$this->load->view('index_content');
		if (empty($data["explore"])) {  // homepage
//			$data["home"] = true;
			$blog_content = $this->cache_model->get("cache.home.blog.html");
			$this->load->model("blog_model");
			$blog_content = $this->blog_model->check_liked_posts($blog_content);
			$this->load->view('blog/home', array("blog_items" => $blog_content));
		}
		$data["no_header_title"] = true;
		$this->load->view('main_templates/masonry', $data);
		$this->load->view('posts/post_content', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

	private function make_featured() {
		$input_data["featured"] = true;
		$featured = $this->post_model->get_msnry($input_data, true);
		$featured_html = "";
		if (isset($featured["posts"])) {
			foreach ($featured["posts"] as $f_item) {
				$slug = "";
				$link = base_url() . $this->config->item("my_urlparams_view") . '/' . $f_item["id"];
//makes slug from tags
				$slug = str_ireplace($f_item["cat_strings"], ",", $f_item["tags"]);
				$slug = $f_item["cat_strings"] . trim(trim($slug, ","));
//psl - post slug
				$slug = "psl/" . str_ireplace(array(",", " "), array("_", "-"), trim(trim($slug, ",")));
				$link.="/" . $slug;
				$featured_html .=
						"\n" . '<div class="item" style="background-image: url(' . base_url() . $this->config->item("my_upload_big_images") . $f_item["image"] . ')">
	<a href="' . $link . '" data-open_view="' . $f_item["id"] . '" class="open-view" data-slug="' . $slug . '"></a>';
				if (isset($f_item["short_tips"])) {
					$featured_html .=
							'	<div class="content">
		<div class="post_tips_holder">';
					$tip_count = 0;
					foreach ($f_item["short_tips"] as $the_tip) {
						$tip_count ++;
						$featured_html .= $this->post_model->make_short_tip($the_tip);
						if ($tip_count == 2) {
							break;
						}
					}
					$featured_html.=
							"\n" . '		</div>
	</div>';
				}
				$featured_html.=
						"\n" . '</div>';
			}
		}
		$this->load->view('main_templates/featured_content', array("featured" => $featured_html));
	}

	public function recent($tag = "") {
		$data["explore"] = "";
		$data["unique_id"] = uniqid();
		$featured = "";
		$data["page_type"] = "main";
		$title = "";
		if ($tag != "") {
			$input_data["explore"] = $tag;
		}
		$input_data["ui"] = $data["unique_id"];
		$input_data["recent"] = true;
		$result = $this->post_model->get_msnry($input_data);
		$data["data"] = $result;
		$data_seo = array(
			"image" => base_url() . $this->lang->line("fe_seo_default_image"),
			"title" => $this->lang->line("fe_site_menu")["recent"]["description"],
			"description" => $this->lang->line("fe_seo_site_description"),
			"image:width" => "1200",
			"image:height" => "630"
		);

		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
		$seo = $this->seo_model->make_seo($data_seo);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);
		if (!isset($data["data"]["none"])) {
			$this->make_featured();
		}

		$this->load->view('index_content');
		if (empty($data["explore"])) {  // homepage
//			$data["home"] = true;
			$blog_content = $this->cache_model->get("cache.home.blog.html");
			$this->load->model("blog_model");
			$blog_content = $this->blog_model->check_liked_posts($blog_content);
			$this->load->view('blog/home', array("blog_items" => $blog_content));
		}
		$data["page_title"] = $this->lang->line("page_title_newest_pots");
		$this->load->view('main_templates/masonry', $data);
		$this->load->view('posts/post_content', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

	public function dashboard() {
		$this->session->set_userdata('last_url', base_url(uri_string()));
		if ($this->session->userdata("logged_in")) {
			$this->load->model('tips_model');
			$title = $this->lang->line("fe_site_menu")["my_feed"]["title"];
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);

			$data["my_feed"] = true;
			$data["unique_id"] = uniqid();


			$data["page_type"] = "main";
//			$input_data["explore"] = "";
//			$input_data["search"] = "";
//			$input_data["exclude"] = "";
//			$input_data["category"] = "";
//			$input_data["profile"] = "";
			$input_data["ui"] = $data["unique_id"];
//			$input_data["likes"] = false;
//			$input_data["my_likes"] = false;
			$input_data["my_feed"] = true;
//			$input_data["button"] = false;
			$result = $this->post_model->get_msnry($input_data);
			$data["data"] = $result;


			$this->load->view('index_content');
			$this->load->view('main_templates/masonry', $data);
			$this->load->view('posts/post_content', $data);
			$this->load->view('main_templates/footer');
			$this->load->view('main_templates/common_footer');
		} else {
			redirect(base_url() . "login");
		}
	}

	public function likes() {
		$this->session->set_userdata('last_url', base_url(uri_string()));
		if ($this->session->userdata("logged_in")) {

			$title = $this->lang->line("fe_site_menu")["fave"]["title"];
			$data_seo = array(
				"title" => $title
			);
			$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
			$seo = $this->seo_model->make_seo($data_seo, true);
			$header_data = array_merge($header_data, $seo);
			$this->load->view('main_templates/header', $header_data);


			$data["my_likes"] = true;
			$data["unique_id"] = uniqid();


			$data["page_type"] = "main";
//			$input_data["explore"] = "";
//			$input_data["search"] = "";
//			$input_data["exclude"] = "";
//			$input_data["category"] = "";
//			$input_data["profile"] = "";
			$input_data["ui"] = $data["unique_id"];
//			$input_data["likes"] = false;
			$input_data["my_likes"] = true;
//			$input_data["my_feed"] = false;
//			$input_data["button"] = false;
			$result = $this->post_model->get_msnry($input_data);
			$data["data"] = $result;


			$this->load->view('index_content');
			$this->load->view('main_templates/masonry', $data);
			$this->load->view('posts/post_content', $data);
			$this->load->view('main_templates/footer');
			$this->load->view('main_templates/common_footer');
		} else {
			redirect(base_url() . "login");
		}
	}

	public function search($string = "") {
		if (trim($string) != "" && strlen(trim($string)) >= 3) {
			$string = urldecode(trim($string));
			$string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
			$data["search"] = $string;
		} else {
			if (trim($string) != "" && strlen(trim($string)) < 3) {
				redirect("/", "refresh");
			}
			$data["search"] = "";
		}

//		if ($string == "") {
//			$this->load->view('main_templates/featured_content');
//		}
//		$data["current_url"] = base_url(uri_string());
		$data["unique_id"] = uniqid();


		$data["page_type"] = "main";
//		$input_data["explore"] = "";
		$input_data["search"] = $string;
//		$input_data["exclude"] = "";
//		$input_data["category"] = "";
//		$input_data["profile"] = "";
		$input_data["ui"] = $data["unique_id"];
//		$input_data["likes"] = false;
//		$input_data["my_likes"] = false;
//		$input_data["my_feed"] = false;
//		$input_data["button"] = false;
		$result = $this->post_model->get_msnry($input_data);
		$data["data"] = $result;

		$title = sprintf($this->lang->line("fe_seo_search"), $data["search"]);
		if (isset($data["data"]["first_short_description"])) {
			$description = $data["data"]["first_short_description"];
		}
		$data_seo = array(
			"title" => $title,
//				"image" => $data["data"]["first_image"]
			"image" => base_url() . $this->lang->line("fe_seo_default_image"),
			"description" => $this->lang->line("fe_seo_site_description"),
//				"description" => $description
			"image:width" => "1200",
			"image:height" => "630"
		);

		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
		$header_data["fb_pixel"] = $this->load->view("fb_pixel/event", ['events' => ["search"], 'search' => $string], true);
		$seo = $this->seo_model->make_seo($data_seo);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);



		$this->load->view('index_content');
		$this->load->view('main_templates/masonry', $data);
		$this->load->view('posts/post_content', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

	public function cat($string = "") {
		if (trim($string) != "" && strlen(trim($string)) >= 3) {
			$string = urldecode(trim($string));
			$string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8");
			$data["cat"] = $string;
		} else {
			if (trim($string) != "" && strlen(trim($string)) < 3) {
				redirect("/", "refresh");
			}
			$data["cat"] = "";
		}
//		if ($string == "") {
//			$this->load->view('main_templates/featured_content');
//		}
//		$data["current_url"] = base_url(uri_string());
		$data["unique_id"] = uniqid();
		$data["page_type"] = "main";
//		$input_data["explore"] = "";
		$input_data["cat"] = $string;
//		$input_data["exclude"] = "";
//		$input_data["category"] = "";
//		$input_data["profile"] = "";
		$input_data["ui"] = $data["unique_id"];
//		$input_data["likes"] = false;
//		$input_data["my_likes"] = false;
//		$input_data["my_feed"] = false;
//		$input_data["button"] = false;
		$result = $this->post_model->get_msnry($input_data);
		$data["data"] = $result;

		$title = mb_convert_case($data["cat"], MB_CASE_UPPER, "UTF-8");
		if (isset($data["data"]["first_short_description"])) {
			$description = $data["data"]["first_short_description"];
		}
		$data_seo = array(
			"title" => $title,
//				"image" => $data["data"]["first_image"]
			"image" => base_url() . $this->lang->line("fe_seo_default_image"),
			"description" => $this->lang->line("fe_seo_site_description"),
//				"description" => $description
			"image:width" => "1200",
			"image:height" => "630"
		);

		$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
//$header_data["fb_pixel"] = $this->load->view("fb_pixel/event", ['events' => ["search"], 'search' => $string], true);
		$seo = $this->seo_model->make_seo($data_seo);
		$header_data = array_merge($header_data, $seo);
		$this->load->view('main_templates/header', $header_data);



		$this->load->view('index_content');
		$this->load->view('main_templates/masonry', $data);
		$this->load->view('posts/post_content', $data);
		$this->load->view('main_templates/footer');
		$this->load->view('main_templates/common_footer');
	}

	public function get_masonry($input_data = "") {
		$this->post_model->get_msnry($input_data);
	}

	public function dolike() {
		if (IS_AJAX) {
			if ($this->session->userdata('logged_in')) {
				$post_id = $this->input->post("likes_id");
				list($post_type, $post_id) = explode("_", $post_id); // {zilla_, blog_}
				if ($post_type == "blog") {
					$this->load->model("blog_model");
					$result = $this->blog_model->toggle_like($post_id);
					$this->clear_cache("cache.home.blog.html");
				} else {
					$result = $this->post_model->toggle_like($post_id);
				}
				echo json_encode($result);
			} else {
				echo json_encode(array("type" => "login"));
			}
		}
	}

	public function comingsoon() {
		$this->load->view("comingsoon");
	}

	function clear_cache($name) {
		$file = FCPATH . "cache/" . $name;
		if (file_exists($file)) {
			unlink($file);
		}
	}

}
