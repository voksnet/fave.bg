<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class alias extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('post_model');
		$this->load->model('tips_model');
	}

	public function index($alias, $type = "") {
		$alias = mb_convert_case($alias, MB_CASE_LOWER, "UTF-8");
		$alias = $this->db->escape_str($alias);
		$result = $this->users_model->alias_to_selector($alias);
		$selector = $result["selector"];
		if ($result["sex"] == "b" && $type == "") {
			$type = "catalog";
		}
		$this->users_model->alias_profile($selector, $type);
	}

}

?>