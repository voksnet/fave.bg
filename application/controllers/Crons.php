<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class crons extends CI_Controller {

	public function __construct() {
		parent::__construct();
//		$this->load->model('post_model');
//		$this->load->model('tips_model');
		$this->load->model('crons_model');
	}

	public function index($case) {
		$isCLI = ( php_sapi_name() == 'cli' );
		if ($isCLI) {
//		php index.php welcome show
			switch ($case) {
				case "wants_once":
//					$wants_array = array();
//					$this->db->select("id, usersFK, users_selectorFK, cat_strings, categoriesFK");
//					$this->db->from('posts');
//					$where_string = "del = '0' AND post_type = 'post' ";
//					$this->db->where($where_string);
//					$query = $this->db->get();
//					if ($query->num_rows() > 0) {
//						foreach ($query->result() as $row) {
//							$result["usersFK"] = $row->usersFK;
//							$result["users_selectorFK"] = $row->users_selectorFK;
//							$result["categoriesFK"] = trim($row->categoriesFK,",");
//							$result["categories_nameFK"] = trim($row->cat_strings,",");
//							$result["postsFK"] = $row->id;
//							if (stristr($result["categoriesFK"],",")) {
//								$result["categoriesFK"]  = explode(",", $result["categoriesFK"]);
//								$result["categoriesFK"] = $result["categoriesFK"][0];
//							}
//							if (stristr($result["categories_nameFK"],",")) {
//								$result["categories_nameFK"]  = explode(",", $result["categories_nameFK"]);
//								$result["categories_nameFK"] = $result["categories_nameFK"][0];
//							}
//							$wants_array[] = $result;
//						}
//					}
//					foreach ($wants_array as $wants_data) {
//						$this->db->insert('wants', $wants_data);
//					}
//					die(print_r($wants_array));
					break;
				case "base_update":
					$this->crons_model->update_counts();
					break;
				case "sitemap":
					$this->crons_model->make_sitemap();
					break;
				case "base_fix_1":
					$this->crons_model->base_fix_1();
					break;
				default:
					break;
			}
		}
	}

}

?>