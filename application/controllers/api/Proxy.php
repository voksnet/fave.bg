<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Proxy extends CI_Controller {

        public $proxydb;
    
	public function __construct() {
		parent::__construct();
                
                $this->proxydb = $this->load->database('proxydb', TRUE);

                
                $this->load->library('lib_proxy');
		$this->load->model('scrape_model');
	}
	public function get() {
            
                $this->lib_proxy->getRandom();            
                echo json_encode(['ip' => $this->lib_proxy->ip, 'port' => $this->lib_proxy->port]);
	}

}
