<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('post_model');
		$this->load->model('tips_model');
	}

	public function index($id = "") {
		if (!IS_AJAX) {
			if ($id == "") {
				show_404();
			} else {
				if (!$this->post_model->check_post_id($id)) {
//					show_404();
					redirect(base_url());
				} else {
					$data["post"] = $this->post_model->get_post($id);
					$data["id"] = $id;
					$data["view"] = true;
					$data["unique_id"] = uniqid();
					$data["exclude"] = $id;
					$data["page_type"] = "view";
					$input_data["explore"] = implode(",", $data["post"]["tags"]);
//					$input_data["search"] = "";
					$input_data["exclude"] = $id;
					$input_data["category"] = $data["post"]["category"];
//					$input_data["profile"] = "";
					$input_data["ui"] = $data["unique_id"];
//					$input_data["likes"] = false;
//					$input_data["my_likes"] = false;
//					$input_data["my_feed"] = false;
//					$input_data["button"] = false;

					$result = $this->post_model->get_msnry($input_data);
					$data["data"] = $result;


					//total_categories
					if (count($data["post"]["post_categories"]) < $this->config->item("my_max_cats_per_post")) {
						$all_categories = $this->post_model->get_all_cats();
						$post_cats = explode(",", $data["post"]["category"]);
						$select_options = "";
						foreach ($all_categories as $groups) {
							$select_options .= '<optgroup label="' . $groups["group_name"] . '">' . "\n";
							foreach ($groups["data"] as $item) {
								if (!in_array($item["cat_id"], $post_cats)) {
									$select_options .= '<option value="' . $item["cat_id"] . '">' . $item["cat_name"] . '</option>' . "\n";
								}
							}
							$select_options .= '</optgroup>' . "\n";
						}
						$data["post"]["cat_options"] = $select_options;
					}
					$data["post"]["banner"] = $this->config->item("my_adsense_responsive_unit");
//					die (print_r($data["post"]["tips"][0]["tips"]));
					$title = $data["post"]["post_categories"][0]["big_name"] . " : " . implode(" ", $data["post"]["tags"]);
					$description = "";
					if (isset($data["post"]["tips"][0]["tips"][0]["price"])) {
						$description = $data["post"]["tips"][0]["tips"][0]["price"] . " " . $this->lang->line("fe_currency")[$data["post"]["tips"][0]["tips"][0]["currency"]] . " ";
					}
					if ($data["post"]["comment"] != "") {
						if (mb_strlen($data["post"]['comment'], "utf8") > 156) {
							$description .= mb_substr(strip_tags($data["post"]['comment']), 0, 156, "utf8") . " ... ";
						} else {
							$description .= strip_tags($data["post"]['comment']);
						}
					}
					$data_seo = array(
						"image" => $data["post"]["image"],
						"title" => $title,
						"description" => $description,
						"image:width" => $data["post"]["image_width"],
						"image:height" => $data["post"]["image_height"]
					);

					$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
					$header_data["fb_pixel"] = $this->load->view("fb_pixel/event", ['events' => ["postView"], 'id' => $id], true);
					$seo = $this->seo_model->make_seo($data_seo);
					$header_data = array_merge($header_data, $seo);
					$this->load->view('main_templates/header', $header_data);
					$this->load->view('index_content');
					$this->load->view('posts/post_content', $data);
					$this->load->view('main_templates/masonry', $data);
					$this->load->view('main_templates/footer');
					$this->load->view('main_templates/common_footer');
				}
			}
		} else {
			if ($this->router->fetch_class() != "users") {
				$this->session->set_userdata('last_url', $_SERVER["HTTP_REFERER"]);
			}
			if ($id == "") {
				$data["error"] = true;
			} else {
				$post = $this->post_model->get_post($id, "", false, true);
				if (!$post) {
					$data["error"] = true;
				} else {
					$data["exclude"] = $id;
					$data["explore"] = implode(",", $post["tags"]);
					$data["category"] = $post["category"];
					$data['categories'] = $this->post_model->get_all_cats();
					$data["post"] = $post;
				}
			}
			echo json_encode($data);
		}
	}

	public function ajax() {
		//count_post_tips($post_id)
		if (IS_AJAX) {
			$result = array();
			switch ($this->input->post("action")) {
				case "addcat":
//					die(print_r($this->session->userdata()));
					if ($this->session->userdata('logged_in')) {

						$category = $this->input->post("category");
						$post_id = $this->input->post("post_id");
						$post = $this->post_model->get_post($post_id, "", false, true);
						if (count($post["post_categories"]) < $this->config->item("my_max_cats_per_post")) {
							if (!stristr($post["category"], "," . $category . ",")) {
								$new_cat = $this->post_model->get_cat("", $category);
								$this->post_model->update_post_cats($post_id, $category, $new_cat["name"]);
								if ($this->session->userdata('sex') != "b") {
									$wants_data["usersFK"] = $this->session->userdata('id');
									$wants_data["users_selectorFK"] = $this->session->userdata('selector');
									$wants_data["postsFK"] = $post_id;
									$wants_data["categoriesFK"] = $category;
									$wants_data["categories_nameFK"] = $new_cat["name"];
									$this->post_model->addWants($wants_data);
								}

								$result["category_to_remove"] = $category;
								if ((count($post["post_categories"]) + 1) < $this->config->item("my_max_cats_per_post")) {
									$result["result_action"] = "added";
								} else {
									$result["result_action"] = "added_last";
								}

								$result["tip_header"]["name"] = $new_cat["name"];
								$result["tip_header"]["big_name"] = $new_cat["big_name"];
								$result["tip_header"]["param"] = $new_cat["param"];
								$result["tip_header"]["id"] = $category;
								$result["tip_header"]["count"] = 0;
								$result["tip_header"]["wants"] = "yes";
							} else {
								$result["result_action"] = "nothing";
							}
						} else {
							$result["result_action"] = "remove_add";
						}
					} else {
						$result["login"] = true;
					}
					break;
				case "getremotetip":
					$result = $this->tips_model->getMicroData($this->input->post("url"));
					break;
				case "addtip":
					if ($this->session->userdata('logged_in')) {
						$url = $this->input->post("tip_url");

//					//check if this domain is forbidden
						$url_status = $this->tips_model->forbidden_domain($url);
						//check if url exists and if it is valid and forbidden
						if ($url_status["result"] === "invalid") {
							$result["error"]["url"] = $this->lang->line("fe_tip_error_url");
						} else if ($url_status["result"]) {
							$result["error"]["url"] = $this->lang->line("fe_tip_error_forbiden_url");
						}
						if ($url_status["url"] != $url) {
							$url = $url_status["url"];
							$result["url"] = $url;
						}
						//check url for duplication
						if ($this->tips_model->tip_duplicate($url, $this->input->post("post_id"), $this->input->post("cat_id"))) {
							$result["error"]["url"] = $this->lang->line("fe_tip_error_duplicate");
						}

						//check post_id
						if (!$this->tips_model->post_exists($this->input->post("post_id"))) {
							$result["error"]["main"] = $this->lang->line("fe_tip_error_error");
						}

						//check cat_id
						if (!$this->tips_model->cat_exists($this->input->post("cat_id"))) {
							$result["error"]["main"] = $this->lang->line("fe_tip_error_error");
						}

						//check post tip limit
						if (!isset($result["error"])) {
							$tips = $this->tips_model->check_tip_limit($this->input->post("post_id"), $this->input->post("cat_id"));
							if ($tips >= $this->config->item("my_max_tips")) {
								$result["error"]["main"] = $this->lang->line("fe_tip_error_error") . " (max tips reached)";
							}
						}

						//check currency
						if (!isset($this->lang->line("fe_currency")[$this->input->post("currency")])) {
							$result["error"]["main"] = $this->lang->line("fe_tip_error_error");
						}

						//check price
						if ($this->input->post("tip_price") == null || trim($this->input->post("tip_price")) == "" || !is_numeric(trim($this->input->post("tip_price")))) {
							$result["error"]["price"] = $this->lang->line("fe_tip_error_no_price");
						}
						if (!isset($result["error"])) {
							$data_array = $this->input->post();
							unset($data_array['action']);
							$data_array["tip_price"] = trim($data_array["tip_price"]);
							$data_array["usersFK"] = $this->session->userdata('id');
							$data_array["users_selectorFK"] = $this->session->userdata('selector');
							
							
							$result = array();
							$tip_result = $this->tips_model->add_tip($data_array);
							sort($tip_result);
							$result["new_tip"] = $tip_result[0];
						}
					} else {
						$result["login"] = true;
					}
					break;
				case "addthnx":
					if (!$this->session->userdata("logged_in")) {
						$result["login"] = true;
					} else {
						$this->tips_model->add_thnx($this->input->post("tip_id"), $this->input->post("selector"), $this->input->post("domain"));
						$result["done"] = true;
					}
					break;
				case "wants":
					if (!$this->session->userdata("logged_in")) {
						$result["answer"] = "login";
					} else {
						$wants_data["cat_id"] = $this->input->post("cat_id");
						$wants_data["cat_name"] = $this->input->post("cat_name");
						$wants_data["cat_param"] = $this->input->post("cat_param");
						$wants_data["post_id"] = $this->input->post("post_id");
						$result["answer"] = $this->post_model->toggleWants($wants_data);
//						$this->tips_model->add_thnx($this->input->post("tip_id"), $this->input->post("selector"), $this->input->post("domain"));
//						$result["done"] = true;
//						$result["answer"] = "remove";
					}
					break;
				case "deltip":
					$result = $this->tips_model->del_tip($this->input->post("tip_id"));
					break;
				case "delpost":
					$result = $this->post_model->del_post($this->input->post("post_id"));
					break;
			}
			echo json_encode($result);
		}
	}

}
