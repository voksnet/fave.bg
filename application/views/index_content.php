<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Begin Featured Articles -->
<!-- Featured Slider Section -->
<!-- Begin Sub-Header -->
<script>
	$(function () {
		$("#searchform1 .buttonicon").on("click", function () {
			if ($('#searchform1 .search_field').val().trim() != "") {
				$('#searchform1').submit();
			}
		});
		$("#searchform1").submit(function(e){
			if ($('#searchform1 .search_field').val().trim() == "") {
				e.preventDefault();
				return false;
			}
		});

	});
</script>
<div class="clear_header"></div>
<div class="sub-header">
	<div class="wrap-middle">
		<!-- Navigation Menu Categories -->
		<form id="searchform1" method="get">
			<input placeholder="<?= $this->lang->line("fe_word_search_placeholder") ?>" type="text" class="search_field">
			<div class="buttonicon"><div class="fa fa-search"></div></div>
			<!--<input type="submit" value="<?= $this->lang->line("fe_word_search") ?>" class="buttonicon" id="search_submit"><div class="clear"></div>-->
		</form>
		<script>
			$(function () {
				$('#searchform1').submit(function (e) {
					e.preventDefault();
					search_form_submit();
				});
			});

			function search_form_submit() {
				var form_action = '<?= base_url() . $this->config->item("my_urlparams_search"); ?>/';
				var search_term = $(".search_field").val().trim();
//				if (search_term.length >= 3) {
				window.location = form_action + search_term;
//				} else {
//					console.log("error");
//				}
				return false;
			}

		</script>
		<!-- Top social icons. -->
		<!--
		<ul class="top-social">
			<li><a href="#"><i class="fa fa-facebook"></i></a></li>
			<li><a href="#"><i class="fa fa-twitter"></i></a></li>
			<li><a href="#"><i class="fa fa-instagram"></i></a></li>
			<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
			<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
			<li><a href="#"><i class="fa fa-youtube"></i></a></li>
		</ul>
		-->
	</div><!-- end .wrap-middle -->
</div><!-- end .sub-header -->
