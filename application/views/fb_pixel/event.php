<?php

echo "fbq('track', 'PageView');";

if(!isset($events)) {
    $events = array();
}

if (in_array("search", $events)) {
    if (isset($search)) {
        echo "fbq('track', 'Search', {search_string: '".$search."'});";
    }
    else {
        echo "fbq('track', 'Search');";
    }
}
if (in_array("registration", $events) || ($this->session->flashdata("add_user") && $this->session->flashdata("add_user") == "complete")) {
    echo "fbq('track', 'CompleteRegistration');";
}
if (in_array("postView", $events)) {
    if (isset($postid)) {
        echo "fbq('trackCustom', 'postView', {id: '".$postid."'});";
    }
    else {
        echo "fbq('trackCustom', 'postView');";
    }
}
if (in_array("blogView", $events)) {
    if (isset($title)) {
        echo "fbq('trackCustom', 'blogView', {title: '".$title."'});";
    }
    else {
        echo "fbq('trackCustom', 'blogView');";
    }
}

////////////////////////////////////////////////////////
// javascript, bind to click events in js code
////////////////////////////////////////////////////////
if (in_array("showmore", $events)) {
    echo "fbq('trackCustom', 'showMore');";
}
if (in_array("outboundclick", $events)) {
    echo "fbq('trackCustom', 'outboundClick', {link: '<outboundlink>');";
}
if (in_array("tagclick", $events)) {
    echo "fbq('trackCustom', 'tagClick', {value: '<tagvaluehere>'});";
}
if (in_array("like", $events)) {
    echo "fbq('trackCustom', 'actionLike', {type: 'post'});";
    echo "fbq('trackCustom', 'actionLike', {type: 'blog'});";
}

?>