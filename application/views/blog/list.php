<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$post_config["my_urlparams_blog"] = $this->config->item("my_urlparams_blog");

$lang["fe_blog_i_am_reading"] = $this->lang->line("fe_blog_i_am_reading");
$lang["fe_blog_share_with_friends"] = $this->lang->line("fe_blog_share_with_friends");
?>

<?php foreach ($blogs as $blog) { ?>

	<span class="blog_post_appendor blog-list">
		<div class="wrap-fullwidth post blog-list post_main_holder">

			<div class="blog-post-image" style="background-image: url('<?= $this->config->item("cache_base_url") . "cache/blog/" . $blog->slug . ".jpg"; ?>');">
				<a href="<?= $this->config->item("cache_base_url") . "blog/" . $blog->slug; ?>" class="blog-link">&nbsp;</a>
				<div class="post-likes">
					<a target="_blank" class="facebook_share" href="https://www.facebook.com/sharer/sharer.php?u=<?= $this->config->item("cache_base_url") . "blog/" . $blog->slug; ?>"><i class="fa fa-facebook-square"></i></a>
					<a title="<?= $this->lang->line("fe_word_like_this"); ?>" class="zilla-likes active-<?= $blog->id; ?>" id="blog_<?= $blog->id; ?>" href="#">
						<span class="zilla-likes-count"><?= $blog->likes; ?></span>
					</a>
				</div>
			</div>

			<div class="header-info">

				<div class="post-tags-wrapper">
					<div class="tagcloud">
						<?php
						$tags = array_filter(explode(",", $blog->tags));
						foreach ($tags as $tag) {
							?>
							<a href="<?= base_url() . "sh/" . $tag; ?>"><?= $tag; ?></a>
	<?php } ?>
					</div>
				</div>

				<h1 class="title ellipsis"><a href="<?= base_url('blog/' . $blog->slug); ?>"><?= $blog->title; ?></a></h1>
				<h2 class="subtitle ellipsis"><a href="<?= base_url('blog/' . $blog->slug); ?>"><?= $blog->subtitle; ?></a></h2>
				<p class="content">
					<a href="<?= base_url('blog/' . $blog->slug); ?>">
						<?php
						$content = str_replace("\n", " ", strip_tags($blog->content));
						$pos = mb_strpos($content, ' ', 120);
						echo mb_substr($content, 0, $pos) . ' ...';
						?>
					</a>
				</p>
				<div class="read-more">
					<a href="<?= base_url('blog/' . $blog->slug); ?>"><?= $this->lang->line("fe_word_read_full_article"); ?></a>
					<div class="at-time"><?= $blog->date_word; ?> / <?= $blog->views_word; ?></div>
				</div>
			</div>
		</div>
	</span>

	<?php
}
?>

<div class="blog-list-pagination">
<?php echo $pagination; ?>
</div>