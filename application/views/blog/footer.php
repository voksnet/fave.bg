<div class="blog-list">
    <?php foreach ($blogs as $blog) { ?>
        <div class="blog-single">
            <a href="<?=$this->config->item("cache_base_url")."/blog/".$blog->slug;?>">
                <div class="blog-img-cover" style="background-image: url('<?=$this->config->item("cache_base_url").'/cache/blog/'.$blog->slug.'.jpg';?>');"></div>
                <div class="blog-content">
                    <h2 class="title ellipsis"><?=$blog->title;?></h2>
                    <?php if ($blog->subtitle != "") { ?>
                        <h3 class="subtitle ellipsis"><?=$blog->subtitle;?></h3>
                    <?php } ?>
                </div>
			</a>
        </div>
    <?php } ?>
</div>