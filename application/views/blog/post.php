<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$post_config["my_urlparams_blog"] = $this->config->item("my_urlparams_blog");

$lang["fe_blog_i_am_reading"] = $this->lang->line("fe_blog_i_am_reading");
$lang["fe_blog_share_with_friends"] = $this->lang->line("fe_blog_share_with_friends");
?>
<span class="blog_post_appendor">
    <div class="wrap-fullwidth post" style="display:block">
        <div class="post-tags-wrapper">
            <!--			<div class="post_closer fa fa-close"></div>-->
            <div class="tagcloud">
                <?php
                $tags = array_filter(explode(",", $post->tags));
                foreach ($tags as $tag) {
                    ?>
                    <a href="<?= $this->config->item("cache_base_url") . "sh/" . $tag; ?>"><?= $tag; ?></a>
                <?php } ?>
            </div>
        </div>
        <div class="clear"></div>
        <div class="single-content">
            <article>
                <div class="post post_main_holder" style="display:block">
                    <div class="header-info">
                        <h1 class="title"><?= $post->title; ?></h1>
                        <h2 class="subtitle"><?= $post->subtitle; ?></h2>
                        <div class="social-share">
                            <span><?= $lang["fe_blog_share_with_friends"]; ?></span>
                            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>"><i class="fa fa-facebook-official"></i></a>
                            <a target="_blank" href="https://twitter.com/intent/tweet?text=<?= $post->title; ?> <?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>"><i class="fa fa-twitter"></i></a>
                            <a data-service="pinterest" data-target="_blank" data-href="https://pinterest.com/pin/create/button/?url=<?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>&amp;media=<?= $post->image; ?>&amp;description=<?= $post->title; ?>" href="#"><i class="fa fa-pinterest"></i></a>
                            <a target="_blank" href="mailto:?subject=<?= $post->title; ?>&amp;body=<?= $lang["fe_blog_i_am_reading"]; ?> <?= $post->title; ?> <?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>"><i class="fa fa-envelope-o"></i></a>
                            <a data-action="share/whatsapp/share" data-iosurl="http://www.whatsapp.com/appstore/" data-androidurl="http://www.whatsapp.com/android/" data-appurl="whatsapp://send?text=<?= $lang["fe_blog_i_am_reading"]; ?> <?= $post->title; ?> <?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>" href="whatsapp://send?text=<?= $lang["fe_blog_i_am_reading"]; ?> <?= $post->title; ?> <?= $this->config->item("cache_base_url") . $post_config["my_urlparams_blog"] . "/" . $post->slug; ?>"><i class="fa fa-whatsapp"></i></a>
                        </div>
                    </div>

                    <div class="media-single-content">
                        <img src="<?= $this->config->item("cache_base_url") . "cache/blog/" . $post->slug . ".jpg"; ?>" alt="<?= $post->title; ?>" class="attachment-thumbnail-single-image wp-post-image" alt="">
                        <div class="post-likes">
                            <a title="<?= $this->lang->line("fe_word_like_this"); ?>" class="zilla-likes <?= $bIsLiked ? "active" : ""; ?>" id="blog_<?= $post->id; ?>" href="#">
                                <span class="zilla-likes-count"><?= $post->likes; ?></span>
                            </a>
                        </div>
                    </div>

                    <div id="author-box-single">
                        <div class="author-box">
                            <div class="at-img">
                                <a href="<?= $post->profile_url; ?>"><img width="50" height="50" class="avatar avatar-50 photo" src="<?= $post->post_avatar; ?>" alt="<?= $post->post_username; ?>"></a>
                            </div>
                            <div class="at-links">
                                <a rel="author" title="<?= $post->post_username; ?>" href="<?= $post->profile_url; ?>"><?= $post->post_username; ?></a><br>
                                <div class="at-time"><?= $post->date_word; ?> / <?= $post->views_word; ?></div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="entry">
                        <div class="entry-content">
                            <?= $post->content; ?>

                            <?php if (!empty($post->image)) { ?>
                                <div class="clearfix blog-img-container">
                                    <?php
                                    $post_images = explode("|", $post->image);
                                    foreach ($post_images as $img) {
                                        ?>
                                        <div class="blog-img">
                                            <a href="<?= $this->config->item("cache_base_url"); ?>cache/blog/<?= $img; ?>" data-lightbox="<?= $post->slug; ?>" data-title="<?= $post->title; ?>">
                                                <img src="<?= $this->config->item("cache_base_url"); ?>cache/blog/<?= $img; ?>" alt="">
                                            </a>
                                        </div>
    <?php } ?>
                                </div>
                                <link href="/assets/css/lightbox/lightbox.min.css" rel="stylesheet">
                                <script src="/assets/js/lightbox/lightbox.min.js"></script>
                                <script>
                                    $(function () {
                                        lightbox.option({
                                            'alwaysShowNavOnTouchDevices': true,
                                            'albumLabel': '<?= $post->title; ?> %1',
                                            'showImageNumberLabel': false,
                                            'disableScrolling': true,
                                            'resizeDuration': 200,
                                            'wrapAround': true
                                        });
                                    });
                                </script>
<?php } ?>

                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div> 
                </div>
            </article>
            <div style="padding:10px; margin-top: 50px;">
                <?= $this->config->item("my_adsense_responsive_unit"); ?>
            </div>
        </div>
        <aside class="sidebar-buy" style="display: block;">

            <div class="side_blog_posts">
                <ul>
                    <?php foreach ($side_posts as $idx => $blog) {
                        if ($idx == 2) {
                            echo '<li><div>'.$this->config->item("my_adsense_responsive_unit").'</div></li>';
                        }
                        ?>
                        <li>
                            <div>
                                <div class="post-likes">
                                    <a target="_blank" class="facebook_share" href="https://www.facebook.com/sharer/sharer.php?u=<?= $this->config->item("cache_base_url") . "blog/" . $blog->slug; ?>"><i class="fa fa-facebook-square"></i></a>
                                    <a title="<?= $this->lang->line("fe_word_like_this"); ?>" class="zilla-likes active-<?= $blog->id; ?>" id="blog_<?= $blog->id; ?>" href="#">
                                        <span class="zilla-likes-count"><?= $blog->likes; ?></span>
                                    </a>
                                </div>
                                <div class="blog-meta"><?= $blog->date_word; ?> / <?= $blog->views_word; ?></div>
                                <a href="<?= $this->config->item("cache_base_url") . "blog/" . $blog->slug; ?>" title="<?= $blog->title . ": " . $blog->subtitle; ?>">
                                    <img src="<?= $this->config->item("cache_base_url") . "cache/blog/" . $blog->slug . ".jpg"; ?>" alt="">
                                    <h3><?= $blog->title; ?></h3>
                                    <div class="subtitle"><?= $blog->subtitle; ?></div>
                                </a>
                            </div>
                        </li>
<?php } ?>
                    <li>
                        <a class="btn-read-more" href="<?= $this->config->item("cache_base_url")."/blog"; ?>"><?= $this->lang->line("fe_word_view_more_interesting"); ?></a>
                    </li>
                </ul>
            </div>

        </aside>
        <div class="clear"></div>
    </div>
</span>