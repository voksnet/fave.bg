<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($this->session->userdata("user_type") != null && $this->session->userdata("user_type") == "admin") {
	enviroment("development");
}

if ($this->router->fetch_class() != "users") {
	$this->session->set_userdata('last_url', base_url(uri_string()));
}
?>
<!DOCTYPE HTML>
<html lang="bg-BG">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=UTF-8" >
		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui' >
		<!--[if IE]> <script src="<?= base_url() ?>assets/js/html5.js"></script> <![endif]-->
		<link rel="shortcut icon" href="<?= base_url() ?>assets/images/favicon16.ico?v=2" type="image/x-icon">
		<link rel="icon" href="<?= base_url() ?>assets/images/favicon16.ico?v=2" type="image/x-icon">
		<link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/favicon32.png?v=2" sizes="32x32" />
		<link rel="icon" type="image/png" href="<?= base_url() ?>assets/images/favicon32.png?v=2" sizes="16x16" />
		<!-- Custom style -->
		<link rel='stylesheet' href='<?= base_url() ?>assets/css/custom.css' type='text/css' media='all' >
		<!-- Theme output -->
		<?php
		if (isset($seo_url)) {
			?>
			<link rel="canonical" content="<?= $seo_url ?>" href="<?= $seo_url ?>">
			<?php
		}
		?>
		<title><?= $page_title ?></title>
		<?php
		if (isset($page_description)) {
			?>
			<meta name="description" content="<?= $page_description ?>">
			<?php
		}
		?>
		<?= isset($og) ? $og : "" ?>
		<link rel='stylesheet' href='<?= base_url() ?>assets/css/main.css' type='text/css' media='all' >
		<link rel='stylesheet' href='<?= base_url() ?>assets/css/some_2.css' type='text/css' media='all' >
		<link rel='stylesheet' href='<?= base_url() ?>assets/fonts/css/font-awesome.css' type='text/css' media='all' >
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
		<script type='text/javascript' src='<?= base_url() ?>assets/js/jquery-migrate.min.js'></script>
		<script type='text/javascript' src='<?= base_url() ?>assets/js/text_helper.js'></script>
		<script src="<?= base_url() ?>assets/js/modernizr.js"></script>
		<script>
            $(function () {
                $('.profile_follow, .follow_follower').on('click', function (event) {
                    var that = $(event.currentTarget);
                    var this_selector = "";
                    var button_selector = "";
                    var user_selector = "";
                    if (that.hasClass("profile_follow")) {
                        this_selector = ".profile_follow";
                        user_selector = that.data("selector");
                        button_selector = this_selector;
                    } else {
                        this_selector = ".follow_follower";
                        user_selector = that.data("follow_selector");
                        button_selector = "#follow_" + user_selector;
                    }
                    $.ajax({
                        url: '<?= base_url() ?>dofollow',
                        dataType: 'json',
                        type: 'post',
                        //follow_follower
                        //data-follow_selector

                        //profile_follow
                        //data-selector
                        data: {selector: user_selector}
                    })
                            .done(function (result) {
                                if (result["type"] == "login") {
                                    //redirecto to login
                                    window.location.replace(baseUrl + "login");
                                } else {
                                    if (result["type"] == "remove") {
                                        $(that).removeClass('followed');
                                    } else {
                                        $(that).addClass('followed');
                                    }
//									if (this_selector == ".profile_follow") {	
//										$(".user_followers").html(result["value"]);
//									}
                                }
                            });
                    return false;
                });
            });
//			window.setInterval(function(){alert("here")},1000);
            function scrol_to(selector, offset) {
                if (offset === undefined) {
                    offset = 0;
                }
                $('html, body').animate({
                    scrollTop: $(selector).offset().top + offset
                }, 300);
            }
            ;
            function isLogged(action, event) {
                if (action != undefined) {
                    $.ajax({
                        url: baseUrl + 'users/ajax',
                        dataType: 'json',
                        type: 'post',
                        data: {action: "is_logged"},
                    }).done(function (result) {
                        if (result["login"] && action != undefined && action != "") {
                            window.location.replace(baseUrl + "login");
                        } else {
                            switch (action) {
                                case "post_a_tip_click":
                                    if (event != undefined) {
                                        post_a_tip_click(event);
                                    }
                                    break;
                                case "addCatToggle":
                                    addCatToggle();
                                    break;
                            }
                        }
                    });
                }
                return false;
            }

            controler = "";
            sub_controler = "";
            featured = false;
            show_url = "<?= $this->config->item("my_urlparams_show"); ?>";
            view_url = "<?= $this->config->item("my_urlparams_view"); ?>";
            upload_url = "<?= $this->config->item("my_urlparams_upload") ?>";
            baseUrl = "<?= base_url(); ?>";
            var bhittani_plugin_kksr_js = {};
            var zilla_likes = {
                "ajaxurl": "<?= base_url() ?>main/dolike"
            };
            var hash_process = false;


            $(function () {
                $("body").on("click", ".post_author_delete .fa-times", function (e) {
                    var modalTinyBtn = new tingle.modal({
                        footer: true
                    });
                    var that = $(this);
                    var id = that.attr('data-post_del');
                    var masonry_holder = that.attr('data-mason');
                    var post_image = "";
                    if ($(".post_" + id + " .open-view img").length) {
                        post_image = '<br><img src="' + $(".post_" + id + " .open-view img").attr("src") + '" style="max-width: 210px;" alt="">';
                    }
                    modalTinyBtn.setContent("<?= $this->lang->line("fe_delete_confirm") ?>" + post_image);
                    modalTinyBtn.setFooterContent("");
                    modalTinyBtn.addFooterBtn('<?= $this->lang->line("fe_delete_confirm_yes") ?>', 'tingle-btn tingle-btn--danger tingle-btn--pull-right', function () {
                        $.ajax({
                            url: baseUrl + 't/ajax',
                            dataType: 'json',
                            type: 'post',
                            data: {post_id: id, action: "delpost"},
                        }).done(function (result) {
                            if (result["login"]) {
                                window.location.replace(baseUrl + "login");
                            } else {
                                if (result["done"]) {
                                    masonries[masonry_holder].msnry.masonry("remove", $(".post_" + id));
                                    masonries[masonry_holder].msnry.masonry("layout");
                                }
                            }
                        });
                        modalTinyBtn.close();
                    });
                    modalTinyBtn.addFooterBtn('<?= $this->lang->line("fe_delete_confirm_no") ?>', 'tingle-btn tingle-btn--primary tingle-btn--pull-right', function () {
                        modalTinyBtn.close();
                    });
                    modalTinyBtn.open();
                });
            });
		</script>
		<script type='text/javascript' src='<?= base_url() ?>assets/js/something.js'></script>
		<script type='text/javascript'>
            $(function () {
                if (Modernizr.touchevents) {
                    $("body").removeClass("no-touch");
                } else {
                    $("body").addClass("no-touch");
                }
            });
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
		<link rel='shortlink' href='<?= base_url() ?>' />
		<link rel='stylesheet' href='<?= base_url() ?>assets/css/index.css' type='text/css' media='all' />
	</head>
	<body>
		<!-- Facebook Pixel Code -->
		<script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq)
                    return;
                n = f.fbq = function () {
                    n.callMethod ?
                            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq)
                    f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window,
                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1158092437546759');
<?= isset($fb_pixel) ? $fb_pixel : $this->load->view("fb_pixel/event", '', true); ?>
		</script>
		<noscript><img src="https://www.facebook.com/tr?id=1158092437546759&ev=PageView&noscript=1" height="1" width="1" style="display:none" alt=""></noscript>
		<!-- End Facebook Pixel Code -->
		<?= $this->config->item("my_analytics") ?>
		<?= $this->config->item("fb_social_plugin") ?>
		<script>
            //ios cache fix
            //MYTODO: !!! detect ios
            var last_post_image = "";
            $(function () {
                window.setInterval(function () {
                    if ((controler == "" || controler == "profile") && $(".the_post_holder .media-single-content").length) {
                        $(".the_post_holder .media-single-content").imagesLoaded().progress(
                                function (instance, image) {
                                    var image = image.img.src;
                                    if (last_post_image != image) {
                                        var main_scroll = $(document).scrollTop();
                                        $(document).scrollTop(main_scroll - 1);
                                        last_post_image = image;
                                        //										$("#counter").html(image);
                                    }
                                }
                        );
                    }

                }, 500);
            })


		</script>

		<header class="sticky">
			<div id="click-menu">
				<div class="threeLines" id="RM3Lines">
					<div class="line"></div>
					<div class="line"></div>
					<div class="line"></div>
					<div id="counter"></div>
				</div>
			</div>
			<script>
                var base_url = "<?= base_url() ?>";
			</script>
			<script src="<?= base_url() ?>assets/js/menus.js" type="text/javascript"></script>
			<div class="main-header">
				<div class="sticky-on">
					<div class="logo_holder">
						<a href="<?= base_url() ?>"><img class="logo" src="<?= base_url() ?>assets/images/logo.png" alt="<?= $this->lang->line("fe_seo_site_name") ?>"/></a>
					</div>
					<nav id="myjquerymenu" class="jquerycssmenu">
						<?= $menu["header"] ?>
					</nav>

					<div class="uploadbtn">
						<a href="<?= base_url() . $this->config->item("my_urlparams_upload") ?>" class="simplebtn"><i class="fa fa-cloud-upload"></i> <span><?= $this->lang->line("fe_word_upload") ?></span></a>
					</div>
					<?php
					if ($this->session->userdata('logged_in')) {
						//die(print_r( $this->session->userdata() ));
						?>
						<div class="profilebtn">
							<a href="<?= $this->session->userdata('profile_url') ?>" class="simplebtn"><i class="fa fa-user"></i> <span><?= $this->lang->line("fe_word_profile") ?></span></a>
						</div>
					<?php } ?>

					<?php if (!$this->session->userdata('logged_in')) { ?>
						<div class="profilebtn">
							<a href="<?= base_url() ?>signup" class="simplebtn"><i class="fa fa-user-plus"></i> <span><?= $this->lang->line("fe_word_signup") ?></span></a>
						</div>
						<div class="profilebtn">
							<a href="<?= base_url() ?>login" class="simplebtn"><i class="fa fa-sign-in"></i> <span><?= $this->lang->line("fe_word_login") ?></span></a>
						</div>
					<?php } else { ?>
						<div class="profilebtn message">
							<a href="<?= base_url() ?>signup" class="simplebtn"><i class="fa fa-envelope-o"></i></a>
							<div class="new_messages"></div>
						</div>
						<div class="profilebtn">
							<a href="<?= base_url() ?>logout" class="simplebtn"><i class="fa fa-sign-out"></i> <span><?= $this->lang->line("fe_word_logout") ?></span></a>
						</div>
					<?php } ?>

				</div>
				<div id="responsive-menu">
					<div>
						<?= $menu["header"] ?>
					</div>
				</div>
			</div>
		</header>
		<div class="clear"></div>
		<main>

