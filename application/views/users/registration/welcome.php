<div class="clear_header"></div>
<div class="wrap-fullwidth the_content">

	<div class="single-content full">


		<div class="register-container welcome">

			<div class="title"><h1><?= $this->lang->line('fe_register_push_1') ?></h1></div>
			<br>
			<div class="social-login facebook">
				<a href="<?= $login_url ?>"><?= $this->lang->line('fe_register_facebook') ?></a>
			</div>


			<script src="https://apis.google.com/js/api:client.js"></script>
			<script src="<?= base_url(); ?>assets/js/gm_login.js"></script>

			<div id="gSignInWrapper">
				<div id="gmailLogin" class="customGPlusSignIn"><?= $this->lang->line('fe_register_gmail') ?></div>
			</div>
			<div id="name"></div>
			<script>gmailLogin("<?= $this->config->item("my_google_id") ?>", "");</script>

			<div class="ml_register">
				<a href="<?= base_url()?>signup/ml" class="to-action"><?= $this->lang->line('fe_register_email') ?></a>
			</div>
			<div class="login">
				<?= $this->lang->line('fe_register_already') ?> <a href="<?= base_url()?>login"><?= $this->lang->line('fe_login') ?></a>
			</div>
		</div>

	</div><!-- end .single-content -->
	<div class="clear"></div>
</div>