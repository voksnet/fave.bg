<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Begin Home Full width -->
<script>
	controler = "profile";
//	var last_masonry_type = "";
	var add_data = {};
	$(function () {
		var type = window.location.hash.substr(1).trim();
		if (type == "uploaded") {
			$(".infinite-articles .open-view").imagesLoaded(function () {
				$('html, body').animate({
					scrollTop: $(".top_uploads").offset().top - 50
				}, 100)
			})
		}
	})
//	$(window).on('hashchange', function () {
//		hashChanged();
//	});

//	function hashChanged() {
//		var type = window.location.hash.substr(1).trim();
//		if (type === "" || type == "uploaded") {
//			if (type == "uploaded") {
//				go_to_uploaded = true;
//			}
//			type = "wants";
//		}
//		;
//		sub_controler = type;
//		$("#likes, #wants").removeClass("selected");
//		if (type == "likes") {
//			add_data = {
//				likes: 1
//			}
//			$("#likes").addClass("selected");
//		} else {
//			$("#wants").addClass("selected");
//			add_data = {
//				likes: 0
//			}
//		}
//		if (type == "likes" || type == "wants") {
//			$(".follow_wrapper").remove();
//			if (last_masonry_type != type && last_masonry_type != "") {
//				location.reload();
//			} else {
//				masonryInit("main", add_data);
//			}
//			last_masonry_type = type;
//		} else {
//			$("#likes").removeClass("selected");
//			$("#wants").removeClass("selected");
//			$(".masonry_wrapper").remove();
//			$(".post_wrapper").remove();
//			$(".wrap-fullwidth.followers").show();
//
//
//			if (type == "followers") {
//				get_followers();
//				$(".follow_header").html("<?= $this->lang->line("fe_word_followers_header") ?>");
//			} else {
//				get_following();
//				$(".follow_header").html("<?= $this->lang->line("fe_word_follows_header") ?>");
//			}
//		}
//	}
</script>

<div class="wrap-fullwidth the_content profile">
	<div class="profile_holder">
		<div class="profile_avatar">
			<img src="<?= $info["avatar"] ?>" alt="<?= $info["username"] ?>">
		</div>
		<div class="user_info">
			<div><?= $info["username"] ?></div>
			<?php
			$small_info = "";
			if ($info["sex"] != "b") {
				if ($info["sex"] != "") {
					$small_info.=$this->lang->line("fe_sex_" . $info["sex"]);
					if ($info["age"] != "") {
						$small_info.=", ";
					}
				}
				if ($info["age"] != "") {
					$small_info.=$info["age"] . " " . $this->lang->line("fe_word_years");
				}
			} else if ($info["brand_name"]!="" && $info["website"]!="" ){
				if ($info["brand_name"] != $info["username"]) {
					$small_info.=$info["brand_name"] ."<br>";
				}
				//MYTODO: !!! tracking and affiliate link here
				$small_info.='<a href="'.$info["website"].'" target="_blank">'.$info["website"].'</a>';
			}
			?>
			<div><?= $small_info ?></div>
			<div>
				<?php
				if ($info["about"] != "") {
					echo $info["about"];
				} else if (isset($current_user_selector) && $current_user_selector == $info["selector"]) {
					echo '<a href="' . base_url() . $this->config->item("my_urlparams_myaccount") . '" class="empty_about">' . $this->lang->line("fe_word_more_about_you") . '</a>';
				}
				?>
			</div>

		</div>
		<div class="user_stat">
			<div class="user_stat_row">
				<div class="user_stat_row_title"><a href="<?= $info["profile_url"] ?>/followers" id="followers"><?= $this->lang->line("fe_word_followers") ?>: <strong  class="user_followers"><?= $info["followers"] ?></strong></a></div>
				<?php
				if (isset($info["followers_data"])) {
					?>
					<div class="user_stat_row_avatars">
						<ul>
							<?php
							foreach ($info["followers_data"] as $f_data) {
								?>
								<li><a href="<?= $f_data["profile_url"] ?>" alt="<?= $f_data["username"] ?>"><img src="<?= $f_data["avatar"] ?>" title="<?= $f_data["username"] ?>"></a></li>
								<?php
							}
							?>
						</ul>
					</div>
					<?php
				}
				?>
			</div>
			<div class="user_stat_row">
				<div class="user_stat_row_title"><a href="<?= $info["profile_url"] ?>/following" id="following"><?= $this->lang->line("fe_word_follows") ?>: <strong><?= $info["follows"] ?></strong></a></div>
				<?php
				if (isset($info["follows_data"])) {
					?>
					<div class="user_stat_row_avatars">
						<ul>
							<?php
							foreach ($info["follows_data"] as $f_data) {
								?>
								<li><a href="<?= $f_data["profile_url"] ?>" alt="<?= $f_data["username"] ?>"><img src="<?= $f_data["avatar"] ?>" title="<?= $f_data["username"] ?>"></a></li>
								<?php
							}
							?>
						</ul>
					</div>
					<?php
				}
				?>
			</div>
			<div class="user_stat_row">
				<div class="user_stat_row_title"><?= $this->lang->line("fe_word_rating") ?>: <strong><?= $info["rating"] ?></strong></div>
				<div class="user_stat_row_title">, <!--<a href="<?= $info["profile_url"] ?>/helps" id="helps">--><?= $this->lang->line("fe_word_tips") ?>: <strong><?= $info["tips"] ?></strong><!--</a>--></div>
			</div>
			<?php
			if ($info["followed"]) {
				$follow_class = " followed";
			} else {
				$follow_class = "";
			}
			if (isset($current_user_selector) && $current_user_selector == $info["selector"]) {
				?>
				<div class="edit_profile_btn">
					<a href="<?= base_url() . $this->config->item("my_urlparams_myaccount") ?>" id="edit_profile"><?= $this->lang->line("fe_word_edit_profile") ?></a>
				</div>
			<?php } else {
				?>
				<div class="profile_follow_holder">
					<div class="profile_follow<?= $follow_class ?>" data-selector="<?=$info["selector"]?>"><span class="follow"><?= $this->lang->line("fe_word_you_follow") ?></span><span class="is_followed"><?= $this->lang->line("fe_word_you_follows") ?></span></div>
				</div>
				<?php
			}
			?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php 
if ($info["sex"] != "b") {
	$wants = "wants";
} else {
	$wants = "catalog";
}
$column_class = "";
if ($info["has_sponsored"]) {
	$column_class = " columns_3";
}
?>
<div class="wrap-fullwidth  the_content profile line_second">
	<div class="profile_holder_sub">
		<div class="profile_holder_sub_links<?= $column_class ?>"><span><a href="<?= $info["profile_url"] ?>/<?= $wants ?>"<?php if ($page_sub_type == "wants" || $page_sub_type == "catalog") { ?> class="selected"<?php } ?>><?= $this->lang->line("fe_word_" . $wants) ?></a></span><span><a href="<?= $info["profile_url"] ?>/likes"<?php if ($page_sub_type == "likes") { ?> class="selected"<?php } ?>><?= $this->lang->line("fe_word_likes") ?></a></span><?php if ($info["has_sponsored"]) { ?><span><a href="<?= $info["profile_url"] ?>/sponsored"<?php if ($page_sub_type == "sponsored") { ?> class="selected"<?php } ?>><?= $this->lang->line("fe_word_profile_sponsored") ?></a></span><?php } ?></div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
