<!--<div class="wrap-fullwidth">
	<div class="single-content full">
<?php if (isset($error)): ?>
					<div style="color: red">
	<?php echo $error; ?>
					</div>
<?php endif; ?>

<?php echo form_open(); ?>

		<label for="username">Username: </label>
		<input type="text" name="username" id="username" />

		<label for="password">Password: </label>
		<input type="password" name="password" id="password" />
		<label for="remember">Remember me: </label>
		<input type="checkbox" name="remember" value="1" id="remember" />

		<input type="submit" value="login" />
		</form>
	</div> end .single-content 
	<div class="clear"></div>
</div>-->
<div class="clear_header"></div>
<div class="wrap-fullwidth the_content">
	<div class="single-content full">
		<div id="content">
			<div class="register-container ml">
				<h1><?= $this->lang->line('fe_login'); ?></h1>
				<br>

				<? if (isset($login_url) && $login_url != "") { ?>
					<div class="social-login facebook">
						<a href="<?= $login_url ?>"><?= $this->lang->line('fe_login_facebook') ?></a>
					</div>
				<? } ?>


				<script src="https://apis.google.com/js/api:client.js"></script>
				<script src="<?= base_url(); ?>assets/js/gm_login.js"></script>

				<div id="gSignInWrapper">
					<div id="gmailLogin" class="customGPlusSignIn"><?= $this->lang->line('fe_login_gmail') ?></div>
				</div>
				<div id="name"></div>
				<script>gmailLogin("<?= $this->config->item("my_google_id") ?>", "<?= $redirect_to ?>");</script>

				<div class="row-wrapper or"><?= $this->lang->line('fe_word_or') ?></div>

                                
                                <?php if ($this->session->flashdata("reset_pass_success")) {
                                    echo '<div class="status success">'.$this->session->flashdata("reset_pass_success").'</div>';
                                } else { ?>
                                    <div class="status"></div>
                                <?php } ?>
                                    
                                
				<?php
				$attributes = array('id' => 'frm_login');
				//echo form_open('email/send', $attributes);
				echo form_open("", $attributes);
				?>
				<div class="row-wrapper">
					<label for="email_address"><?= $this->lang->line('fe_email'); ?>:<span>*</span></label>
					<div class="input-wrapper">
						<input type="text" id="email_address" name="email_address" maxlength="40" value="<?php echo set_value('email_address'); ?>" />
					</div>
				</div>
				<div class="row-wrapper">
					<label for="password"><?= $this->lang->line('fe_password'); ?>:<span>*</span></label>
					<div class="input-wrapper">
						<input type="password" id="password" name="password" maxlength="32" value="<?php echo set_value('password'); ?>" />
					</div>
				</div>
				<div class="row-wrapper clearfix forgot-pass-container">
                                    <div>
                                        <input id="sign-in-remember-me" name="remember_me" type="checkbox" checked="checked">
                                        <label for="sign-in-remember-me"><?= $this->lang->line('fe_remember_me'); ?></label>
                                    </div>
                                    <div>
                                        <a href="<?=base_url("users/reset");?>"><?=$this->lang->line("fe_forgot_pass");?></a>
                                    </div>
				</div>
				<div class="row-wrapper center">
                                    <button type="submit" id="submit-register"><?= $this->lang->line('fe_login_submit'); ?></button>
				</div>
                                
                                <div class="row-wrapper_disclaimer center">
                                    <div>
                                        <?=$this->lang->line("fe_register_notyet");?>
                                    </div>
                                    <div>
                                        <a href="<?=base_url("signup");?>"><b><?=strip_tags($this->lang->line("fe_register_push_1"));?></b></a>
                                    </div>
                                </div>
				<?php echo form_close(); ?>
			</div>
		</div>
		<script>
			jQuery(document).ready(function () {
				jQuery("#frm_login").submit(function (e) {
					e.preventDefault();
                                        
                                        $("#submit-register").attr("disabled", "disabled").data("value", $("#submit-register").html()).html('<i class="fa fa-spin fa-spinner"></i> <?=$this->lang->line("fe_please_wait");?>...');
                                        
					var url = jQuery(this).attr('action');
					var method = jQuery(this).attr('method');
					var data = jQuery(this).serialize();

                                        jQuery(".status").html('').removeClass("error success");

					jQuery.ajax({
						url: url,
						type: method,
						data: data
					}).done(function (data) {
						if (data !== '')
						{
							var data_obj = jQuery.parseJSON(data);
							if (data_obj.error !== undefined) {
								jQuery(".status").addClass("error").html(data_obj.error);
                                                                
                                                                $("#submit-register").html( $("#submit-register").data("value") ).removeAttr("disabled");
							}
							if (data_obj.logged) {
								window.location.href = data_obj.redirect;
							}
						} else {
							//window.location.href = '<?php echo base_url() ?>';
							//throw new Error('go');
						}
					});
				});

				jQuery("div").each(function (index) {
					var cl = jQuery(this).attr('class');
					if (cl == '')
					{
						jQuery(this).hide();
					}
				});

			});
        </script>


	</div>
	<div class="clear"></div>
</div>