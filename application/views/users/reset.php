<!--<div class="wrap-fullwidth">
        <div class="single-content full">
<?php if (isset($error)): ?>
                                            <div style="color: red">
    <?php echo $error; ?>
                                            </div>
<?php endif; ?>

<?php echo form_open(); ?>

                <label for="username">Username: </label>
                <input type="text" name="username" id="username" />

                <label for="password">Password: </label>
                <input type="password" name="password" id="password" />
                <label for="remember">Remember me: </label>
                <input type="checkbox" name="remember" value="1" id="remember" />

                <input type="submit" value="login" />
                </form>
        </div> end .single-content 
        <div class="clear"></div>
</div>-->
<div class="clear_header"></div>
<div class="wrap-fullwidth the_content">
    <div class="single-content full">
        <div id="content">
            <div class="register-container ml">
                <h1><?= $this->lang->line('fe_reset_pass'); ?></h1>
                <div class="row-wrapper center">
                    <i><?= $this->lang->line('fe_reset_pass_enter_mail'); ?></i>
                </div>
                <br />
                <?php if ($this->session->flashdata("reset_pass_error")) {
                    echo '<div class="status error">'.$this->session->flashdata("reset_pass_error").'</div>';
                } else { ?>
                    <div class="status"></div>
                <?php } ?>

                <?php
                $attributes = array('id' => 'frm_resetpass');
                //echo form_open('email/send', $attributes);
                echo form_open("", $attributes);
                ?>
                <div class="row-wrapper">
                    <label for="eamil"><?= $this->lang->line('fe_email'); ?>:<span>*</span></label>
                    <div class="input-wrapper">
                        <input type="text" id="email" name="email_address" maxlength="32" value="<?php echo set_value('email'); ?>" />
                    </div>
                </div>
                <div class="row-wrapper center">
                    <button type="submit" id="submit-register"><?= $this->lang->line('fe_reset_send_link'); ?></button>
                </div>

                <div class="row-wrapper_disclaimer center">
                    <div>
                        <?= $this->lang->line("fe_register_notyet"); ?>
                    </div>
                    <div>
                        <a href="<?= base_url("signup"); ?>"><b><?= strip_tags($this->lang->line("fe_register_push_1")); ?></b></a>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <script>
            jQuery(document).ready(function () {
                jQuery("#frm_resetpass").submit(function (e) {
                    
                    $("#submit-register").attr("disabled", "disabled").data("value", $("#submit-register").html()).html('<i class="fa fa-spin fa-spinner"></i> <?=$this->lang->line("fe_please_wait");?>...');
                    
                    e.preventDefault();
                    var url = jQuery(this).attr('action');
                    var method = jQuery(this).attr('method');
                    var data = jQuery(this).serialize();
                    
                    jQuery.ajax({
                        url: url,
                        type: method,
                        data: data
                    }).done(function (data) {
                        
                        jQuery(".status").html('').removeClass("error success");
                        
                        if (data !== '') {
                            var data_obj = jQuery.parseJSON(data);
                            jQuery(".status").addClass(data_obj.error === true ? "error" : "success").html(data_obj.text);

                        } else {
                            jQuery(".status").addClass(".error").html('<?=$this->lang->line("fe_contacts_sent_error");?>');
                        }
                        
                        $("#submit-register").html( $("#submit-register").data("value") ).removeAttr("disabled");
                        $("#frm_resetpass")[0].reset();
                    });
                });

                jQuery("div").each(function (index) {
                    var cl = jQuery(this).attr('class');
                    if (cl == '')
                    {
                        jQuery(this).hide();
                    }
                });

            });
        </script>


    </div>
    <div class="clear"></div>
</div>