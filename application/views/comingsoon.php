<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie ie6"> <![endif]--> 
<!--[if IE 7 ]>	<html lang="en" class="ie ie7"> <![endif]--> 
<!--[if IE 8 ]>	<html lang="en" class="ie ie8"> <![endif]--> 
<!--[if IE 9 ]>	<html lang="en" class="ie ie9"> <![endif]--> 
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Fave.bg</title>
        <meta name="description" content="">
        <meta http-equiv="X-UA-Compatible" content="chrome=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" >
        <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui' >
	<meta property="fb:app_id" content="1151140538265910" >
        <meta property="og:site_name" content="Fave.bg" />
        <meta property="og:title" content="Fave.bg" />
        <meta property="og:description" content="Очаквайте скоро... новата модна краудсорсинг платформа!" />
        <meta property="og:image" content="<?= base_url(); ?>assets/images/big_logo.png" />
        <meta property="og:locale" content="bg_BG" />
        <meta property="og:url" content="<?= base_url(); ?>" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />

        <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/favicon.ico" type="image/x-icon">
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">

        <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=PT+Sans+Narrow:regular,bold"> 
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/comingsoon/styles.css">
    </head>

    <body id="home">
		<script>
			(function (i, s, o, g, r, a, m) {
				i['GoogleAnalyticsObject'] = r;
				i[r] = i[r] || function () {
					(i[r].q = i[r].q || []).push(arguments)
				}, i[r].l = 1 * new Date();
				a = s.createElement(o),
						m = s.getElementsByTagName(o)[0];
				a.async = 1;
				a.src = g;
				m.parentNode.insertBefore(a, m)
			})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

			ga('create', 'UA-80868549-1', 'auto');
			ga('send', 'pageview');

		</script>
        <section class="main">
            <div id="Content" class="wrapper topSection">
                <div id="Header">
                    <div class="wrapper">
                        <div class="logo"><h1><img src="<?= base_url(); ?>assets/images/logo.png" /></h1>	</div>
                    </div>
                </div>
                <h2>Очаквайте скоро... новата модна краудсорсинг платформа!</h2> 	
                <div class="countdown styled"></div>
            </div>
        </section>

        <!--Scripts-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script> 
        <script type="text/javascript" src="<?= base_url(); ?>assets/comingsoon/jquery.countdown.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/comingsoon/global.js"></script>

    </body>
</html>
