<div class="wrap-content the_content">
    <div class="wrap-fullwidth post the_post_holder" style="display: block;">
        <div class="single-content">
            <article>
                <div class="static-page-content">
                    <?php if (isset($data->title) && !empty($data->title)) { ?>
                        <h1 class="title"><?= $data->title; ?></h1>
                    <?php } ?>
                    <?php if (isset($data->subtitle) && !empty($data->subtitle)) { ?>
                        <h2 class="subtitle"><?= $data->subtitle; ?></h2>
                    <?php } ?>
                    <?= $data->content; ?>
                </div>
            </article>
        </div>
        <aside class="sidebar-buy" style="display: block;">
            <ul class="grid_list">
				<?php
				if (isset($data->posts)) {
					foreach ($data->posts as $a_post) {
						echo $a_post;
					}
				}
				?>
			</ul>
        </aside>
        <div class="clear"></div>

    </div>

    <div class="static-page-footer">
        <i class="fa fa-question-circle"></i>
        <div>
            <h5><?= $this->lang->line("static_footer_h1"); ?></h5>
            <p><?= $this->lang->line("static_footer_h2"); ?></p>
            <p><i class="fa fa-envelope"></i>&nbsp;<?= $this->config->item("my_emails_support"); ?></p>
        </div>
		</footer>
	</div>