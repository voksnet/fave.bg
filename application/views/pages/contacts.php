<div class="static-contacts">
    <div class="contacts-general">
        
        <?php
        $sections = $this->lang->line("contacts_sections");
        foreach ($sections as $s) {
            echo '<div>'.$s['title'].'</div>';
            echo '<div>'.$s['content'].'</div>';
        }
        ?>
        
    </div>
    <div class="contacts-container ml">
        <form accept-charset="utf-8" method="post" id="frm_contacts" action="http://fave.voksnet.com/login">
            <div class="row-wrapper">
                <label for="first_name"><?=$this->lang->line("fe_register_name");?>:<span>*</span></label>
                <div class="input-wrapper">
                    <input type="text" value="" maxlength="40" name="first_name" id="first_name">
                    <input type="text" value="" maxlength="40" name="last_name" id="last_name">
                    <span class="error" style="display: none"></span>
                </div>
            </div>
            <div class="row-wrapper">
                <label for="email_address"><?=$this->lang->line("fe_email");?>:<span>*</span></label>
                <div class="input-wrapper">
                    <input type="text" value="" maxlength="40" name="email_address" id="email_address">
                    <span class="error" style="display: none"></span>
                </div>
            </div>
            <div class="row-wrapper">
                <label for="message"><?=$this->lang->line("fe_message");?>:<span>*</span></label>
                <div class="input-wrapper description">
                    <textarea name="message" id="message"></textarea>
                    <span class="error" style="display: none"></span>
                </div>
            </div>

            <div class="row-wrapper center">
                <button type="submit" id="submit-send"><?=$this->lang->line("fe_word_send");?></button>
                <div class="submit-status" style="display: none; margin-top: 25px;"></div>
            </div>
        </form>
    </div>
</div>

<script>
    $(function() {
        $("#submit-send").click(function(e) {
            e.preventDefault();
            
            $(".submit-status").slideUp("fast");
            
            if ($.trim($("#first_name").val()) == "") {
                $("#first_name").siblings(".error").html("<?=$this->lang->line("fe_contacts_error_name");?>").slideDown("fast");
            }
            else {
                $("#first_name").siblings(".error").slideUp("fast");
            }
            if ($.trim($("#email_address").val()) == "") {
                $("#email_address").siblings(".error").html("<?=$this->lang->line("fe_contacts_error_email");?>").slideDown("fast");
            }
            else {
                $("#email_address").siblings(".error").slideUp("fast");
            }
            if ($.trim($("#message").val()) == "") {
                $("#message").siblings(".error").html("<?=$this->lang->line("fe_contacts_error_email");?>").slideDown("fast");
            }
            else {
                $("#message").siblings(".error").slideUp("fast");
            }
            
            if ($("#frm_contacts .row-wrapper:not(:last) .error:visible").length) {
                return false;
            }
            
            $.ajax({
                url: '<?=base_url("pages/do_contact");?>',
                type: "POST",
                dataType: 'json',
                async: false,
                beforeSend: function() {
                    $("#submit-send").data("value", $("#submit-send").html() ).html('<i class="fa fa-spin fa-spinner"></i> <?=$this->lang->line("fe_please_wait");?>...');
                },
                data: {"first_name": $("#first_name").val(), "last_name": $("#last_name").val(), "email": $("#email_address").val(), "message": $("#message").val()},
                error: toggle_form_error,
                success: function(data, textStatus, jqXHR) {
                    if (typeof(data.error) != "undefined" && data.error === true) {
                        toggle_form_error(false, false, data.text);
                        return;
                    }
                    
                    $("#frm_contacts")[0].reset();
                    
                    $("#submit-send").slideUp("fast", function() {
                        $("#submit-send").siblings(".submit-status").removeClass("error").addClass("ap-post-submission-message").html('<?=$this->lang->line("fe_contacts_sent_ok");?>').slideDown("fast");
                        window.setTimeout(function() {
                            $("#submit-send").html( $("#submit-send").data("value") );
                            $("#submit-send").siblings(".submit-status").slideUp("fast", function() {
                                $("#submit-send").slideDown("fast");
                            });
                        }, 4000);
                    });
                },
                complete: function( jqXHR, textStatus ) {   //"success", "notmodified", "nocontent", "error", "timeout", "abort", or "parsererror"4
                }
            });
            
            return false;
        });
    });
    
    function toggle_form_error(jqXHR, textStatus, errorThrown) {
        var msg = '';

        if (errorThrown != "") {
            msg = errorThrown;
        }
        else {
            msg = '<?=$this->lang->line("fe_contacts_sent_error");?>';
        }

        $("#submit-send").siblings(".submit-status").removeClass("ap-post-submission-message").addClass("error").html(msg).slideDown("fast");
        $("#submit-send").html( $("#submit-send").data("value") );
    }
</script>