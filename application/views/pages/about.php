<div class="static-about-container">

    <?php foreach ($this->lang->line("about_sections") as $section) { ?>
        <p class="tmb">
            <?=$section['content'];?>
        </p>
    <?php } ?>

    <h1 class="title"><?=$this->lang->line("about_title2");?></h1>
    <h2 class="subtitle"><?=$this->lang->line("about_subtitle2");?></h2>
    <div class="social-media-container tmb">
        <a href="<?=$this->config->item("my_socialmedia_facebook");?>" target="_blank"><i class="fa fa-facebook"></i></a>
        <a href="<?=$this->config->item("my_socialmedia_linkedin");?>" target="_blank"><i class="fa fa-linkedin"></i></a>
        <a href="<?=$this->config->item("my_socialmedia_twitter");?>" target="_blank"><i class="fa fa-twitter"></i></a>
        <a href="<?=$this->config->item("my_socialmedia_pinterest");?>" target="_blank"><i class="fa fa-pinterest"></i></a>
        <a href="<?=$this->config->item("my_socialmedia_instagram");?>" target="_blank"><i class="fa fa-instagram"></i></a>
    </div>
    
    <h1 class="title"><?=$this->lang->line("contacts_title");?></h1>
    <h2 class="subtitle"><?=$this->lang->line("contacts_subtitle");?></h2>
    <p><?=$this->lang->line("about_mailing_address");?></p>
    <p class="tmb"><?=$this->config->item("my_support_address");?></p>
    <p><?=$this->lang->line("about_mailing_phone");?></p>
    <p class="tmb"><?=$this->config->item("my_support_phone");?></p>
    <p><?=$this->lang->line("about_email");?></p>
    <p class="tmb"><?=$this->config->item("my_emails_support");?></p>
        
</div>