<!--jquery-confirm.min-->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$lang["fe_word_post_by"] = $this->lang->line("fe_word_post_by");
$lang["fe_word_want_product"] = $this->lang->line("fe_word_want_product");
$lang["fe_imageupload_cat_title"] = $this->lang->line("fe_imageupload_cat_title");
$lang["fe_uploadpost_selectitem"] = $this->lang->line("fe_uploadpost_selectitem");
$lang["fe_tip_word_request_cat"] = $this->lang->line("fe_tip_word_request_cat");
$lang["fe_word_where_to_find"] = $this->lang->line("fe_word_where_to_find");
$lang["fe_tip_word_post_a_tip"] = $this->lang->line("fe_tip_word_post_a_tip");
$lang["fe_tip_word_shop"] = $this->lang->line("fe_tip_word_shop");
$lang["fe_tip_word_shops"] = $this->lang->line("fe_tip_word_shops");
$lang["fe_word_thank"] = $this->lang->line("fe_word_thank");
$lang["fe_word_thanked"] = $this->lang->line("fe_word_thanked");
$lang["fe_tip_word_buy"] = $this->lang->line("fe_tip_word_buy");
$lang["fe_word_iwant"] = $this->lang->line("fe_word_iwant");
$lang["fe_word_already_want"] = $this->lang->line("fe_word_already_want");
//TSM some brands will want to add everal items for post

if ($this->session->userdata("sex") != "b") {
	$wants_ok = true;
} else {
	$wants_ok = false;
}
$post_config["my_urlparams_show"] = $this->config->item("my_urlparams_show");
$post_config["my_urlparams_view"] = $this->config->item("my_urlparams_view");

function get_post_html($lang, $template = false, $wants_ok = true, $post = "", $post_config) {
	$template_class = "";
	$main_style = "";
	if ($template) {
		$template_class = " template";
	} else {
		$main_style = ' style="display:block"';
	}

	$post_id = "";
	$post_tags = "";
	$liked = "";
	$liked_count = 0;
	$post_image = "";
	$post_comment = "";
	$post_profile_url = "";
	$post_username = "";
	$post_avatar = "";
	$post_time = "";
	$post_banner = '';
	$post_selector = "";
	$post_cat_rows = "";
	$cat_options = "";
	$ad_cat_form = "";
	$image_alt = "";
	if (is_array($post)) {
//		die(print_r($post));
		$post_id = $post["id"];
		$post_selector = " the_post_holder";
		$liked_id = $post["id"];
		foreach ($post["tags"] as $tag) {
			$post_tags .= '<a href="' . base_url() . $post_config["my_urlparams_show"] . '/' . $tag . '">' . $tag . '</a>';
		}
		if ($post["liked"]) {
			$liked = " active";
		}
		$liked_count = $post["likes"];
		$post_image = $post["image"];
		$image_alt = ' alt="'.implode(",",$post["tags"]).'" ';
		$post_comment = $post["comment"];
		$post_profile_url = $post["profile_url"];
		$post_username = $post["username"];
		$post_avatar = $post["avatar"];
		$post_time = $post["date_word"]; // . " / " . $post["views_word"]
		$post_banner = $post["banner"];
		foreach ($post["post_categories"] as $post_cat_key => $post_cat) {
			$post_cat_rows .= get_cattiprow_html($lang, false, $wants_ok, $post_id, $post_cat, $post["tips"][$post_cat_key]);
		}
		if (isset($post["cat_options"])) {
			$cat_options = $post["cat_options"];
		}
	}
//	if ($wants_ok) {
		$ad_cat_form = '<div class="add_another_cat_wrapper">
				<div class="add_cat_btn">
					<a href="#" class="add-category">' . $lang["fe_word_want_product"] . '</a>
				</div>
				<div class="add_cat_form">
					<form class="form-add-cat" method="POST" action="t/ajax">
						<input type="hidden" name="action" value="addcat">
						<input type="hidden" name="post_id"  class="add_cat_id"  value="' . $post_id . '">
						<span class="">' . $lang["fe_imageupload_cat_title"] . '</span>
						<div class="form_row">
							<select name="category" class="add_cat_more">
								<option value="-1">' . $lang["fe_uploadpost_selectitem"] . '</option>
								' . $cat_options . '
							</select>
						</div>
						<div class="clear"></div>
						<div class="add_cat_btn">
							<a href="#" class="add-category-submit">' . $lang["fe_tip_word_request_cat"] . '</a>
						</div>
					</form>
				</div>
			</div>';
//	}
	$post_html = '<div class="wrap-fullwidth the_content' . $template_class . ' post' . $post_selector . '" typeof="product:Product" itemscope itemtype="http://schema.org/Product" ' . $main_style . '>
		<div class="post-tags-wrapper">
			<div class="post_closer fa fa-close"></div>
			<div class="tagcloud">' . $post_tags . '</div>
		</div>
		<div class="clear"></div>
		<div class="single-content">
			<article>
				<div class="post post_main_holder"' . $main_style . '>
					<div class="media-single-content">
						<img src="' . $post_image . '" class="attachment-thumbnail-single-image wp-post-image" itemprop="image"'.$image_alt.'/>
							<div class="post-likes">
							<a href="https://www.facebook.com/sharer/sharer.php?u=' . base_url() . $post_config["my_urlparams_view"] . "/" . $post_id . '" class="facebook_share" target="_blank"><i class="fa fa-facebook-square"></i></a>
							<a href="#" id="zilla_' . $post_id . '" class="zilla-likes' . $liked . '">
								<span class="zilla-likes-count">' . $liked_count . '</span>
							</a>
						</div>
					</div>

					<div id="author-box-single">
						<div class="author-box">
							<div class="at-img">
								<a href="' . $post_profile_url . '"><img alt="' . $post_username . '" src="' . $post_avatar . '" class="avatar avatar-50 photo" height="50" width="50" /></a>
							</div>
							<div class="at-links">
								<a href="' . $post_profile_url . '" title="' . $lang["fe_word_post_by"] . '" rel="author">' . $post_username . '</a><br />
								<div class="at-time">' . $post_time . '</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>

					<div class="entry">
						<div class="entry-content comment"  itemprop="description">' . $post_comment . '</div>
						<div class="clear"></div>                                                
					</div>
					<div class="clear"></div> 
				</div>
			</article>
		</div>
		<aside class="sidebar-buy">
			<div class="cat_tips_wrapper">' .
			$post_cat_rows
			. '</div>' .
			$ad_cat_form
			. '<div class="side_banners">'
			. $post_banner .
			'</div>
		</aside>
		<div class="clear"></div>
	</div>';
	return $post_html;
}

function get_cattiprow_html($lang, $template = false, $wants_ok, $post_id = "", $post_cat = "", $tips_array = "") {
	$template_class = "";
	if ($template) {
		$template_class = "_template";
	}

	$catrow_id = "";
	$cat_id = "";
	$catrow_small_catname = "";
	$catrow_catname = "";
	$catrow_tip_counts = 0;
	$cat_param = "";
	$catrow_word_shops = "";
	$catrow_word_postatip = $lang["fe_tip_word_post_a_tip"];
	$catrow_postatip_datas = "";
	$the_tips = "";
	$wants_word = $lang["fe_word_iwant"];
	$wants_class = "";
	if (is_array($post_cat)) {
		if ($post_cat["wants"] == "yes") {
			$wants_word = $lang["fe_word_already_want"];
			$wants_class = " wanted";
		}

		$catrow_id = ' id="cat_' . $post_cat["param"] . '"';
		$cat_param = $post_cat["param"];
		$cat_id = $post_cat["id"];
		$catrow_small_catname = $post_cat["name"];
		$catrow_catname = $post_cat["big_name"]; //$lang["fe_word_where_to_find"] . $post_cat["word_gender"]." ". $post_cat["big_name"];
		$catrow_tip_counts = $tips_array["count"];
		if ($tips_array["count"] == 1) {
			$catrow_word_shops = $lang["fe_tip_word_shop"];
		} else {
			$catrow_word_shops = $lang["fe_tip_word_shops"];
		}
		$catrow_postatip_datas = ' data-catid="' . $post_cat["id"] . '" data-catname="' . $post_cat["param"] . '"';
		if (isset($tips_array["tips"])) {
			foreach ($tips_array["tips"] as $the_tip) {
				$the_tips .=get_cattiprowholder_html($lang, false, $the_tip);
			}
		}
	}
	$cat_wants = "";
	if ($wants_ok) {
		$cat_wants = '<span data-catid="' . $cat_id . '" data-catparam="' . $cat_param . '" data-catname="' . $catrow_small_catname . '" data-postid="' . $post_id . '" class="cat_wants' . $wants_class . '">' . $wants_word . '</span>';
	}
	$result_html = '<div class="cat-tip-row' . $template_class . '"' . $catrow_id . '>
		<div class="tip_row_header">
			<span class="cat_name" itemprop="name">' . $catrow_catname . '</span>' . $cat_wants . '
		</div>
		<span class="post_a_tip_row ">
			<span class="tip_counts">' . $catrow_tip_counts . '</span> <span class="tip_shops">' . $catrow_word_shops . '</span> <span class="post-a-tip"' . $catrow_postatip_datas . '>' . $catrow_word_postatip . '</span>
			<span class="tip_add_nest"></span>
		</span>
		' . $the_tips . '
	</div>';
	return $result_html;
}

function get_cattiprowholder_html($lang, $template = false, $tip = "") {
	$template_class = "";
	if ($template) {
		$template_class = "_template";
	}
	$thetip_id = "";
	$thetip_avatar = "";
	$thetip_author = "";
	$thnx_done = "";
	$thnx_datas = "";
	$thnx_ok = "";
	if (is_array($tip)) {
		if ($tip["is_thanked"]) {
			$thnx_done = " done";
		} else {
			$thnx_ok = " ok";
		}
		$thnx_datas = ' data-tip="' . $tip["id"] . '" data-user="' . $tip["selector"] . '" data-domain="' . $tip["provider_domain"] . '"';
	}
	$thetip_thnx = '<span class="data_tip_buy_thnx' . $thnx_done . $thnx_ok . '"' . $thnx_datas . '><span class="ok">' . $lang["fe_word_thank"] . '</span><span class="done">' . $lang["fe_word_thanked"] . '</span></span>';
	$thetip_buylink = "";
	$thetip_image = "";
	$thetip_price = "";
	$thetip_domain = "";
	$thetip_brand = "";
	$no_image = "";
	if ($template) {
		$thetip_author = '<div class="tip_author"> 
				<div class="tip_author_avatar"></div>
				<div class="tip_author_name"></div>
				<div class="tip_author_delete"><span class="fa fa-times" data-tip_del="" data-from_cat=""></span></div>
			</div>';
	} else if (is_array($tip)) {
		$thetip_id = ' id="tip_id_' . $tip["id"] . '"';
		$can_delete = "";
		$go = "";
//		die(print_r($_SESSION));
		if ($tip["can_delete"]) {
			$go = " ok";
		}
		//die(print_r($tip));
		if ($tip["username"] != "") {
			$thetip_author = '<div class="tip_author"> 
				<div class="tip_author_avatar"><a href="' . $tip["profile_url"] . '"><img src="' . $tip["avatar"] . '" alt=""></a></div>
				<div class="tip_author_name"><span><a href="' . $tip["profile_url"] . '">' . $tip["username"] . '</a></span></div>
				<div class="tip_author_delete' . $go . '"><span class="fa fa-times" data-tip_del="' . $tip["id"] . '" data-from_cat="' . $tip["param"] . '"></span></div>
				</div>';
		} else if ($tip["can_delete"]) {
			$thetip_author = '<div class="tip_author"> 
				<div class="tip_author_avatar"></div>
				<div class="tip_author_name"></div>
				<div class="tip_author_delete' . $go . '"><span class="fa fa-times" data-tip_del="' . $tip["id"] . '" data-from_cat="' . $tip["param"] . '"></span></div>
				</div>';
		}
		if ($tip["is_own"]) {
			$thetip_thnx = '';
		}
		$thetip_buylink = '<a href="' . $tip["new_link"] . '" target="_blank" class="data_tip_buy">' . $lang["fe_tip_word_buy"] . '</a>';
		if ($tip["image"] != "") {
			$thetip_image = '<a href="' . $tip["new_link"] . '" target="_blank" class="product_image"><img src="' . $tip["image"] . '" itemprop="image" alt="></a>';
		} else {
			$no_image = " no_image";
		}
		$thetip_price = '<meta itemprop="priceCurrency" content="' . $tip["currency"] . '"><meta itemprop="price" content="' . $tip["price"] . '"><a href="' . $tip["new_link"] . '" target="_blank" class="cat_price_info"><span itemprop="category">' . $tip["cat_big_name"] . '</span>: <span>' . $tip["price"] . ' ' . $tip["currency_sign"] . '</span></a>';



//		if (data[key].brand) {
//		var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("brand_info");
//		product_href.appendTo(that.find('.tip_data_row.brand'));
//		that.find('.brand_info').html(data[key].brand);
//		}
//		if (data[key].brand == "" || data[key].brand!=data[key].provider_domain  ) {
//		var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("domain_info");
//		product_href.appendTo(that.find('.tip_data_row.domain'));
//		that.find('.domain_info').html(data[key].provider_domain);
		if ($tip["brand"] != "") {
			$thetip_brand = '<a href="' . $tip["new_link"] . '" target="_blank" class="brand_info">' . $tip["brand"] . '</a>';
		}
		if ($tip["brand"] == "" || ($tip["brand"] != $tip["provider_domain"])) {
			$thetip_domain = '<a href="' . $tip["new_link"] . '" target="_blank" class="domain_info">' . $tip["provider_domain"] . '</a>';
		}
	}
	$result_html = '<div class="tip_holder' . $template_class . '"' . $thetip_id . '>
		' . $thetip_author . '
		<div class="tip_main_wrapper">
			<div class="tip_main_holder" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<div class="tip_product_image' . $no_image . '">' . $thetip_image . '</div>
				<div class="tip_data_holder">
					<div class="tip_data_row price">' . $thetip_price . '</div>
					<div class="tip_data_row brand">' . $thetip_brand . '</div>
					<div class="tip_data_row domain">' . $thetip_domain . '</div>
					<div class="data_tip_buy_holder">' . $thetip_buylink . $thetip_thnx . '</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>';
	return $result_html;
}
?>
<div class="post_wrapper">
	<a href="#" name="uploaded" class="top_uploads"></a>
	<div class="tip_add_holder_template">
		<form id="form-add-tip" method="POST" action="t/ajax">
			<input type="hidden" name="action" value="addtip">
			<input type="hidden" class="tip_add_id" name="post_id" value="<?= isset($id) ? $id : ""; ?>">
			<input class="cat_id_input" type="hidden" name="cat_id" value="">
			<span class="closer fa fa-close"></span>
			<div class="tip_error main"></div>
			<div class="row-wrapper">
				<label><?= $this->lang->line('fe_tip_url') ?>:<span>*</span></label>
				<div class="input-wrapper">
					<input type="text" class="tip_url" name="tip_url" maxlength="500" value="" />
					<input type="hidden" class="old_tip_url" name="old_tip_url" value="" />
					<span class="fa fa-close"></span>
					<div class="tip_error url"></div>
				</div>
			</div>
			<div class="tip_images">
				<div class="tip_images_nav">
					<div class="arrows left_sequence fa fa-caret-square-o-left"> </div>
					<div class="total_sequences"> <span class="current">0</span> / <span class="total">0</span> </div>
					<div class="arrows right_sequence fa fa-caret-square-o-right"> </div>
				</div>
				<div class="tip_images_img"></div>
				<div class="tip_images_store_img"></div>
				<input type="hidden" name="tip_image_url" class="tip_image_url">
			</div>
			<div class="row-wrapper left">
				<label><?= $this->lang->line('fe_tip_price') ?>:<span>*</span></label>
				<div class="clear"></div>
				<div class="select-wrapper left">
					<select name="currency" class="tip_currency">
						<option value="-1"><?= $this->lang->line('fe_tip_currency') ?></option>
						<?php
						foreach ($this->lang->line("fe_currency") as $c_k => $c_v) {
							?>
							<option value="<?= $c_k ?>"><?= $c_v ?></option>
							<?php
						}
						?>
					</select>
				</div>

				<div class="input-wrapper left">
					<input type="number" class="tip_price" name="tip_price" min="<?= $this->config->item("my_min_tip_price") ?>" max="<?= $this->config->item("my_max_tip_price") ?>" value="" />
	<!--					<span class="tip_price_close fa fa-close"></span>-->
				</div>
				<div class="clear"></div>
				<div class="tip_error currency"></div>
				<div class="tip_error price"></div>
			</div>

			<div class="row-wrapper clear">
				<label><?= $this->lang->line('fe_tip_brand_name') ?>:</label>
				<div class="input-wrapper">
					<input type="text" class="tip_brand" name="tip_brand" maxlength="50" value="" />
					<span class="fa fa-close"></span>
				</div>
				<div class="tip_error brand"></div>
			</div>
			<div class="add_tip">
				<a href="#" id="add-tip"><?= $this->lang->line("fe_word_send") ?></a>
			</div>
			<div class="add_tip_loading"></div>
		</form>
	</div>

	<?php
	if ($page_type != "view") {
		echo get_post_html($lang, true, $wants_ok, "", $post_config);
	} else {
		?>
		<span class="post_appendor"><?= get_post_html($lang, false, $wants_ok, $post, $post_config); ?></span>
		<?php
	}

	echo get_cattiprow_html($lang, true, $wants_ok);
	echo get_cattiprowholder_html($lang, true);
	?>
	<script src="<?= base_url(); ?>assets/js/helpers.js"></script>
	<script>

		if (controler === "") {
			controler = "<?= isset($view) ? 'view' : ''; ?>";
		}
		var post_id = <?= isset($id) ? $id : 0; ?>;
		var processing_url = "";
		var sequences = [];
		var current_sequence = 0;
		var processed = 0;
		var stop_load;
		var max_tips = <?= $this->config->item("my_max_tips") ?>;
		var max_tips_per_post = <?= $this->config->item("my_max_cats_per_post"); ?>;
		var tags;
		var the_post;
		var base_url = '<?= base_url(); ?>';
		var langLine = {
			"fe_word_where_to_find": '<?= $this->lang->line("fe_word_where_to_find") ?>',
			"fe_tip_error_url": '<?= $this->lang->line("fe_tip_error_url") ?>',
			"fe_tip_error_no_url": '<?= $this->lang->line("fe_tip_error_no_url") ?>',
			"fe_tip_error_no_currency": '<?= $this->lang->line("fe_tip_error_no_currency") ?>',
			"fe_tip_error_no_price": '<?= $this->lang->line("fe_tip_error_no_price") ?>',
			"fe_tip_error_invalid_price": '<?= $this->lang->line("fe_tip_error_invalid_price") ?>',
			"fe_tip_error_min_price": '<?= sprintf($this->lang->line("fe_tip_error_no_currency"), $this->config->item("my_min_tip_price")) ?>',
			"fe_tip_error_max_price": '<?= sprintf($this->lang->line("fe_tip_error_max_price"), $this->config->item("my_max_tip_price")) ?>',
			"fe_tip_error_brand_url": '<?= $this->lang->line("fe_tip_error_brand_url") ?>',
			"fe_tip_error_brand_short": '<?= sprintf($this->lang->line("fe_tip_error_brand_short"), $this->config->item("my_min_brand_length")) ?>',
			"fe_tip_error_brand_long": '<?= sprintf($this->lang->line("fe_tip_error_brand_long"), $this->config->item("my_max_brand_length")) ?>',
			"fe_tip_word_buy": '<?= $this->lang->line("fe_tip_word_buy") ?>',
			"fe_tip_word_shop": '<?= $this->lang->line("fe_tip_word_shop") ?>',
			"fe_tip_word_shops": '<?= $this->lang->line("fe_tip_word_shops") ?>',
			"fe_tip_word_post_a_tip": '<?= $this->lang->line("fe_tip_word_post_a_tip") ?>',
			"fe_word_iwant": '<?= $this->lang->line("fe_word_iwant") ?>',
			"fe_word_already_want": '<?= $this->lang->line("fe_word_already_want") ?>'
		};
		var post_tips = "";
		var post_categories = "";
		var total_categories = "";
		var just_loaded_post = <?= isset($post["id"]) ? "true" : "false"; ?>;
		var no_more_cats = <?= !isset($post["cat_options"]) ? "true" : "false"; ?>;

		$(function () {
			if ('scrollRestoration' in history) {
				//alert("here");
				history.scrollRestoration = 'manual';
			} else {
				$(window).load(function () {
//					alert("ready");
//					setTimeout(function(){$(window).scrollTop(0)},1000);

				});
			}

			if (!just_loaded_post) {
				just_loaded_post = false;
				if (post_id != "") {
					get_post(post_id, "view");
				}
			} else {
				var add_data = {
					append_to: ".post_mason_appendor"
				}
				masonryInit("view", add_data);
				if (!no_more_cats) {
					add_post_categories();
				}
				$(".the_post_holder .post-a-tip").on("click", function (e) {
//					console.log("here 5");
					resetTipImages();
					isLogged("post_a_tip_click", e);
					return false;
				});
			}
			$(".post_main_holder .at-img img").imagesLoaded(function () {
				$(".post_main_holder").show();
				$(".sidebar-buy").show();
			});
			$('.post_appendor').on('click', ".data_tip_buy_thnx.ok", function (e) {
				var that = $(this);
				var id = that.attr('data-tip');
				var user = that.attr('data-user');
				var domain = that.attr('data-domain');
				$.ajax({
					url: baseUrl + 't/ajax',
					dataType: 'json',
					type: 'post',
					data: {tip_id: id, action: "addthnx", selector: user, domain: domain},
				}).done(function (result) {
					if (result["login"]) {
						window.location.replace(baseUrl + "login");
					} else {
						if (result["done"]) {
							$("#stadtInput").attr("class", "textboxRight");
							that.removeClass('ok');
							that.addClass('done');
						}
					}
				});
			});
		});
		function get_post(post_id, key) {
			$(".add_cat_id").val(post_id);
			$(".tip_add_id").val(post_id);
			$.ajax({
				type: "POST",
				url: baseUrl + view_url + "/" + post_id,
				dataType: "json",
				success: function (data) {
					if (data.error) {
						history.go(-1);
					} else {
						var add_data = {
							exclude: data.exclude,
							explore: data.explore,
							category: data.category,
							append_to: ".post_mason_appendor"
						}

						post_tips = data.post.tips;
						data.post.tips = undefined;
						post_categories = data.post.post_categories;
						data.post.post_categories = undefined;
						total_categories = data.categories;
						tags = data.post.tags;
						data.post.tags = undefined;
						the_post = data.post;
						data = undefined;
						show_post();
						masonryInit(key, add_data);
						$(".home-fullwidth.mason.view .search_header").html("<?= $this->lang->line("fe_tip_word_related") ?>");
					}
				}
			});
		}
		function show_tags() {
			for (var key in tags) {
				$(".the_post_holder .tagcloud").append($("<a>", {"href": base_url + show_url + "/" + tags[key], "html": tags[key]}));
			}
		}
		function reset_post() {
			$(".post_appendor").html("");
			$(".post_mason_appendor").html("");
			if (controler == "profile") {
				$(".clear_header").show();
				$(".wrap-fullwidth.profile").show();
			} else {
				if (featured) {
//					$("#featured-slider").show();
					$(".clear_header").hide();
					$('#featured-slider').css({top: 0, opacity: 1, height: "auto"});
					$(".blog-home-container").show();
				} else {
					$(".clear_header").show();
				}
			}
		}

		function show_post() {
			//wrap-fullwidth template
			var top_selector = "the_post_holder";
			var template_class = $(".wrap-fullwidth.template").attr("class").replace(" template", "") + " " + top_selector;
			$(".post_appendor").html($("<div>", {class: template_class}));
			top_selector = "." + top_selector;
			$(top_selector).html($(".wrap-fullwidth.template").html())
			if (masonries.main !== undefined) {
				$(top_selector + " .post_closer").show();
				$(top_selector + " .post_closer").on("click", function () {
					history.go(-1);
				});
			}
			add_post_categories();
			for (var i = 0; i < post_tips.length; i++) {
				add_tip_row(post_tips[i]);
			}

			$(top_selector + " .post-a-tip").on("click", function (e) {
//				console.log("here 6");
				resetTipImages();
				isLogged("post_a_tip_click", e);
				return false;
			});
			add_post_cats();
			show_tags(top_selector);
			$(top_selector + " .post_main_holder").html($(".post_main_holder_template").html());
			if (the_post["liked"]) {
				$(top_selector + " .post_main_holder .zilla-likes").addClass("active");
			}
			$(top_selector + " .post_main_holder .zilla-likes").attr({"id": "zilla_" + the_post["id"]});
			$(top_selector + " .post_main_holder .zilla-likes-count").html(the_post["likes"]);
			$(top_selector + " .post_main_holder .attachment-thumbnail-single-image").attr({"src": the_post["image"]});
			var post_by = $(top_selector + " .post_main_holder .at-links a").attr("title");
			$(top_selector + " .post_main_holder .at-img a").attr("href", the_post["profile_url"]);
			$(top_selector + " .post_main_holder .at-img img").attr({"src": the_post["avatar"]});
			$(top_selector + " .post_main_holder .at-links a").attr({"href": the_post["profile_url"], "title": post_by + " " + the_post["username"]}).html(the_post["username"]);
			$(top_selector + " .post_main_holder .at-time").html(the_post["date_word"] + " / " + the_post["views_word"]);
			$(top_selector + " .post_main_holder .entry-content").html(the_post["comment"]);
			$(top_selector + " .post_main_holder .facebook_share").attr("href", $(top_selector + " .post_main_holder .facebook_share").attr("href") + the_post["id"]);

			//$(document).scrollTop();
			$(top_selector).show();




		}

		function add_post_cats() {
			select_target = $(".the_post_holder .add_cat_more");
			if (post_categories.length < max_tips_per_post) {
				var cats_ids = [];
				for (var i = 0; i < post_categories.length; i++) {
					cats_ids.push(post_categories[i].id);
				}
				for (var key in total_categories) {
					var select_options = '<optgroup label="' + total_categories[key].group_name + '">' + "\n";

					//select_target.append('<optgroup label="' + total_categories[key].group_name + '">');
					for (var data_key in total_categories[key].data) {
						if ($.inArray(total_categories[key].data[data_key].cat_id, cats_ids) == -1) {
//							select_target.append($('<option>', {"value": total_categories[key].data[data_key].cat_id, "html": total_categories[key].data[data_key].cat_name}));
							select_options += '	<option value="' + total_categories[key].data[data_key].cat_id + '">' + total_categories[key].data[data_key].cat_name + '</option>' + "\n";
						}
					}
					select_options += ('</optgroup>');
					select_target.append(select_options);
				}
			}
		}

		function toggle_wants(input_data) {
			input_data.action = "wants";
			$.ajax({
				url: base_url + "t/ajax",
				dataType: 'json',
				type: 'post',
				data: input_data,
			}).done(function (result) {
				if (result["answer"] == "login") {
					//redirecto to login
					window.location.replace(baseUrl + "login");
				} else {
					if (result["answer"] == "remove") {
						$("#cat_" + input_data.cat_param + " .cat_wants").removeClass("wanted").text(langLine.fe_word_iwant);
					} else if (result["answer"] == "add") {
						$("#cat_" + input_data.cat_param + " .cat_wants").addClass("wanted").text(langLine.fe_word_already_want);
					}
				}
			});
			return false;
		}


		function add_tip_row(row_data, new_tip_row) {
			$(".cat-tip-row_template").clone().appendTo(".the_post_holder .cat_tips_wrapper");
			var row_template = $(".cat_tips_wrapper .cat-tip-row_template");
			row_template.addClass("cat-tip-row").removeClass("cat-tip-row_template").attr("id", "cat_" + row_data.param);
			var this_selctor = "#cat_" + row_data.param;


			row_template.find(".cat_name").html(row_data.big_name); //langLine.fe_word_where_to_find + row_data.word_gender + " " + row_data.big_name
			row_template.find(".tip_counts").html(row_data.count);
			//row_template.find(".post-a-tip").html(langLine.fe_tip_word_post_a_tip);
			if (row_data.count == 1) {
				row_template.find(".tip_shops").html(langLine.fe_tip_word_shop);
			} else {
				row_template.find(".tip_shops").html(langLine.fe_tip_word_shops);
			}
			row_template.find(".post-a-tip").attr("data-catid", row_data.id).attr("data-catname", row_data.param);
			var wants_class = "";
			var want_word = langLine.fe_word_iwant;

			if (row_data.wants == "yes") {
				wants_class = "wanted";
				want_word = langLine.fe_word_already_want;
			}
			row_template.find(".cat_wants").attr("data-catid", row_data.id).attr("data-catparam", row_data.param).attr("data-catname", row_data.name).attr("data-postid", post_id).addClass(wants_class).text(want_word);
			if (new_tip_row != null) {
				row_template.find(".post-a-tip").addClass("new_tip_row-" + new_tip_row);
			}
			$(".add_cat_more option[value='" + row_data["id"] + "']").remove();
			if (row_data.tips) {
				show_tip(this_selctor, row_data.tips);
			}
			if (row_data.count >= max_tips) {
				//$(this_selctor + " .post_a_tip_row").html("");
				$(this_selctor + " .post-a-tip").remove();
			}
			//$(".cat-tip-row");
		}
		function add_post_categories() {
			if (post_categories.length < max_tips_per_post) {
				$(".the_post_holder .add_another_cat_wrapper").show();
				$(".the_post_holder .add-category").on("click", function (e) {
					isLogged("addCatToggle");
					return false;
				});
				$(".the_post_holder  .add-category-submit").on("click", function (e) {
					if ($(".the_post_holder  .add_cat_more").val() != "" && $(".the_post_holder  .add_cat_more").val() != -1) {
						$.ajax({
							type: "POST",
							url: baseUrl + $('.the_post_holder .form-add-cat').attr("action"),
							data: $('.the_post_holder .form-add-cat').serialize(),
							dataType: "json",
							success: function (data) {
								if (data["login"]) {
									window.location.replace(baseUrl + "login");
									return false;
								} else {
									if (data["category_to_remove"] != null) {
										$(".the_post_holder .add_cat_more option[value='" + data["category_to_remove"] + "']").remove();
									}
									if (data["result_action"] == "remove_add" || data["result_action"] == "added_last") {
										$(".the_post_holder .add_another_cat_wrapper").remove();
									} else {
										$(".the_post_holder .add_another_cat_wrapper").show();
										isLogged("addCatToggle");
									}

									add_tip_row(data["tip_header"], new_tip_row);
									$(".new_tip_row-" + new_tip_row).on("click", function (e) {
										isLogged("post_a_tip_click", e);
										return false;
									});
									new_tip_row++;
								}
							},
							complete: function (data) {
							}
						});
					}

					return false;
				});
			} else {
				$(".the_post_holder .add_another_cat_wrapper").remove();
			}

		}



		$(function () {

//			var btn2 = document.querySelector('.js-tingle-modal-2');
//			btn2.addEventListener('click', function () {
//				modalTinyBtn.open();
//			});

			$("body").on('click', ".cat_wants", function (event) {
				var that = $(event.currentTarget);
				var wants_data = {};
				wants_data.cat_id = that.data("catid");
				wants_data.cat_name = that.data("catname");
				wants_data.cat_param = that.data("catparam");
				wants_data.post_id = that.data("postid");
				toggle_wants(wants_data);
			});


			$("body").on("click", ".tip_author_delete .fa-times", function (e) {
				var modalTinyBtn = new tingle.modal({
					footer: true
				});
				var that = $(this);
				var id = that.attr('data-tip_del');
				var cat = that.attr('data-from_cat');
				var tip_image = "";
				//tip_product_image
				if ($("#tip_id_" + id + " .tip_product_image img").length) {
					tip_image = '<img src="' + $("#tip_id_" + id + " .tip_product_image img").attr("src") + '" alt="">';
				}
				modalTinyBtn.setContent("<?= $this->lang->line("fe_delete_confirm") ?><br>" + $("#tip_id_" + id + " .tip_data_row.price").text() + "<br>" + tip_image);
				modalTinyBtn.setFooterContent("");
				modalTinyBtn.addFooterBtn('<?= $this->lang->line("fe_delete_confirm_yes") ?>', 'tingle-btn tingle-btn--danger tingle-btn--pull-right', function () {
					$.ajax({
						url: baseUrl + 't/ajax',
						dataType: 'json',
						type: 'post',
						data: {tip_id: id, action: "deltip"},
					}).done(function (result) {
						if (result["login"]) {
							window.location.replace(baseUrl + "login");
						} else {
							if (result["done"]) {
								var tip_counts = parseInt($("#cat_" + cat + " .tip_counts").html()) - 1;
								$("#tip_id_" + id).remove();
								$("#cat_" + cat + " .tip_counts").html(tip_counts);
							}
						}
					});
					modalTinyBtn.close();
				});
				modalTinyBtn.addFooterBtn('<?= $this->lang->line("fe_delete_confirm_no") ?>', 'tingle-btn tingle-btn--primary tingle-btn--pull-right', function () {
					modalTinyBtn.close();
				});
				modalTinyBtn.open();
			});
		});
		function show_tip(selector, data) {
			for (var key in data) {
				$(".tip_holder_template").clone().appendTo(selector);
				$(selector + " .tip_holder_template").attr("id", "tip_id_" + data[key].id);
				var that = $("#tip_id_" + data[key].id);
				that.removeClass("tip_holder_template").addClass("tip_holder");
				if (data[key].user) {
					var img = $('<img />', {
						src: data[key].avatar
					});
					//img.appendTo(that.find('.tip_author_avatar'));
					var author_href = $("<a />").attr("href", data[key].profile_url).html(data[key].user);
					var author_href_avatar = $("<a />").attr("href", data[key].profile_url).html(img);
					author_href_avatar.appendTo(that.find('.tip_author_avatar'));
					var author_name = $('<span />').html(author_href);
					author_name.appendTo(that.find('.tip_author_name'));
					if (!data[key].is_thanked && !data[key].is_own) {
						that.find(".data_tip_buy_thnx").attr({"data-tip": data[key].id, "data-user": data[key].selector, "data-domain": data[key].provider_domain}).addClass("ok");
					} else {
						if (data[key].is_thanked) {
							that.find(".data_tip_buy_thnx").addClass("done");
						} else {
							that.find(".data_tip_buy_thnx").remove();
						}
					}
					if (data[key].can_delete) {
						that.find('.tip_author_delete .fa-times').attr({"data-tip_del": data[key].id, "data-from_cat": data[key].param});
						that.find('.tip_author_delete').show();
					} else {
						that.find('.tip_author_delete').remove();
					}
				} else {
					that.find(".data_tip_buy_thnx").remove();
					that.find('.tip_author').remove();
				}

				if (data[key].image) {
					var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"});
					product_href.addClass("product_image");
					product_href.appendTo(that.find('.tip_product_image'));
					var img = $('<img />', {
						src: data[key].image
					});
					img.appendTo(that.find('.product_image'));
				} else {
					that.find('.tip_product_image').addClass("no_image");
				}
				var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("cat_price_info");
				product_href.appendTo(that.find('.tip_data_row.price'));
				var data_span = $('<span />').html(data[key].cat_big_name + ": ");
				data_span.appendTo(that.find('.cat_price_info'));
				data_span = $('<span />').html(data[key].price + " " + data[key].currency_sign);
				data_span.appendTo(that.find('.cat_price_info'));
				if (data[key].brand) {
					var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("brand_info");
					product_href.appendTo(that.find('.tip_data_row.brand'));
					that.find('.brand_info').html(data[key].brand);
				}
				if (data[key].brand == "" || data[key].brand != data[key].provider_domain) {
					var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("domain_info");
					product_href.appendTo(that.find('.tip_data_row.domain'));
					that.find('.domain_info').html(data[key].provider_domain);
				}
				var product_href = $("<a />").attr({"href": data[key].new_link, "target": "_blank"}).addClass("data_tip_buy");
				product_href.prependTo(that.find('.data_tip_buy_holder'));
				that.find('.data_tip_buy').html(langLine.fe_tip_word_buy);
			}
			$(".a_post_tip_image img,.tip_product_image img").one("load", function () {
				$(this).parent().parent().addClass("tip-img-loaded");
			}).each(function () {
				if (this.complete)
					$(this).load();
			});
		}

		function post_a_tip_click(event) {
			var that_cat_id = $(event.currentTarget).attr("data-catid");
			var that_cat_name = $(event.currentTarget).attr("data-catname");

			var that = $(event.currentTarget).parent();
			var remoteTipTimeout;

			if (that.find(".tip_add_nest").html() == "") {
				$(".tip_add_nest").html("");
				$(".tip_add_holder_template").clone().appendTo(that.find(".tip_add_nest"));
				that.find(".tip_add_holder_template").addClass("tip_add_holder").removeClass("tip_add_holder_template").show();
				that.find("#add-tip").attr("data-catname", that_cat_name);
				that.find(".cat_id_input").val(that_cat_id);
				that.find(".closer").on("click", function (e) {
//					console.log("here 7");
					resetTipImages();
					$(".tip_add_holder").remove();
				});
				that.find(".tip_add_holder .fa.fa-close").on("click", function (e) {
					$(e.currentTarget).parent().find("input").val("");
					if ($(e.currentTarget).parent().find("input").hasClass("tip_url")) {
//						console.log("here 8");
						resetTipImages();
					}
				});
				that.find(".tip_url").on("change", function (e) {// blur
//					console.log(e);
					var url_input = that.find(".tip_url");
					var that_url = decodeURI(url_input.val().trim());
					var old_url = decodeURI(that.find(".old_tip_url").val().trim());
					var that_selector = that.find("#form-add-tip");
					var timeOut;
					//						if (url_input.val().trim() != that_url) {
					//							url_input.val(that_url);
					//						}
					if (that_url != old_url) {
						that.find(".old_tip_url").val(that_url);
						if (processing_url !== that_url && that_url !== "") {
							processing_url = that_url;
							clearTimeout(remoteTipTimeout);
							if (e.type == "keyup") {
								remoteTipTimeout = setTimeout(function () {
									getRemoteTip(that_selector)
								}, 1000);
							} else {
								getRemoteTip(that_selector);
							}
							//resetTipImages();
						}
					}

				});
				that.find("#add-tip").on("click", function (e) {
					submitTip(that.find("#form-add-tip"), $(this).data("catname"));
					return false;
				});
			}

		}


		function resetTipImages() {
			$('.cat-tip-row .tip_images .right_sequence').off("click");
			$('.cat-tip-row .tip_images .left_sequence').off("click");
			$(".tip_images_img").html("");
			$(".tip_images_store_img").html("");
			$(".tip_images").hide();
			$(".tip_images .current").html("0");
			$(".tip_images .total").html("0");
//			$(".tip_brand").val("");
//			$(".tip_price").val("");
//			$(".tip_currency").val("-1");
			sequences = [];
			processed = 0;
			clearTimeout(stop_load);
			$(".tip_images_img img").off("load");
			processing_url = "";
			$(".add_tip").show();
		}

		function getRemoteTip(selector) {
			selector.find('.tip_error.url').html("");
			var errors = false;
			var url = selector.find(".tip_url").val().trim();
			if (url === "") {
				errors = true;
			} else {
				var check_url = isUrlValid(url, true);
//				console.log("here 1");
				resetTipImages();
				if (!check_url) {
					selector.find('.tip_error.url').html(langLine.fe_tip_error_url);
					errors = true;
				} else if (check_url.length > 0) {
					url = check_url + url;
					selector.find(".tip_url").val(url);
				}
			}
			if (!errors) {
				var request_data = {
					"action": "getremotetip",
					"url": url
				};
				$('.cat-tip-row .tip_images').show();
				$('.cat-tip-row .tip_images').addClass("loading");
				$(".add_tip").hide();
				//			stop_load = setTimeout(function () {
				//				resetTipImages();
				//			}, 5000);

				$('.cat-tip-row .tip_images .left_sequence').on("click", function (e) {
					rotateSequencesPrevious();
				});
				$('.cat-tip-row .tip_images .right_sequence').on("click", function (e) {
					rotateSequencesNext();
				});
				$.ajax({
					type: "POST",
					url: baseUrl + selector.attr("action"),
					data: request_data,
					dataType: "json",
					async: true,
					success: function (data)
					{
						var shown = false;
						if (data.url) {
							$(".cat-tip-row .tip_url").val(data.url);
						}
						if (data.error) {
							for (var key in data.error) {
								$(".cat-tip-row .tip_error." + key).html(data.error[key]);
							}
						}
						if (data.price) {

							$(".cat-tip-row .tip_price").val(data.price);
						}
						if (data.currency) {
							$('.cat-tip-row .tip_currency').val(data.currency);
						}
						if (data.site_name) {
							$('.cat-tip-row .tip_brand').val(data.site_name);
							//$(".tip_currency").html(data.price);
						}
						if (data.site_name) {
							$('.cat-tip-row .tip_brand').val(data.site_name);
							//$(".tip_currency").html(data.price);
						}
						if (data.images !== undefined && data.images.length > 0) {
							clearTimeout(stop_load);
							for (var i = 0; i < data.images.length; i++) {
								var img = $('<img id="tip_img_' + i + '">');
								img.attr('src', data.images[i]);
								img.attr('sequence', i);
								img.appendTo('.cat-tip-row .tip_images_store_img');

								$("#tip_img_" + i).on("load", function (e) {
									var that = e.currentTarget;
									processed++;
									$("#" + that.id).off("load");
									var ratio = that.width / that.height;
									if (that.height > 200 && ratio < 2.5) {
										$("#" + that.id).clone().appendTo(".cat-tip-row .tip_images_img");
										sequences.push($("#" + that.id).attr("sequence"));
										$(".add_tip").show();
										if (!shown) {
											$('.cat-tip-row .tip_images').removeClass("loading");
											$(".add_tip").show();
											shown = true;
											current_sequence = sequences.length - 1;
											rotateSequences();
										}
									}
									$(".cat-tip-row .tip_images_store_img #" + that.id).remove();
									$(".total_sequences .total").html(sequences.length);
									clearTimeout(stop_load);
									if (!shown) {
										stop_load = setTimeout(function () {
//											console.log("here 2");
											resetTipImages();
										}, 5000);
									}
									if (processed == data.images.length) {
										clearTimeout(stop_load);
										$('.cat-tip-row .tip_images').removeClass("loading");
										$(".add_tip").show();
									}
								});

							}
						} else {
//						console.log("here 3");
							resetTipImages();
							$(".add_tip").show();
						}

					}
				});
			}
		}

		function rotateSequences() {
			$(".total_sequences .current").html(current_sequence + 1);
			$(".tip_images_img img").hide();
			$(".tip_images .tip_images_img img").eq(current_sequence).show();
			$(".cat-tip-row .tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
		}

		function rotateSequencesNext() {
			if (sequences.length > 0) {
				if (current_sequence == sequences.length - 1) {
					current_sequence = 0;
				} else {
					current_sequence++;
				}
				$(".total_sequences .current").html(current_sequence + 1);
				$(".tip_images_img img").hide();
				$(".tip_images_img img").eq(current_sequence).show();
				$(".cat-tip-row .tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
			}
		}

		function rotateSequencesPrevious() {
			if (sequences.length > 0) {
				if (current_sequence == 0) {
					current_sequence = sequences.length - 1;
				} else {
					current_sequence--;
				}
				$(".total_sequences .current").html(current_sequence + 1);
				$(".tip_images_img img").hide();
				$(".tip_images_img img").eq(current_sequence).show();
				$(".cat-tip-row .tip_image_url").val($(".tip_images .tip_images_img img").eq(current_sequence).attr("src"));
			}
		}

		function submitTip(selector, parent_cat) {
			//vaildate
			$(".add_tip").hide();
			$(".add_tip_loading").show();
			selector.find('.tip_error').html("");
			var that = selector.find('#form-add-tip');
			var url = selector.find(".tip_url").val().trim();
			var price = selector.find(".tip_price").val().trim();
			var currency = selector.find(".tip_currency").val().trim();
			var brand = selector.find(".tip_brand").val().trim();
			var min_tip_price =<?= $this->config->item("my_min_tip_price") ?>;
			var max_tip_price =<?= $this->config->item("my_max_tip_price") ?>;
			var max_brand_length =<?= $this->config->item("my_max_brand_length") ?>;
			var min_brand_length =<?= $this->config->item("my_min_brand_length") ?>;
			var errors = false;
			if (url === "") {
				//resetTipImages();
				selector.find('.tip_error.url').html(langLine.fe_tip_error_no_url);
				errors = true;
			} else {
				var check_url = isUrlValid(url, true);
				//resetTipImages();
				if (!check_url) {
					selector.find('.tip_error.url').html(langLine.fe_tip_error_url);
					errors = true;
				} else if (check_url.length > 0) {
					url = check_url + url;
					selector.find(".tip_url").val(url);
				}
			}
			if (currency === "" || currency == -1) {
				selector.find('.tip_error.currency').html(langLine.fe_tip_error_no_currency);
				errors = true;
			}
			if (price === "") {
				selector.find('.tip_error.price').html(langLine.fe_tip_error_no_price);
				errors = true;
			} else if (isNaN(price)) {
				selector.find('.tip_error.price').html(langLine.fe_tip_error_invalid_price);
				errors = true;
			} else if (price < min_tip_price) {
				selector.find('.tip_error.price').html(langLine.fe_tip_error_min_price);
				errors = true;
			} else if (price > max_tip_price) {
				selector.find('.tip_error.price').html(langLine.fe_tip_error_max_price);
				errors = true;
			}
			if (brand != "") {
				if (brand.length < min_brand_length) {
					selector.find('.tip_error.brand').html(langLine.fe_tip_error_brand_short);
					errors = true;
				} else if (brand.length > max_brand_length) {
					selector.find('.tip_error.brand').html(langLine.fe_tip_error_brand_long);
					errors = true;
				}
			}
			if (!errors) {
				$("#add-tip").off("click");
				$("#add-tip").on("click", function (e) {
					return false;
				});

				$.ajax({
					type: "POST",
					url: baseUrl + selector.attr("action"),
					data: selector.serialize(),
					dataType: "json",
					success: function (data) {
						$("#add-tip").off("click");
						if (data["login"]) {
							window.location.replace(baseUrl + "login");
							return false;
						} else {
							if (data.error) {
								for (var key in data.error) {
									$(".cat-tip-row .tip_error." + key).html(data.error[key]);
								}
								$("#add-tip").on("click", function (e) {
									submitTip(selector, selector.data("catname"));
									return false;
								});
								$(".add_tip").show();
								$(".add_tip_loading").hide();
							} else {
//								console.log("here 4");
								resetTipImages();
								$(".tip_add_holder").remove();
								if ($("#cat_" + parent_cat + " .tip_holder").length >= max_tips - 1) {
									$("#cat_" + parent_cat + " .post_a_tip_row").html("");
									$("#cat_" + parent_cat + " .post_a_tip_row").addClass("removed");
								}
								show_tip("#cat_" + parent_cat, data.new_tip)
								var tip_counts = parseInt($("#cat_" + parent_cat + " .tip_counts").html());
								$("#cat_" + parent_cat + " .tip_counts").html((tip_counts + 1));
								if (tip_counts == 0) {
									$("#cat_" + parent_cat + " .tip_shops").html(langLine.fe_tip_word_shop);
								} else {
									$("#cat_" + parent_cat + " .tip_shops").html(langLine.fe_tip_word_shops);
								}
								$(".add_tip").show();
								$(".add_tip_loading").hide();
							}
						}

					}
				});
			} else {
				$(".add_tip").show();
				$(".add_tip_loading").hide();
			}
		}

		function addCatToggle() {
			var that = $(".the_post_holder  .add_cat_form");
			if (that.hasClass("visible")) {
				that.animate({"opacity": 0}, function () {
					that.removeClass("visible");
				});
			} else {
				that.addClass("visible");
				that.animate({"opacity": 1});
			}
		}
		var new_tip_row = 0;
	</script>
</div>