<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once "Simpleimage.php";

class Sharpimage {

//	public function __construct($params) {
//	}

	public function sharpen_image($image_loaction) {
		$handle = fopen($image_loaction, "rb");
		$image = stream_get_contents($handle);
		$image = imagecreatefromstring($image);
		$image = new SimpleImage($image);
		$offset = 0;
		$sharpen = 11;
		$spnMatrix = array(
			array(-1, -2, -1),
			array(-2, $sharpen + 12, -2),
			array(-1, -2, -1)
		);
		$divisor = $sharpen;
		imageconvolution($image->image, $spnMatrix, $divisor, $offset);
		$image->save($image_loaction);
	}
	
	public function sharpen_image_resize($image_loaction, $image_destination, $width) {
		$handle = fopen($image_loaction, "rb");
		$image = stream_get_contents($handle);
		$image = imagecreatefromstring($image);
		$image = new SimpleImage($image);
		$image->resizeToWidth($width);
		$image->save($image_destination);
	}

}
