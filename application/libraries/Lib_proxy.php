<?php

class Lib_proxy {

    private $CI;
    public $id;
    public $ip;
    public $port;

    function __construct() {
        $this->CI = &get_instance();

//        $this->getRandom();
    }

    function setProxy($id, $ip, $port) {
        $this->id = $id;
        $this->ip = $ip;
        $this->port = $port;
    }

    function getRandom($beforeUT=false) {

        $res = $this->CI->proxydb
                ->select("id, ip, port")
                ->from("parser_proxy")
                ->where(["active" => "yes", "del" => "no", "used_last" => null])
                ->order_by('ut', 'asc')
                ->limit(1)
                ->get();
        if (!$res->num_rows()) {
            $where = ["active" => "yes", "del" => "no", "used_last" => null];
            if ($beforeUT !== false) {
                $where['used_last < '] = $beforeUT;
            }
            $res = $this->CI->proxydb
                    ->select("id, ip, port")
                    ->from("parser_proxy")
                    ->where($where)
                    ->order_by('used_last', 'asc')
                    ->limit(1)
                    ->get();
            if (!$res->num_rows()) {
                die("NO ACTIVE PROXIES!!!\n");
            }
        }
        $res = $res->row();
        $this->setProxy($res->id, $res->ip, $res->port);
    }

    function setLastUsed() {
        $this->CI->proxydb->set('used_last', 'NOW()', false);
        $this->CI->proxydb->where('id', $this->id);
        $this->CI->proxydb->update("parser_proxy",  []);
    }

    function checkAvailability() {
        if (!$this->id) {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.google.com");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, $this->ip . ":" . $this->port);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $content = curl_exec($ch);
        curl_close($ch);

        if (stripos($content, "google") !== false) {
            $this->CI->proxydb->set('checked_last', 'NOW()', false);
            $this->CI->proxydb->where('id', $this->id);
            $this->CI->proxydb->update("parser_proxy",  ['active' => 'yes']);
            
            return true;
        } else {
            $this->markInactive();
            
            return false;
        }
    }

    function markInactive() {
        $this->CI->proxydb->set('checked_last', 'NOW()', false);
        $this->CI->proxydb->set('error_count', 'error_count + 1', false);
        $this->CI->proxydb->where('id', $this->id);
        $this->CI->proxydb->update("parser_proxy",  ['active' => 'no']);
    }

    /**
     * 
     * @param array $params [url*, headers, host, referer, cookie, postdata] *-required
     * @return string scraped data
     */
    function getCURLData($params = []) {

        if (!isset($params['url']) || !filter_var($params['url'], FILTER_VALIDATE_URL)) {
            echo "ERROR: Missing or invalid url supplied (".$params['url'].")\n";
            return false;
        }

        $headers = [];
        if (!isset($params['headers'])) {
            $headers = array(
                'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1.17) Gecko/20110121 Firefox/3.5.17 ( .NET CLR 3.5.30729)',
                'Accept	text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Language: en-us,en;q=0.5',
                'Accept-Encoding: gzip,deflate',
                'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
                'Keep-Alive: 300',
                'Connection: keep-alive',
                'X-lori-time-1: ' . microtime(),
                'Cache-Control: max-age=0'
            );
        }
        else {
            foreach ($params['headers'] as $hdr) {
                $headers[] = $hdr;
            }
        }
            
        if (isset($params['host'])) {
            $headers[] = 'Host: ' . $params['host'];
        }
        if ($params['referer']) {
            $headers[] = 'Referer: ' . $params['referer'];
        }
        if ($params['cookie']) {
            $headers[] = 'Cookie: ' . $params['cookie'];
        }


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $params['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        //curl_setopt($ch, CURLOPT_PROXYPORT, "80");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if ($params['postdata']) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params['postdata']);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);

        $iTryCount = 0;
        do {
            if ($iTryCount) {
                $this->setLastUsed();
                $this->markInactive();
                echo " - <b>failed</b> (" . $params['url'] . ")<br>";
                if ($iTryCount == 10) {
                    return false;
                }
            }
            $this->getRandom();
            curl_setopt($ch, CURLOPT_PROXY, $this->ip . ":" . $this->port);
            echo "Proxy: " . $this->id . ". " . $this->ip . ":" . $this->port ;
            $data = curl_exec($ch);
            $iTryCount++;
        } while (!strlen($data));
        echo " - <b>success</b> (" . $params['url'] . ")\n";

        curl_close($ch);

        return $data;
    }

}
