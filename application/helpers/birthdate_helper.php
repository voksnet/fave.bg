<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('buildDayDropdown')) {
	function buildDayDropdown($name = '', $value = '', $title = "") {
		if (trim($title) != "") {
			$day_list[''] = $title;
		} else {
			$day_list[''] = 'Day';
		}
		$days = range(1, 31);
		foreach ($days as $day) {
			$day_list[$day] = $day;
		}
		return form_dropdown($name, $day_list, $value);
	}
}

if (!function_exists('buildYearDropdown')) {
	function buildYearDropdown($name = '', $value = '',$minimal_age = 13, $title="") {
		if (trim($title) != "") {
			$year_list[''] = $title;
		} else {
			$year_list[''] = 'Year';
		}
		$years = range(date("Y") - $minimal_age,1922);
		foreach ($years as $year) {
			$year_list[$year] = $year;
		}
		return form_dropdown($name, $year_list, $value);
	}
}

if (!function_exists('buildMonthDropdown')) {
	function buildMonthDropdown($name = '', $value = '', $months_list = "") {
		if (is_array($months_list) && count($months_list) == 13) {
			$month = $months_list;
		} else {
			//fallback to english
			$month = array(
				'' => 'Month',
				'01' => 'January',
				'02' => 'February',
				'03' => 'March',
				'04' => 'April',
				'05' => 'May',
				'06' => 'June',
				'07' => 'July',
				'08' => 'August',
				'09' => 'September',
				'10' => 'October',
				'11' => 'November',
				'12' => 'December');
			}
		return form_dropdown($name, $month, $value);
	}
}