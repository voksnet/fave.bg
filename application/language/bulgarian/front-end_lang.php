<?php

$lang["fe_seo_site_name"] = "Fave.bg"; //short - domain only because will be used further
$lang["fe_seo_default_image"] = "assets/images/big_logo.png";
$lang["fe_seo_site_description"] = "Онлайн фешън платформа за споделяне, търсене и намиране на любими дрехи или модни аксесоари.";
$lang["fe_seo_short_description"] = "Онлайн фешън платформа за споделяне.";
$lang["fe_seo_site_locale"] = "bg_BG";

$lang["fe_seo_site_user_description"] = "Потребителски профил на %s във %s ";
$lang["fe_seo_upload"] = "Добави";
$lang["fe_seo_search"] = "Търсене на %s";


$lang["fe_email"] = "Имейл";
$lang["fe_password"] = "Парола";
$lang["fe_password_repeat"] = "Повтори паролата";
$lang["fe_remember_me"] = "Запомни ме";
$lang["fe_message"] = "Съобщение";
$lang["fe_login"] = "Вписване";
$lang["fe_login_submit"] = "Вписване";
$lang["fe_logout"] = "Изход";
$lang["fe_login_failed"] = "Невалидни данни за вход";
$lang["fe_login_failed_gmail"] = "Проблем при логване с Gmail (server status error 200)";
$lang["fe_login_failed_gmail_notverified"] = "Проблем при логване с Gmail (email verify error)";
$lang["fe_register"] = "Регистрация";
$lang["fe_register_push_1"] = "Регистрирай се! <br>Безплатно е!";
$lang["fe_register_push_2"] = "Регистрирай се! <br>Безплатно е!";
$lang["fe_register_edit"] = "Редакция на профил!";
$lang["fe_register_edit_of"] = "Редакция на профил на %s";
$lang["fe_register_facebook"] = "Регистрация с  Facebook";
$lang["fe_register_gmail"] = "Регистрация с Gmail";
$lang["fe_register_email"] = "или се регистрирай с имейл";
$lang["fe_register_already"] = "вече имаш регистрация?";
$lang["fe_register_notyet"] = "Все още нямаш регистрация?";
$lang["fe_register_description"] = "Няколко думи за Вас";
$lang["fe_register_avatar"] = "Аватар";
$lang["fe_register_has_error"] = "Има грешка/и. Моля корегирате за да продължите";
$lang["fe_forgot_pass"] = "Забравена парола?";
$lang["fe_reset_pass"] = "Възстановяване на парола";
$lang["fe_reset_send_link"] = "Изпрати линк";
$lang["fe_reset_pass_enter_mail"] = "Въведете имейлът, който сте използвали при регистрация, и ще Ви изпратим линк за смяна на паролата.";
$lang["fe_reset_mail_not_found"] = "Не е намерен потребител с този имейл.";
$lang["fe_reset_check_mail"] = "Линкът за промяна на паролата е изпратен. Моля проверете си имейла сега.";
$lang["fe_reset_enter_new_pass"] = "Въведете нова парола";
$lang["fe_reset_btn_change_pass"] = "Промени паролата";
$lang["fe_reset_pass_enter_new"] = "Въведете новата парола в полетата по-долу и натиснете <b>".$lang["fe_reset_btn_change_pass"]."</b>.";
$lang["fe_reset_invalid_link"] = "Линкът за възстановяване на паролата не е валиден/активен.";
$lang["fe_reset_match_pass"] = "Въведените пароли трябва да са поне 5 символа и да съвпадат.";
$lang["reset_pass_success"] = "Паролата Ви е успешно сменена. Може да влзете във Вашият акаунт.";

$lang["fe_sex_m"] = "Мъж";
$lang["fe_sex_f"] = "Жена";
$lang["fe_sex_u"] = "Унисекс";
$lang["fe_brand"] = "Представлявате марка 	( &copy;  &reg; )";
$lang["fe_register_name"] = "Име / псевдоним";
$lang["fe_register_birthday"] = "Дата на раждане";
$lang["fe_register_brandname"] = "Име на марка";
$lang["fe_register_website"] = "Уеб сайт";
$lang["fe_register_phone"] = "Телефон";
$lang["fe_register_error_sex"] = "Пол";
$lang["fe_register_error_name"] = "Име / псевдоним";
$lang["fe_register_error_email"] = "Имейл";
$lang["fe_register_error_email–exists"] = "Потребител с този имейл вече съществува.";
$lang["fe_register_error_password"] = "Парола";
$lang["fe_register_error_birthdate"] = "Невалидна дата";
$lang["fe_register_error_password-repeat"] = "Повтори паролата";
$lang["fe_register_error_brand"] = "Име на марка";
$lang["fe_register_error_website"] = "Уеб сайт";
$lang["fe_register_error_phone"] = "Телефон";
$lang["fe_register_error_description"] = "Описанието трябва да е не по-малко от 50 символа и не повече от 500";
$lang["fe_register_submit"] = "Регистрация";
$lang["fe_profile_edit_submit"] = "Промени";
$lang["fe_brand_disclaimer"] = "Ако представлявате марка, то вашата регистрация НЯМА ДА БЪДЕ АКТИВИРАНА ВЕДНАГА. Това ще се случи след като някой от екипа на Fave.bg потвърди регистрацията Ви.";
$lang["fe_session_error_user"] = "Изтекла сесия";

$lang["fe_login_facebook"] = "Влез с Facebook";
$lang["fe_login_gmail"] = "Влез с Gmail";

$lang["fe_word_or"] = "или";
$lang["fe_word_add-image"] = "Избери снимка";
$lang["fe_word_cancel"] = "Откажи";
$lang["fe_word_error"] = "Грешка";
$lang["fe_word_delete"] = "Изтрий";
$lang["fe_word_start-upload"] = "Качи";
$lang["fe_word_like_this"] = "Харесай";
$lang["fe_word_post_by"] = "От: ";
$lang["fe_word_view"] = "%d преглеждане";
$lang["fe_word_views"] = "%d преглеждания";
$lang["fe_word_search_placeholder"] = "Търсене";
$lang["fe_word_similar_posts"] = "Подобни";
$lang["fe_word_show_more"] = "Покажи още";
$lang["fe_word_send"] = "Изпрати";
$lang["fe_word_want_product"] = "Желаеш друг артикул?";
$lang["fe_word_years"] = "години";
$lang["fe_word_search"] = "Търси";
$lang["fe_word_more_about_you"] = "Кажете малко за себе си, какво харесвате ...";
$lang["fe_word_wants"] = "Желае";
$lang["fe_word_wants_this"] = "желае:";
$lang["fe_word_iwant"] = "желая";
$lang["fe_word_already_want"] = "желаеш";
$lang["fe_word_catalog"] = "Каталог";
$lang["fe_word_profile_sponsored"] = "Спонсорирани";
$lang["fe_word_likes"] = "Харесва";
$lang["fe_word_you_follow"] = "Следвай";
$lang["fe_word_you_follows"] = "Следваш";
$lang["fe_word_follows"] = "следва";
$lang["fe_word_follows_header"] = "Следва";
$lang["fe_word_followers"] = "последователи";
$lang["fe_word_followers_header"] = "Последователи";
$lang["fe_word_rating"] = "рейтинг";
$lang["fe_word_tips"] = "подшушвания";
$lang["fe_word_edit_profile"] = "Редактирай профила";
$lang["fe_word_thank"] = "Благодари";
$lang["fe_word_thanked"] = "Благодарено";
$lang["fe_word_read_full_article"] = "Прочети цялата статия";
$lang["fe_word_view_more_interesting"] = "Вижте още интересни статии";
$lang["fe_word_latest_blogs"] = "Последно от блога";
$lang["fe_word_useful"] = "Полезно";
$lang["fe_word_most_tags"] = "Популярни тагове";

$lang["fe_word_error_no_result"] = "Няма открити резултати";

$lang["fe_word_upload"] = "Добави";
$lang["fe_word_profile"] = "Профил";
$lang["fe_word_login"] = "Вписване";
$lang["fe_word_signup"] = "Регистрация";
$lang["fe_word_logout"] = "Изход";
$lang["fe_word_chouse"] = "Избери";

$lang["fe_requestupload_title"] = "Качване на снимка";
$lang["fe_requestupload_menorowomen"] = "За мъж или жена ...";
$lang["fe_paste_image_url"] = "... поставете линк";
$lang["fe_imgupload_invalid_url"] = "Невалидна снимка или URL";
$lang["fe_imgupload_invalid_url_site"] = "Невалидно URL или не са намерени подходящи снимки";
$lang["fe_imageupload_small_image"] = "Размера на снимката трябва да е най-малко 330x330";
$lang["fe_imageupload_big_file"] = "Файлът е твърде голям %s Kb. Трябва да е по-малък от %s Kb";
$lang["fe_imageupload_big_file_js"] = "Файлът е твърде голям {0} Kb. Трябва да е по-малък от {1} Kb";
$lang["fe_imageupload_max_files"] = "Надхвърлен е максималния брой файлове";
$lang["fe_imageupload_file_types"] = "Непозволен тип файл";
$lang["fe_imageupload_small_file"] = "Файлът е твърде малък";

$lang["fe_imageupload_tag_link"] = "Някои тагове съдържат линкове";
$lang["fe_imageupload_tag_censor"] = "Някои тагове съдържат неподходящи думи";
$lang["fe_imageupload_tag_none"] = "Поне един таг трябва да се въведе";
$lang["fe_imageupload_comment_link"] = "В коментара е открит линк";
$lang["fe_imageupload_comment_censor"] = "Коментара съдържа неподходящи думи";
$lang["fe_imageupload_cat_title"] = "Къде мога да намеря ...";
$lang["fe_imageupload_cat_none"] = "Това поле е задължително";
$lang["fe_imageupload_gender_none"] = "Това поле е задължително";

$lang["fe_upload_short_tag"] = "В таговете е открит къс таг. Таговете трябва да са минимум %d символа и не повече от %d символа";
$lang["fe_upload_long_tag"] = "В таговете е открит дълъг таг. Таговете трябва да са минимум %d символа и не повече от %d символа";
$lang["fe_upload_too_many_tags"] = "Максималния брой тагове са надвишени. Не повече от %d";
$lang["fe_upload_error_url"] = "Невалиден или грешен линк";
$lang["fe_upload_error_currency"] = "Ако е избрана цена то трябва да се избере и валута";
$lang["fe_upload_error_no_price"] = "Ако е избрана валута то трябва да се избере и цена";
$lang["fe_upload_error_price"] = "Невалидна цена";
$lang["fe_upload_error_must_price"] = "След като е въведен линк за продукта трябва да се въведе и цена";
$lang["fe_upload_error_price_url"] = "Цената е позволена само за продукти с линк. Въведете линк";

$lang["fe_uploadpost_selectitem"] = "Изберете артикул";
$lang["fe_add_comment"] = "Добави коментар";
$lang["fe_add_url"] = "Добави линк (само за спонсорирани)";
$lang["fe_add_post_url"] = "линк за продукта (не задължително)";
$lang["fe_add_post_url_sponsored"] = "линк за продукта * ";
$lang["fe_add_post_url_brand"] = "линк за продукта * ";
$lang["fe_upload_error_url_must"] = "Вие сте представител на марка, линка е задължителен";
$lang["fe_upload_error_url_must_sponsored"] = "Вие сте представител на марка, трябва или да изберете че това е спонсориран пост или да въведете цена";

$lang["fe_add_is_sponsored"] = "спонсориран пост";
$lang["fe_add_tags"] = "Тагове (въведете таг и натиснете 'Enter' или ',' (запетая) )";

$lang["fe_timebefore_before"] = "преди %d %s";
$lang["fe_timebefore_minute"] = "минута";
$lang["fe_timebefore_minutes"] = "минути";
$lang["fe_timebefore_hour"] = "час";
$lang["fe_timebefore_hours"] = "часа";
$lang["fe_timebefore_day"] = "ден";
$lang["fe_timebefore_days"] = "дена";

$lang["fe_word_sponsored"] = "спонсорирано";

$lang["fe_word_where_to_find"] = "";
$lang["fe_tip_url"] = "Линк към страницата на продукта";
$lang["fe_tip_price"] = "Цена";
$lang["fe_tip_currency"] = "валута";
$lang["fe_tip_brand_name"] = "Марка (незадължително)";

$lang["fe_tip_error_url"] = "Невалиден или грешен линк";
$lang["fe_tip_error_no_url"] = "Не е въведен линк";
$lang["fe_tip_error_no_currency"] = "Не е избрана вид валута";
$lang["fe_tip_error_no_price"] = "Не е въведена цена";
$lang["fe_tip_error_invalid_price"] = "Въведена е невалидна цена";
$lang["fe_tip_error_min_price"] = "Възможната минимална цене е %d";
$lang["fe_tip_error_max_price"] = "Възможната максимална цена е %d";
$lang["fe_tip_error_brand_url"] = "Невалидно име за марка";
$lang["fe_tip_error_brand_short"] = "Името на марката е твърде късо";
$lang["fe_tip_error_brand_long"] = "Името на марката е твърде дълго";
$lang["fe_tip_error_error"] = "Възникна грешка! Моля презередете страницата";
$lang["fe_tip_error_duplicate"] = "Този линк вече е зададен. Пробвайте с друг";
$lang["fe_tip_error_forbiden_url"] = "Този домайен/сайт е забранен за добавяне";
$lang['fe_tip_word_buy'] = 'Купи';
$lang['fe_tip_word_shop'] = 'подшушване';
$lang['fe_tip_word_shops'] = 'подшушвания';
$lang['fe_tip_word_post_a_tip'] = 'подшушни';
$lang['fe_tip_word_request_cat'] = 'Изпрати';
$lang['fe_tip_word_related'] = 'Подобни';

$lang['fe_avup_image_select'] = 'Избери снимка';
$lang['fe_avup_crop'] = 'Изрежи';
$lang['fe_avup_drag_and_drop'] = 'Постави снимка тука';
$lang['fe_avup_err_image_size'] = "Избраната снимка е ";
$lang['fe_avup_err_max_size'] = "Максималният възможен размер е ";
$lang['fe_avup_err_file'] = "Файл ";
$lang['fe_avup_err_is'] = " е ";
$lang['fe_avup_err_larger_than'] = " което е повече от максимално позволения размер от ";
$lang['fe_avup_err_error_upload'] = "Възникна грешка при качването на снимката ";
$lang['fe_avup_err_valid_type'] = "Проверете дали файла е валиден тип за снимка и пробвайте отново.";
$lang['fe_avup_crop_info'] = "Оразмерете и позиционирайте снимката в квадрата по-горе. Натиснете бутона <b>".$lang['fe_avup_crop']."</b>, след което натиснете <b>".$lang['fe_profile_edit_submit']."</b> в <a href=\"#profile-save\">края на профилната страница.";

$lang['fe_delete_confirm'] = "Сигурнили сте че искате да изтриете: ";
$lang['fe_delete_confirm_yes'] = "Да";
$lang['fe_delete_confirm_no'] = "Не";

$lang["fe_currency"]["BGN"] = "лв.";
$lang["fe_currency"]["GBP"] = "£";
$lang["fe_currency"]["EUR"] = "€";
$lang["fe_currency"]["USD"] = "$";

$lang["fe_contacts_error_name"] = "Моля, въведете Вашето име.";
$lang["fe_contacts_error_email"] = "Въведете валиден имейл адрес.";
$lang["fe_contacts_error_message"] = "Въведете съобщение. Ако желаете, оставете и телефон за обратна връзка.";
$lang["fe_contacts_sent_ok"] = "Вашето съобщение е изпратено успешно!";
$lang["fe_contacts_sent_error"] = "Възникна грешка, моля опитайте отново.";
$lang["fe_please_wait"] = "Моля изчакайте";

$lang["fe_blog_share_with_friends"] = "Споделете с приятели";
$lang["fe_blog_i_am_reading"] = "В момента чета";
$lang["fe_usage_terms"] = "Условия за ползване";
$lang["fe_contacts"] = "Връзка с нас";

$lang["email_signoff"] = "Поздрави,";
$lang["email_faveteam"] = "Екипът на Fave.bg";

$lang["page_title_newest_pots"] = "Най-нови постове";

$lang["fe_site_menu"] = array(
	"home" => array(
		"title" => "Начало",
		"description" => "Начало",
		"keywords" => "начало"
	),
	"recent" => array(
		"title" => "Най-нови",
		"description" => "Най-нови постове",
		"keywords" => "Най-нови постове"
	),
	"my_feed" => array(
		"title" => "Моите интереси",
		"description" => "Моите интереси",
		"keywords" => "Моите интереси"
	),
	"fave" => array(
		"title" => "Любими",
		"description" => "Любими",
		"keywords" => "Любими"
	),
	"blog" => array(
		"title" => "Блог",
		"description" => "Блог",
		"keywords" => "Блог"
	),
	"faq" => array(
		"Помощ",
		"title" => "Помощ",
		"description" => "Помощ",
		"keywords" => "Помощ"
	),
	"fashion" => array(
		"Мода",
		"title" => "Мода",
		"description" => "Мода",
		"keywords" => "Мода"
	),
	"about_us" => array(
		"title" => "За нас",
		"description" => "За нас",
		"keywords" => "За нас"
	),
	"how_it_works" => array(
		"title" => "Как работи",
		"description" => "Как работи",
		"keywords" => "Как работи"
	),
	"contact_us" => array(
		"title" => $lang["fe_contacts"],
		"description" => $lang["fe_contacts"],
		"keywords" => $lang["fe_contacts"]
	),
	"terms_of_use" => array(
		"title" => $lang["fe_usage_terms"],
		"description" => $lang["fe_usage_terms"],
		"keywords" => $lang["fe_usage_terms"]
	)
);








	$lang["your_cart_empty"] = "Вашата кошницата беше изпразнена";
	$lang["main_desc"] = "Online магазин за мобилни аксесоари и Apple продукти. Всичко за MacBook, MacBook Pro, MacBook Air, iMac, Mac Mini, Samsung, HTC, Nokia, Dell, Sony, LG";
	$lang["main_title"] = "Дигитален център";
	$lang["homepage_title"] = "Дигитален център - аксесоари за вашите мобилни устройства";
	$lang["main_keywords"] = "калъфи за iphone 4, aksesoari za mobilni ustroistva, Apple MacBook, аксесоари за iphone 4, iphone аксесоари, слушалки за ipod, samsung galaxy s2 калъф, аксесоари за ipad, Samsung Galaxy Note N7000 калъф, зарядно за iphone 4, слушалки за iphone, калъф за ipad 2, apple аксесоари, aksesoari za iPhone 4S, калъф за galaxy note, htc аксесоари, htc sensation калъф, samsung кожен калъф, луксозни аксесоари за iPhone 4S";
	$lang["home"] = "Начало";
	$lang["innovations"] = "Иновации";
	$lang["reviews"] = "Ревюта";
	$lang["music"] = "Музика";
	$lang["about_us"] = "За Нас";
	$lang["my_cart"] = "Моята Кошница";
	$lang["products"] = "Продукти";
	$lang["price"] = "Цена";
	$lang["are_you_sure"] = "Сигурни ли сте че искате да изтриете всички продукти в кошницата?";
	$lang["empty_cart"] = "Изпразни кошницата";
	$lang["send_to_friend"] = "Изпрати на приятел";
	$lang["your_name"] = "Вашето име";
	$lang["your_friends_name"] = "Името на вашия приятел";
	$lang["your_friends_mail"] = "Поща на получателя";
	$lang["send"] = "Изпрати";
	$lang["register"] = "Регистрация";
	$lang["login"] = "Вход";
	$lang["purchases"] = "Поръчки";
	$lang["logout"] = "Изход";
	$lang["profile"] = "Профил";
	$lang["cart"] = "Кошница";
	$lang["promotions"] = "Промоции";
	$lang["sale"] = "Разпродажба";
	$lang["forum"] = "Форум";
	$lang["hello"] = "Здравей";
	$lang["contacts"] = "За Контакт";
	$lang["questions"] = "Въпроси";
	$lang["partners"] = "Партньори";
	$lang["dealers"] = "Дилъри";
	$lang["where_we_are"] = "Къде се намираме";
	$lang["iphone_service"] = "Apple сервизи";
	$lang["footer_text"] = "Общи условия";
	$lang["pages"] = "Страници";
	$lang["product_was_added_your_cart"] = 'Продуктът беше добавен във вашата <a href="#href#" style="background:none; color:black;">кошница</a>';
	$lang["accessories"] = "аксесоари";
	$lang["latest_news"] = "Последни Новини";
	$lang["our_recommendations"] = "Препоръчваме Ви следните продукти";
	$lang["most_commented"] = "Най-коментирани";
	$lang["most_bought"] = "Най-продавани";	
	$lang["price_currency"] = "лв.";
	$lang["view_details"] = "Виж цялата оферта";
	$lang["with_vat"] = "с ДДС";
	$lang["without_vat"] = "без ДДС";
	$lang["this_product_removed_catalog"] = "Този продукт е премахнат от нашия каталог";
	$lang["empty_name"] = "Непопълнено име";
	$lang["empty_comment"] = "Непопълнен коментар";
	$lang["comment_added_admin_approval"] = "Вашия коментар беше добавен,той ще бъде прегледан от администратор";
	$lang["color_chosen_from_cart"] = "Цветът се избира от кошницата (избери цвят)";
	$lang["we_recommend"] = "Препоръчваме Ви";
	$lang["please_fill_fields"] = "Моля попълнете всички полета";
	$lang["comments"] = "Коментари";
	$lang["comments_only_cyrilic"] = " - Коментари се допускат само, ако са на български език";
	$lang["no_comments_for_product"] = "Няма коментари за текущия продукт";
	$lang["all"] = "всички";
	$lang["date"] = "Дата";
	$lang["name"] = "Име";
	$lang["quantity"] = "Количество";
	$lang["quantity_short"] = "Кол.";
	$lang["choose_color"] = "Избери цвят";
	$lang["colors_saved"] = "Цветовете бяха запаметени";
	$lang["quant."] = "бр.";
	$lang["currency_for_qty"] = "лв. за бройка";
	$lang["total_price"] = "Обща сума";
	$lang["button_refresh_qty"] = "<i>Бутона <b>Опресни</b> обновява бройките в количката</i>";
	$lang["characteristics_of"] = "Характеристики на";
	$lang["product"] = "Продукт";
	$lang["save_chararteristics"] = "Запиши характеристиките";
	$lang["Налични цветове"] = "Други цветове";
	$lang["in_color_color"] = "в @color@ цвят";
	$lang["quantity_in_color"] = "бройка @number@: @color@ цвят";
	$lang["menu_pay_methods"] = "Методи на плащане";
	$lang["menu_howto_shop"] = "Как да пазаруваме";
	$lang["menu_price_shipment"] = "Цена и срок на доставката";
	$lang["menu_discounts"] = "Промоции и отстъпки";
	$lang["menu_warranty"] = "Гаранция и рекламации";
	$lang["menu_gifts"] = "Ваучери за подаръци";
	$lang["info_for_dealers"] = "Информация за дилъри";
	$lang["address"] = "Адрес";
	$lang["city"] = "Град";
	$lang["telephone"] = "Телефон";
	$lang["vat_registration"] = "Регистрация по ДДС";
	$lang["eik"] = "ЕИК";
	$lang["firm_name"] = "Име на фирмата";
	$lang["registration_address"] = "Адрес по регистрация";
	$lang["mol"] = "МОЛ";
	$lang["if_registered_user"] = 'Ако сте регистриран потребител вие може да се логнете от <b><a href="@href@">тук</a></b>';
	$lang["steps"] = "Стъпки";
	$lang["fill_personal_info"] = "Попълване на лични данни";
	$lang["recheck_personal_info"] = "Преглед на личните Ви данни";
	$lang["choose_pay_type"] = "Избор на начин на плащане";
	$lang["order_confirm"] = "Потвърждение";
	$lang["personal_info"] = "Лични данни!";
	$lang["please_fill_these_fields"] = "Моля попълнете следните полета";
	$lang["please_fill_personal_info"] = "Моля въведете вашите данни";
	$lang["your_personal_info"] = "Вашите данни";
	$lang["shipping_address"] = "Адрес за доставка";
	$lang["city_village"] = "Град/Село";
	$lang["the_telephone_need_delivery"] = "телефонът е необходим на куриера, за да се свърже с Вас при доставката";
	$lang["zip_code"] = "Пощенски код";
	$lang["want_invoice"] = "Желая фактура";
	$lang["comment"] = "Коментар";
	$lang["all_prices_vat"] = "всички цени са с ДДС";
	$lang["required_fields"] = "задължителни полета";
	$lang["free_shipping"] = 'Вашата доставка ще е безплатна и ще бъде изпратена с куриерска фирма "Спийди"';
	$lang["free_shipping_left"] = "Вашата доставка <br />ще е безплатна";
	$lang["free"] = "безплатни";
	$lang["xx_left_to_free"] = "Остават Ви <b>@price@</b> до безплатна доставка";
	$lang["delivery_service"] = "Куриерски услуги";
	$lang["please_add_items_cart"] = "Моля добавете продукти в кошницата!";
	$lang["code_accepted"] = "Кодът беше приет";
	$lang["code_wrong"] = "Грешен код!";
	$lang["code_removed"] = "Кодът беше премахнат";
	$lang["price_after_discount"] = "Цена След Отстъпката";
	$lang["discount"] = "Отстъпка";
	$lang["type_of_delivery"] = "Доставка чрез";
	$lang["cash_on_delivery"] = "Наложен платеж";
	$lang["promo_code"] = "Код за отстъпка";
	$lang["id1"] = "Чрез наложен платеж с куриер";
	$lang["id8"] = "Чрез наложен платеж с куриер (Експресна)";
	$lang["id2"] = "ePay";
	$lang["id3"] = "Банков превод";
	$lang["id4"] = "В нашия офис";
	$lang["id1_text"] = "Доставката отнема до 2 работни дни";
	$lang["id8_text"] = "Доставката отнема до 2 работни дни";
	$lang["id2_2_text"] = 'Доставката чрез Български Пощи е безплатна<br />Доставката чрез Български пощи отнема до 7 работни дни<br /><b style="color:red;">След като завършите вашата поръчка натиснете бутона "Плати с ePay"</b>';
	$lang["id2_5_text"] = 'С този избор на доставка Вие запазвате продуктите и ще ги получите в нашия офис!<br /><b style="color:red;">След като завършите вашата поръчка натиснете бутона "Плати с ePay"</b>';
	$lang["id2_text"] = 'Доставката отнема до 2 работни дни<br /><b style="color:red;">След като завършите вашата поръчка натиснете бутона "Плати с ePay"</b>';
	$lang["id3_2_text"] = 'Уникредит Булбанк <br />
     IBAN: <b>BG80UNCR70001522422263</b><br />
     BIC: <b>UNCRBGSF</b><br />На оставения от вас email адрес ще получите нашата банкова сметка<br />Доставката чрез Български Пощи е безплатна<br />Доставката чрез Български пощи отнема до 7 работни дни";
	$lang["id3_5_text"] = "Уникредит Булбанк <br />
     IBAN: <b>BG80UNCR70001522422263</b><br />
     BIC: <b>UNCRBGSF</b><br />На оставения от вас email адрес ще получите нашата банкова сметка<br />С този избор на доставка Вие запазвате продуктите и ще ги получите в нашия офис!";
	$lang["id3_text"] = "Уникредит Булбанк <br />
     IBAN: <b>BG80UNCR70001522422263</b><br />
     BIC: <b>UNCRBGSF</b><br />На оставения от вас email адрес ще получите нашата банкова сметка<br />Доставката отнема до 2 работни дни';
	$lang["id4_text"] = "Чрез тази опция вие запазвате продуктите, които сте поръчали и които ще платите и получите в нашия офис!";
	$lang["id2_text_dealer"] = '<b style="color:red;">След като завършите вашата поръчка натиснете бутона "Плати с ePay"</b>';
	$lang["id3_text_dealer"] = '<span id="get_from_office">С този избор на доставка Вие запазвате продуктите и ще ги получите в нашия офис!<br /></span><br /> 
	  Уникредит Булбанк <br />
     IBAN: <b>BG80UNCR70001522422263</b><br />
     BIC: <b>UNCRBGSF</b>';
	$lang["id3_text_dealer_office"] = "";
	$lang["id4_dealer"] = "В брой (в нашия офис)";
	$lang["id4_text_dealer"] = "Продуктите ще бъдат платени в брой и взети от нашия офис";
	$lang["courier"] = "Куриер";
	$lang["post"] = "Български пощи";
	$lang["payment_type"] = "Начин на заплащане";
	$lang["invoice_info"] = "Данни за фактура";
	$lang["to_complete_your_order"] = "За да потвърдите покупката си натиснете бутона!<br />При натискане вашата покупка ще бъде обработена и вашата количка ще бъде изпразнена!";
	$lang["click_on_epay_button"] = 'След като завършите вашата поръчка натиснете бутона "Плати с ePay"';
	$lang["username"] = "Потребителско име";
	$lang["password"] = "Парола";
	$lang["repeat_password"] = "Повтори Паролата";
	$lang["your_names"] = "Вашите имена";
	$lang["user_info"] = "Потребителски данни";
	$lang["delivery_info"] = "Данни за доставка на поръчките";
	$lang["telephone_contact"] = "Телефон за контакти";
	$lang["user_type"] = "Тип Потребител";
	$lang["private_user"] = "Частно лице";
	$lang["company"] = "Фирма";
	$lang["company_data"] = "Фирмени данни";
	$lang["input_captcha"] = "Въведете кода от картинката";
	$lang["required_fields_pass"] = "задължителни полета. Потребителското име и паролата трябва да бъдат поне 3 симвoла";
	$lang["invalid_username"] = "Невалидно потребителско име (поне 3 симвoла)";
	$lang["username_in_use"] = "Потребителското име вече съществува";
	$lang["please_fill_telephone"] = "Моля попълнете телефон за контакти";
	$lang["invalid_password"] = "Паролата не е попълнена коректно или е по-малка от 3 символа";
	$lang["invalid_email"] = "Невалиден email адрес";
	$lang["wrong_code"] = "Неточен код";
	$lang["successfully_registered"] = "Вие се регистрирахте успешно. Може да влезете във вашия акаунт.";
	$lang["your_profile_actualised"] = "Вашият профил бе актуализиран";
	$lang["edit_profile"] = "Редактиране на профил";
	$lang["register_user"] = "Регистрация на потребител";
	$lang["users_entry"] = "Вход за потребители";
	$lang["your_registration_not_confirmed"] = "Вашата регистрация не е потвърдена";
	$lang["invalid_username_password"] = "Невалидно потребителско име или парола";
	$lang["forgotten_password"] = "Забравена парола?";
	$lang["register_now"] = "Регистирай се";
	$lang["you_can_register_here"] = 'Ако не сте регистриран потребител може да се регистрирате от <a href="@url@"> тук </a>";
	$lang["new_password_sent"] = "На вашия имейл беше изпратена новата ви парола<br />Ако не получите писмо в следващите 5 минути, моля погледнете в Спам папката на Вашата поща';
	$lang["please_fill_email_register"] = 'Моля въведете имейл адреса, който сте задали при вашата регистрация.<br />На него ще получите вашата нова парола';
	$lang["unknown_email"] = "Този E-mail не е разпознат от системата";
	$lang["warranty_text"] = '<b style="color:red;">ВАЖНО:  Моля пазете оригиналната опаковка на всеки продукт закупен от нас. Изискванията на производителите, за да уважат гаранцията, са да се предоставят оригиналната опаковка с всички прилежащи към нея аксесоари и документация. Ако не се предоставят оригиналната опаковка с всички прилежащи към нея аксесоари и документация, гаранцията ви няма да бъде уважена.</b><br /><br /><b>Гаранционни Условия:</b>  Гаранционна стока се обслужва като такава при представяне на оригинална гаранционна карта (този документ) от купувача, на чието име същата е издадена. При всички други случаи "Ди Си 2008" ООД (Дигитален Център) има право да откаже приемането на стоката като гаранционна. Ако през гаранционния период, гаранционният продукт прояви дефект, Дигитален Център (ДЦ) поема гаранция да ремонтира или замени продукта в 14 дневен срок. Първоначалният гаранционен срок остава в сила, независимо дали модулите са били отремонтирани или подменени.<br />Гаранцията за продуктите включва само фабрични дефекти, но не и дефекти възникнали след употреба. Като дефекти възникнали след употреба спадат счупване, напукване, надраскване, оцветяване, поява на неравности, подуване и други подобни.<br />
		<b>Внимание:</b> гаранцията на защитните покрития включва само фабрични дефекти, но не и дефекти възникнали след употреба. Като дефекти възникнали след употреба спадат счупване, напукване, надраскване и други подобни. Гаранцията не покрива неправилното поставяне, както и попадането на прашинки и други замърсители между дисплея и покритието.<br /><b>Внимание:</b>  Гаранцията за слушалките включва проявени фабрични дефекти като прекъсване на звука или бръждене, но не включва следи от нормална употреба. Като такива спадат: замърсяване на слушалките, оголване на кабелите, скъсан кабел, скъсано или отлепено уплътнение и подобни.<br /><b>Внимание:</b>  Гаранцията на кожените калъфи и кейсовете включва проявени фабрични дефекти като неравности по калъфа или кейса, несъвместимост с посоченото устройство и т.н., но не включва следи от нормална употреба. Като такива спадат скъсана и нарушена част от калъфа или кейса, обелване на покритието на кейса, наранявания и драскотини, пукнатини и подобни.<br /><b>Гаранцията се прекратява, ако:</b>  Електронното устройство се включи в неизправен или неправилно свързан контакт.  ДЦ не носи отговорност при повреда на стоката вследствие форсмажорни обстоятелства (офазяване -включително и по кабелната мрежа, токови удари и др.).  Не се уважават рекламации за повреди, причинени от лош транспорт, неправилно съхранение, за следи от нормална употреба, неизправност или големи колебания в електрическата мрежа, природни бедствия, както и ако са правени опити за отстраняване на повредата от неупълномощени от продавача лица.<br /><b style="color:red">Внимание:  този документ е гаранционна карта. Моля съхранявайте го и при необходимост го предайте на представител на "Ди Си 2008" ООД.</b>
	<br /><br /><b>Закон за защита на потребителите:</b>
<p>
Чл. 112. (1) При несъответствие на потребителската стока с договора за продажба потребителят има право да предяви рекламация, като поиска от продавача да приведе стоката в съответствие с договора за продажба. В този случай потребителят може да избира между извършване на ремонт на стоката или замяната й с нова, освен ако това е невъзможно или избраният от него начин за обезщетение е непропорционален в сравнение с другия.
(2) Смята се, че даден начин за обезщетяване на потребителя е непропорционален, ако неговото използване налага разходи на продавача, които в сравнение с другия начин на обезщетяване са неразумни, като се вземат предвид:
<ul style="float:none; list-style:none; padding-left: 5px;">
<li>1. стойността на потребителската стока, ако нямаше липса на несъответствие;</li>
<li>2. значимостта на несъответствието;</li>
<li>3. възможността да се предложи на потребителя друг начин на обезщетяване, който не е свързан със значителни неудобства за него.</li>
</ul>
</p>
<p>
Чл. 113. (1) (Нова - ДВ, бр. 18 от 2011 г.) Когато потребителската стока не съответства на договора за продажба, продавачът е длъжен да я приведе в съответствие с договора за продажба.<br />
(2) (Предишна ал. 1 - ДВ, бр. 18 от 2011 г.) Привеждането на потребителската стока в съответствие с договора за продажба трябва да се извърши в рамките на един месец, считано от предявяването на рекламацията от потребителя.<br />
(3) (Предишна ал. 2, изм. - ДВ, бр. 18 от 2011 г.) След изтичането на срока по ал. 2 потребителят има право да развали договора и да му бъде възстановена заплатената сума или да иска намаляване на цената на потребителската стока съгласно чл. 114.<br />
(4) (Предишна ал. 3 - ДВ, бр. 18 от 2011 г.) Привеждането на потребителската стока в съответствие с договора за продажба е безплатно за потребителя. Той не дължи разходи за експедиране на потребителската стока или за материали и труд, свързани с ремонта й, и не трябва да понася значителни неудобства.<br />
(5) (Предишна ал. 4 - ДВ, бр. 18 от 2011 г.) Потребителят може да иска и обезщетение за претърпените вследствие на несъответствието вреди.
</p>
<p>
Чл. 114. (1) При несъответствие на потребителската стока с договора за продажба и когато потребителят не е удовлетворен от решаването на рекламацията по чл. 113, той има право на избор между една от следните възможности:<br />
1. разваляне на договора и възстановяване на заплатената от него сума;<br />
2. намаляване на цената.<br />
(2) Потребителят не може да претендира за възстановяване на заплатената сума или за намаляване цената на стоката, когато търговецът се съгласи да бъде извършена замяна на потребителската стока с нова или да се поправи стоката в рамките на един месец от предявяване на рекламацията от потребителя.<br />
(3) Потребителят не може да претендира за разваляне на договора, ако несъответствието на потребителската стока с договора е незначително.<br />
</p>
<p>
Чл. 115. (1) Потребителят може да упражни правото си по този раздел в срок до две години, считано от доставянето на потребителската стока.<br />
(2) Срокът по ал. 1 спира да тече през времето, необходимо за поправката или замяната на потребителската стока или за постигане на споразумение между продавача и потребителя за решаване на спора.<br />
(3) Упражняването на правото на потребителя по ал. 1 не е обвързано с никакъв друг срок за предявяване на иск, различен от срока по ал. 1.<br />
</p>
<p>
Чл. 118. Търговската гаранция се предоставя на потребителя в писмена форма.
</p>
<p>
Чл. 119. (1) Заявлението за предоставяне на търговска гаранция съдържа задължително следната информация:<br />
1. съдържание и обхват на търговската гаранция;<br />
2. съществените елементи, необходими за нейното прилагане, и по-специално: срок на търговската гаранция; териториален обхват; име и адрес на лицето, предоставящо гаранцията.<br />
(2) Информацията по ал. 1 трябва да бъде ясна и разбираема.<br />
</p>
<p>
Чл. 120. В заявлението за предоставяне на търговска гаранция се посочва, че независимо от търговската гаранция продавачът отговаря за липсата на съответствие на потребителската стока с договора за продажба съгласно този закон.
</p>
<p>
Чл. 121. Неспазването на някои от изискванията на чл. 118, 119 и 120 не води до недействителност на търговската гаранция и потребителят може да се позове на нея и да претендира за изпълнение на посоченото в заявлението за предоставяне на търговска гаранция.
</p>
<b>Важно: Рекламация или заявка за гаранция може да бъде предявена тук:</b>
<p>
<ul style="float:none; list-style:none; padding-left: 5px;">
<li>1. В нашия онлайн магазин: http://dice.bg/warranty_users - електронна заявка (препоръчваме този метод, като най-бърз и лесен)</li>
<li>2. По телефон: 0879 437748, 02 981 3136</li>
<li>3. По електронната поща: support@dice.bg</li>
</ul>
</p>';
	$lang["warranty"] = "Гаранция";
	$lang["warranty_regulations"] = "Гаранционни Условия";
	$lang["warranty_months"] = "Гаранция: @months@ месеца";
	$lang["product"] = "Продукт";
	$lang["serial_number"] = "Сериен номер";
	$lang["total"] = "общо";
	$lang["order_date"] = "Поръчано на";
	$lang["order"] = "Поръчка";
	$lang["starts_from"] = "Важи от";
	$lang["until"] = "до";
	$lang["this_product_not_stock"] = "Този продукт не е в наличност";
	$lang["product_search"] = "Tърсене на продукти";
	$lang["price_from"] = "Цена от";
	$lang["to"] = "до";
	$lang["product_type"] = "Тип продукт";
	$lang["all_products"] = "Всички продукти";
	$lang["sort_by"] = "Нареди по";
	$lang["search"] = "Търси";
	$lang["no_results_found"] = "Няма намерени резултати по текущия критерий";
	$lang["results"] = "Резултати";
	$lang["brand"] = "Марка";
	$lang["discount_code"] = "Код за отстъпка";
	$lang["no_links_pls"] = "В коментарите не се допускат линкове";
	$lang["subject"] = "Относно";
	$lang["message"] = "Съобщение";
	$lang["message_sent_succesfully"] = "Вашето съобщение беше изпратено.Очаквайте отговор в най-кратък срок";
	$lang["source"] = "Източник";			
	$lang["title"] = "Заглавие";		
	$lang["choose_answer"] = "Избирам";
	$lang["your_voice_submited_thank_you"] = "Вашият глас е записан, благодарим Ви!";	
	$lang["acc_for_ipad"] = "Apple аксесоари за iPad";		
	$lang["new_products"] = "Нови продукти";
	$lang["call_us"] = "Обадете се";	
	$lang["all"] = "Всички";	
	$lang["payment_method"] = "Начин на плащане";		
	$lang["headphones"] = "Слушалки";
	$lang["shipping_info"] = "Данни за доставка";
	$lang["please_leave_email"] = "Моля оставете мейл, ако желаете да получите номер на товарителницата, чрез която може да проследите своята пратка.";
	$lang["product_number"] = "Продуктов номер (item):";
	$lang["agree_with_tos"] = 'Съгласен съм с <a href="#tos#" target="_blank"><b>Общите</b></a> и <a href="#warranty#" target="_blank"><b>Гаранционни Условия</b></a>';
	$lang["error_agree"] = 'За да продължите трябва да се съгласите с <a href="#agree_with_tos" style="background:none; color:black;">Общите и Гаранционните правила</a>';
	$lang["see_warranty"] = "Вижте гаранционните условия";
	$lang["picture"] = "снимка";
	$lang["was_uploaded"] = "е качена";
	$lang["full_name"] = "Име и Фамилия";
	$lang["warranty_number"] = "Номер на поръчката/гаранция<br /><i>(намира се на гаранционната карта)</i>";
	$lang["warranty_users_msg1"] = 'Преди да продължите с гаранционния модул, моля прочетете <a href="@url@">гаранционните условия</a> на онлайн магазина и вижте дали вашият продукт отговаря на тези условия';
	$lang["warranty_module"] = "Гаранционен модул";
	$lang["warranty_users_msg2"] = "Моля попълнете полетата по-долу. Полетата с червен цвят са задължителни. Колкото повече информация ни дадете, толкова по-бързо ще обработим Вашата заявка и ще се свържем с Вас.";
	$lang["describe_product_problem"] = "Моля опишете какъв е проблемът с вашият продукт - до 500 символа";
	$lang["your_query_was_saved"] = "Вашата заявка, беше приета успешно. Ще се свържем с Вас възможно най-скоро!";
	$lang["you_have_problematic_product"] = "Имате дефектен продукт? Моля изискайте Вашата гаранция";
	$lang["other"] = "Друго";
	$lang["product_not_avail"] = 'Продукта е недостъпен за момента, ако имате въпроси, моля свържете се с <a href="#href#">нас</a>';
	$lang["page"] = "страница";
	$lang["total_price_end_client"] = "Цена за краен клиент";
	$lang["ipad_landing_title"] = "Аксесоари за iPad. Всичко, от което имате нужда";
	$lang["iphone_landing_title"] = "Аксесоари за iPhone. Всичко, от което имате нужда";
	$lang["new_products_title"] = "Нови аксесоари за мобилни телефони, таблети, компютри и други устройства";
	$lang["discount_dont_work_for"] = "Кодът не важи за продукти на промоция или в разпродажба";
	$lang["are_you_sure_remove_discount"] = "Сигурни ли сте че искате да изтриете кода за отстъпка?";
	$lang["consignation_text_first"] = "Днес на @date@, представител на фирма Ди Си 2008 ООД, предаде на @client_name@ в търговски вид и цялостна опаковка следните продукти:<br />";
	$lang["consignation_text_second"] = 'Продуктите се дават на @client_name@ на консигнацията и не са платени, като @client_name@ се задължава да съхранява продуктите описани по-горе с грижата на добър търговец и при поискване от представител на фирма Ди Си 2008 ООД, да ги предаде в състоянието, в което са му били предадени в срок от един работен ден от поискването. За неспазване на срока се дължи обезщетение от 10 % от стойността на продуктите за всеки просрочен ден.<br />
@client_name@ се задължава при продажба на някои от продуктите дадени на консигнация, да ги заплати в 5 (пет) дневен срок след продажбата.<br /><br />
При нарушен търговски вид или липса на опаковка, @client_name@ се задължава да заплати пълната сума на продукта в 7 (седем) дневен срок от установяването на нарушението.<br />
<br /><br />
<table border="0" width=100%>
	<tr>
		<td align="left">Дата: @date@ </td>
		<td align="right">.......................</td>
	</tr>
	<tr>
		<td align="left">.......................</td>
		<td align="right">@client_name@</td>
	</tr>
	<tr>
		<td align="left">Представител на Ди Си 2008 ООД</td>
		<td></td>
	</tr>
</table>
';
	$lang["cart_add_error"] = "Възникна грешка, моля опитайте пак!";
	$lang["continue_shopping"] = "Продължете с пазаруването";
	$lang["see_cart"] = "Вижте кошницата";
	$lang["product_added_cart"] = "Продуктът беше успешно добавен във Вашата кошница";
	$lang["product_already_added_cart"] = "Вече сте добавили този продукт във Вашата кошница";
	$lang["Клавиатура"] = "Клавиатура";
	$lang["Памет"] = "Памет";
	$lang["Твърд диск"] = "Твърд диск";
	$lang["continue_shopping"] = "Продължете с пазаруването";
	$lang["view_cart"] = "Вижте кошницата";
	$lang["config_option_1"] = "Изберете твърд диск";
	$lang["config_option_2"] = "Изберете RAM памет";
	$lang["search_in"] = "Търси в";
	$lang["only_title"] = "Само заглавие";
	$lang["title_n_content"] = "Заглавие и съдържание";
	$lang["only_content"] = "Само съдържание";
	$lang["product_price_updated_cart"] = "Вашият конфигуриран продукт беше успешно добавен в кошницата";
	$lang["comments_and_reviews"] = "Коментари и ревюта";
	$lang["write_review_for"] = "Напишете ревю за ";
	$lang["write_your_review"] = "Дайте Вашето ревю и оценка за ";
	$lang["unit_price"] = "Единична цена";
	$lang["configure"] = "Конфигурирай";
	$lang["use_options_to_configure"] = "Използвайте опциите по-долу за да конфигурирате избраният от Вас Mac компютър по Ваше желание";
	$lang["click_bigger_image"] = "Кликнете тук за по-голямо изображение";
	$lang["add_to_cart"] = "Добави в кошницата";
	$lang["all_by"] = "Всичко на";
	$lang["name"] = "Име";
	$lang["site"] = "Сайт";
	$lang["comment"] = "Коментар";
	$lang["add"] = "добавете";
	$lang["deduct"] = "извадете";
	$lang["my_orders"] = "Моите поръчки";
	$lang["client"] = "Клиент";
	$lang["status"] = "Статус";
	$lang["remove"] = "Премахни";
	$lang["configure_important_message"] = "<b>Важно:</b> Доставката на поръчковите конфигурации отнема до 30 работни дни. Необходимо е предплащане на 50% от стойността на сумата.";
	$lang["contact_us_info"] = "Данни за контакти";
	$lang["configure_your_mac_here"] = "Конфигурирай своя Mac тук";
	$lang["all_apple_products"] = "Всички Apple продукти";
	$lang["search_results"] = "Резултати за ";
	$lang["all_search_products"] = "Всички продукти в dice.bg";
	$lang["comparing_products"] = "Сравняване на продукти:";
	$lang["and"] = "и";
	$lang["review_of"] = "Ревю на ";
	$lang["submitted_code_doesnt_products"] = "Вкараният код не включва продуктите които сте избрали";
	$lang["discount_from_dice_team"] = "отстъпка от Dice.bg";
	$lang["please_invoice_data_cyr"] = "Моля напишете данните за фактура на български език";
	$lang["no_results_for_this_criteria"] = "Няма намерени резултати по текущия критерий";
	$lang["Размер"] = "Размер";
	$lang["another_product_added_cart"] = "Още една бройка от този продукт беше добавена във Вашата кошница";
	$lang["offered_brands"] = "Нашите марки";
	$lang["show"] = "покажи";
	$lang["hide"] = "скрии";
	$lang["final_user_price"] = "Крайно клиентска цена";
	$lang["total_price_dealer"] = "Обща сума за плащане";
	$lang["dealer_price"] = "Дилърска цена";
	$lang["account_name"] = "Акаунт";
	$lang["education_discount"] = "Образование";
	$lang["vat"] = "ДДС";
	$lang["main_error_msg"] = "Възникна грешка, моля опитайте пак!";
	$lang["close_window"] = "Затвори";
	$lang["new_products_description"] = "Калъфи, зарядни устройства и много други аксесоари за Apple, Samsung, HTC, Blackberry и други мобилни устройства";
	$lang["page_views"] = "разглеждания";
	$lang["close"] = "Затвори";	
	$lang["sitemap"] = "Карта на сайта";	
	$lang["rate_this_product"] = "Оценете ";
	$lang["total_rating"] = "Обща оценка";	
	$lang["rate_product_help"] = "(Задръжте мишката върху звездичките, за да оцените продукта, оценява се от 1 до 5, като 1 е най-ниската оценка, а 5 най-голямата)";
	$lang["reviews_ratings_comments_title"] = "Ревюта, оценки и коментари за продукта";
	$lang["attach_pictures"] = "Прикачете снимки";
	$lang["max_size_of_pictures"] = "максимален размер на единична снимка 3MB, можете да прикачите до 5 снимки";	
	$lang["success_rating"] = "Вашата оценка беше отразена. Благодарим Ви!";
	$lang["promo_page_title"] = "Промоционални оферти за мобилни телефони, таблети и лаптопи";
	$lang["unsubscribe"] = "Отпиши";
	$lang["your_mail"] = "Вашият E-mail";
	$lang["your_mail_was_removed"] = "Вашата електронна поща беше премахната от нашият списък";
	$lang["newest_products_for_type"] = "Най-новите @name@";
	$lang["newest_products_for_brand"] = "Нови аксесоари за @name@";	
	$lang["mini_type_meta_title"] = "Аксесоари за @model@ — калъфи, зарядни, кабели и други на топ цени &mdash; Dice.bg";
	$lang["mini_type_meta_description"] = "Качествени аксесоари и калъфи за @model@, протектори, зарядни, резервни батерии и др. от Dice.bg. Безплатна доставка за поръчки над 100лв.";
	$lang["mini_type_h1"] = "Аксесоари за @model@";
	$lang["brand_name_for_mini_type"] = "Аксесоари за @model@";
	$lang["brand_for_model"] = "@brand@ за @model@ на топ цени &mdash; Dice.bg";
	$lang["brand_for_model_gym"] = "@brand@ аксесоари за @model@";
	$lang["brand_for_model_desc"] = "Качествени @brand@ за @model@ от Дигитален Център Dice.bg. Безплатна доставка за поръчки над 100лв.";
	$lang["brand_for_model_desc_gym"] = "Качествени @brand@ аксесоари за @model@ от Дигитален Център Dice.bg. Безплатна доставка за поръчки над 100лв.";
	$lang["most_stock"] = "Най-много бройки";
	$lang["least_stock"] = "Най-малко бройки";
	$lang["newest_products_for_sale"] = "Най-новите продукти добавени в разпродажба";
	$lang["details_description_suffix"] = "от Dice.bg. Актуални цени, безплатна доставка при поръчка над 100лв.";
	$lang["details_title_suffix"] = "Цена — Dice.bg";
	$lang["and_accessories"] = "и аксесоари";
	$lang["title_top_price"] = " на топ цени &mdash; Dice.bg";
	$lang["single_price"] = "Ед. цена";	
	$lang["final_price_without_discount"] = "Крайна цена преди отстъпката";
	$lang["final_price_after_discount"] = "Крайна цена след отстъпката";
	$lang["promoregistrirani_page_title"] = "Специални оферти за регистрирани потребители";
	$lang["must_be_reg_text"] = 'Трябва да бъдете регистрирани потребители на dice.bg за да виждате тези промоции! Моля <a href="@login_url@">влезте</a> във вашият акаунт или се <a href="@reg_url@">регистрирайте</a>';
	$lang["promoregistrirani"] = "Промоции за регистрирани потребители";
	$lang["promoregistrirani_menu"] = "Специални промоции за регистрирани потребители";
	$lang["single_price_end_client"] = "Ед. цена за краен клиент";
	$lang["stocks"] = "Наличности";
	$lang["stockhouse"] = "Склад";
	$lang["orderby"] = "Нареди";
	$lang["price_to"] = "Цена до";
	$lang["from"] = "от";
	$lang["404_page_title"] = "Не съществува такава страница. Грешка 404";
	$lang["paypal_must_pay"] = "За да завършите Вашата поръчка, трябва да платите сумата й чрез PayPal.";
	$lang["add_discount_codes_one_by_one"] = "може да добавяте различни кодове за отстъпки - един по един";
	// $lang["discounted_products"] = "Отстъпката важи за продукти";
	$lang["discounted_products"] = "Отстъпката важи само за продукти, които не са част от разпродажбата";
	$lang["Processing Payment"] = "Processing Payment";
	$lang["Please wait, your order is being processed and you will be redirected to the paypal website"] = "Please wait, your order is being processed and you will be redirected to the paypal website";
	$lang["If you are not automatically redirected to paypal within 5 seconds"] = "If you are not automatically redirected to paypal within 5 seconds";
	$lang["Click Here"] = "Click Here";
	$lang["all_prices_ex_vat"] = "Всички цени са без ДДС";
	$lang["client_price"] = "Клиентска цена";
	$lang["your_discount"] = "Вашата отстъпка";
	$lang["unisex_name"] = "Име на продукта";	
	$lang["firm_city"] = "Град";
	$lang["legal_cookie_text"] = '<h4>Dice.bg запазва информация (бисквитки) на Вашето устройство, както за да осигури по-добро съдържание, така и за статистически данни. <br />Ако сте съгласни, моля натиснете зеления бутон. Тук можете да получите <a href="http://dice.bg/page.php?id=27">повече информация</a> за бисквитките.<br /><div id="accept_legal_cookie" class="button_aggree">Съгласявам се</div></h4>';
	$lang["special_prices_for"] = "Продукти с цени за @account_name@";



?>
