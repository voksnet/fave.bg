<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
//$route['default_controller'] = 'welcome';
//$allowed_ips = array(
//"85.11.140.50", // Tsanko
//"95.42.36.188",
//"46.238.18.97", // Velislav,
//"95.42.9.6" // office
//);
//if (!in_array($_SERVER['REMOTE_ADDR'], $allowed_ips) && $_SERVER['SERVER_NAME'] != "tsanko.voksnet.com") {
//	$route['default_controller'] = 'main/comingsoon';
//	$route['(:any)'] = 'main/comingsoon';
//} else {
$route['default_controller'] = 'main';
$route['sh/(:any)'] = 'main/index/$1';
$route['cat/(:any)'] = 'main/cat/$1';
$route['me'] = 'main/dashboard';
$route['likes'] = 'main/likes';
$route['recent'] = 'main/recent';
$route['help'] = 'pages/index/help';
$route['about'] = 'pages/index/about';
$route['how'] = 'pages/index/how';
$route['contacts'] = 'pages/index/contacts';
$route['faq'] = 'pages/index/faq';
$route['terms'] = 'pages/index/terms';

$route['s/(:any)'] = 'main/search/$1'; 
$route['signup/ml'] = 'users/ml';
$route['signup'] = 'users';
$route['thankyou'] = 'users/thank/b';
$route['mc'] = 'users/ed';
$route['u/(:any)'] = 'profile/index/$1';
$route['u/(:any)/(:any)'] = 'profile/index/$1/$2';
$route['dofollow'] = 'profile/dofollow';
$route['login'] = 'users/login';
$route['logout'] = 'users/login';
$route['v/(:num)'] = 'view/index/$1';
//psl - post slug
$route['v/(:num)/psl/(:any)'] = 'view/index/$1/$2';
$route['v/(:num)/psl'] = 'view/index/$1/$2';
$route['t/ajax'] = 'view/ajax';
$route['up'] = 'upload';
$route['up/(:any)'] = 'upload/$1';
$route['msgs'] = 'notifications/index';
//	$route['up/remote_filesize'] = 'upload/remote_filesize';
//	$route['up/search_tags'] = 'upload/search_tags';

$route['blog'] = 'blog/bloglist/1';
$route['blog/(:any)'] = 'blog/index/$1';
$route['b/(:any)'] = 'blog/index/$1';

$route['crons/(:any)'] = 'crons/index/$1';
$route['scrape/(:any)/(:any)'] = 'scrape/index/$1/$2';
$route['scrape/(:any)/(:any)/(:any)'] = 'scrape/index/$1/$2/$3';
$route['scrape/(:any)'] = 'scrape/index/$1';
$route['404_override'] = '';

$route['sweepstakes'] = 'sweepstakes';

$route['(:any)'] = 'alias/index/$1';
$route['(:any)/wants'] = 'alias/index/$1/wants';
$route['(:any)/catalog'] = 'alias/index/$1/catalog';
$route['(:any)/sponsored'] = 'alias/index/$1/sponsored';
$route['(:any)/likes'] = 'alias/index/$1/likes';
$route['(:any)/followers'] = 'alias/index/$1/followers';
$route['(:any)/following'] = 'alias/index/$1/following';
$route['(:any)/helps'] = 'alias/index/$1/helps';
$route['translate_uri_dashes'] = FALSE;
//}

//reserved
//$route['test/index'] = 'test/index';
//$route['upload'] = 'upload';
