<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Crons_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function update_counts() {
		$this->db->query("UPDATE users AS a LEFT JOIN (SELECT usersFK, COUNT(usersFK) AS count FROM posts GROUP BY usersFK) AS b on a.id = b.usersFK SET a.posts = b.count");

		//update follows
		$this->db->query("UPDATE users AS a LEFT JOIN (SELECT usersFK, COUNT(usersFK) AS count FROM follow GROUP BY usersFK) AS b on a.id = b.usersFK SET a.follows = b.count");

		//update followers
		$this->db->query("UPDATE users AS a LEFT JOIN (SELECT follows_users_selectorFK, COUNT(follows_users_selectorFK) AS count FROM follow GROUP BY follows_users_selectorFK) AS b on a.selector = b.follows_users_selectorFK SET a.followers = b.count");

		//update users tips
		$this->db->query("UPDATE users AS b LEFT JOIN ( SELECT a.usersFK, COUNT(a.id) AS count FROM tips AS a WHERE a.del = '0' GROUP BY a.usersFK ) AS c ON b.id = c.usersFK SET b.tips = c.count");

		$this->db->query("UPDATE tags AS d LEFT JOIN (SELECT c.tag, COUNT(c.id) AS count FROM (SELECT a.tag, b.id FROM tags AS a LEFT JOIN posts AS b ON b.tags LIKE CONCAT('%',a.tag,'%') ) AS c GROUP BY c.tag) AS e ON d.tag = e.tag SET d.posts = e.count");
		$this->db->query("UPDATE categories AS d LEFT JOIN ( SELECT a.id, a.name, if (b.id IS NULL, 0, COUNT(a.name)) AS count FROM categories AS a LEFT JOIN posts AS b ON b.categoriesFK LIKE CONCAT('%',a.id,'%') GROUP BY a.name ) AS e ON d.id = e.id SET d.posts = e.count");
		$this->db->query("UPDATE posts AS b LEFT JOIN ( SELECT a.postsFK, COUNT(a.postsFK) AS count FROM tips AS a WHERE a.del = '0' GROUP BY a.postsFK ) AS c ON b.id = c.postsFK SET b.tips = c.count");
//		UPDATE posts AS b LEFT JOIN ( 
//    SELECT a.postsFK, COUNT(a.postsFK) AS count 
//    FROM tips AS a 
//    WHERE a.del = '0' GROUP BY a.postsFK 
//) AS c ON b.id = c.postsFK 
//SET b.tips = c.count
	}

	public function make_sitemap() {
//		die(print_r($this->config));
		$this->db->select("id, tags, cat_strings");
		$this->db->from("posts");
		$this->db->where('del = "0" AND post_type = "post"');
		$this->db->limit(50000);
		$this->db->order_by("id"); //RAND()
		$query = $this->db->get();
		$result_txt = "";
		$result_html = "";
		$result_xml = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		$count = 0;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$link = base_url() . $this->config->item("my_urlparams_view") . '/' . $row->id;
				$small_link = $link;
				$slug = str_ireplace($row->cat_strings, ",", $row->tags);
				$slug = $row->cat_strings . trim(trim($slug, ","));
				//psl - post slug
				$slug = "psl/" . str_ireplace(array(",", " "), array("_", "-"), trim(trim($slug, ",")));
				$link.="/" . $slug;
				$result_html.='<a href="' . $link . '">' . $small_link . '</a>' . "\n";
				$result_txt.=$link . "\n";
				$link = str_ireplace("&", "&amp;", $link);
				$result_xml.="\n" . '	<url>' . "\n" . '		<loc>' . $link . '</loc>' . "\n" . '	</url>';
				$count++;
			}
		}
		$result_xml.="\n" . '</urlset>';
		file_put_contents(FCPATH . "sitemap.txt", $result_txt);
		file_put_contents(FCPATH . "sitemap.html", $result_html);
		file_put_contents(FCPATH . "sitemap.xml", $result_xml);
		echo ("count " . $count . "\n");
		die("end \n");
	}

	public function base_fix_1() {
		$this->db->select("id");
		$this->db->from("posts");
		$this->db->where("`tags` LIKE '%,koketna,сако,%'");
		$query = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$result[] = $row->id;
			}
		}
		if (count($result) > 0) {
			foreach ($result as $res) {
				$this->db->query("UPDATE tips SET categoriesFK = 46 WHERE postsFK = " . $res);
				$this->db->query("UPDATE posts SET categoriesFK = ',46,' WHERE id = " . $res);
			}
		}
	}

}

?>
