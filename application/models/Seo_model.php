<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Seo_model extends CI_Model {

	private $seo_array = array(
		"site_name" => "",
		"title" => "",
		"description" => "",
		"image" => "",
		"locale" => "",
		"url" => "",
		"image:width" => "",
		"image:height" => "",
	);
	private $other_array = array();

	public function __construct() {
		parent::__construct();
		//$this->controler = $this->router->class;
		$this->seo_array["site_name"] = $this->lang->line("fe_seo_site_name");
		$this->seo_array["description"] = $this->lang->line("fe_seo_site_description");
		$this->seo_array["locale"] = $this->lang->line("fe_seo_site_locale");
		$this->seo_array["url"] = base_url(uri_string());
		//remove slug offer
		if (stristr($this->seo_array["url"], "/psl/")) {
			$temp_url = explode("/psl/", $this->seo_array["url"]);
			$this->seo_array["url"] = $temp_url[0];
		}
		$this->config->load("facebook");
		$this->other_array["app_id"] = $this->config->item('appId');
	}

	public function make_seo($add_data = "", $no_og = false) {
		$result = array();
		
		if (is_array($add_data)) {
//			if (isset($add_data["title"]) && $add_data["title"] != "") {
//				$add_data["title"] . " | " . $this->lang->line("fe_seo_site_name");
//			}
//			die(print_r($add_data));
			foreach ($add_data as $a_k => $a_v) {
				if (isset($this->seo_array[$a_k]) && trim($a_v) != "") {
					$this->seo_array[$a_k] = $a_v;
				}
			}
		}
		$result ["page_title"] = $this->seo_array["title"];
		$result ["page_description"] = $this->seo_array["description"];
		$result ["seo_url"] = $this->seo_array["url"];
		if (!$no_og) {
			$result ["og"] = '<meta property="fb:app_id" content="' . $this->other_array["app_id"] . '" >' . "\n";
			foreach ($this->seo_array as $s_k => $s_v) {
				if ($s_v != "") {
					$result ["og"] .= '		<meta property="og:' . $s_k . '" content="' . $s_v . '" />' . "\n";
				}
			}
		}
		return $result;
	}

}

?>