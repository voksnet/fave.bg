<?php

class Mailer_model extends CI_Model {

	private $mailinstance = null;

	public function __construct() {
		parent::__construct();

		require_once('/var/www/global_includes/swiftmailer/lib/swift_required.php');

		$transport = Swift_SmtpTransport::newInstance('vega.superhosting.bg', 465, "ssl")
				->setUsername('mailer@fave.bg')
				->setPassword('x~X+yH@cIkvA');

		$this->mailinstance = Swift_Mailer::newInstance($transport);
	}

	function internal($params, $bcc = []) {

		if (!$this->mailinstance) {
			return false;
		}

		$loc_url = 'http://ip-api.com/json/' . $_SERVER['REMOTE_ADDR'];
		$ctx = stream_context_create(array('http' =>
			array(
				'timeout' => 5, // seconds
			)
		));
		$country = @json_decode(@file_get_contents($loc_url, false, $ctx));

		$mail_body = '<b>Name</b>: ' . $params->name . '<br>';
		$mail_body .= '<b>E-mail</b>: ' . $params->email . '<br>';
		$mail_body .= '<b>Message</b>:<br>' . nl2br($params->message) . '<br><br>';
		$mail_body .= '----------------------------------------<br>';
		$mail_body .= 'IP address: <a href="' . $loc_url . '">' . $_SERVER['REMOTE_ADDR'] . '</a><br>';
		if ($country) {
			$mail_body .= 'Location: ' . $country->city . ', ' . $country->country . '[' . $country->timezone . ']' . '<br>';
		}
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
			$mail_body .= 'User agent: ' . $_SERVER['HTTP_USER_AGENT'] . '<br>';
		}
		if (isset($_SERVER['HTTP_REFERER'])) {
			$mail_body .= 'Referer page: <a href="' . $_SERVER['HTTP_REFERER'] . '">' . $_SERVER['HTTP_REFERER'] . '</a>';
		}

		try {

			$message = Swift_Message::newInstance($params->subject)
					->setFrom(array($params->email => $params->name))
					->setTo(array("support@fave.bg"))
					->setBody($mail_body)
					->setContentType("text/html");

			if (is_array($bcc) && count($bcc)) {
				foreach ($bcc as $b) {
					$message->setBcc($b);
				}
			}

			$result = $this->mailinstance->send($message);
		} catch (Swift_TransportException $e) {
			$result = $e->getMessage();
		}

		return $result;
	}

	function sendmail($type, $client_email, $data = null) {

		if (!$this->mailinstance) {
			return false;
		}

		$encrypted_email = $client_email;   //$this->encr->encode( $client_email );
		$content = false;
		$subject = "";
		switch ($type) {
			case "passreset":
				$content = $this->load->view("email/password_reset", $data, true);
				$subject = $this->lang->line("fe_reset_pass");
				break;
		}

		if (!$content) {
			return false;
		}
		$email_body = $this->load->view("email/tpl", array("content" => $content), true);

		try {

			$message = Swift_Message::newInstance($subject)
					->setFrom(array(strip_tags($this->config->item("my_emails_support")) . "@fave.bg" => "Fave.bg"))
					->setTo(array($client_email))
					->setBody($email_body)
					->setContentType("text/html")
//                    ->setBcc("bobev@zero.bg")
			;

			$result = $this->mailinstance->send($message);
		} catch (Swift_TransportException $e) {
			$result = $e->getMessage();
		}

		return $result;
	}

}

?>