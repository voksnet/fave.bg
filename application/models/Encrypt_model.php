<?php

class Encrypt_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->library('encrypt');
    }

    function encode($value) {
        $ret = $this->encrypt->encode($value, $this->config->item('encryption_key'));
        $ret = strtr(
                $ret, array(
            '+' => '.',
            '=' => '-',
            '/' => '~'
                )
        );
        return $ret;
    }

    function decode($value) {
        $value = strtr(
                $value, array(
                    '.' => '+',
                    '-' => '=',
                    '~' => '/'
                )
        );

        return $this->encrypt->decode($value, $this->config->item('encryption_key'));
    }

}

?>