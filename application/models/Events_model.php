<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Events_model extends CI_Model {

	public
			$events = array(),
			$anoying = array();

	public function __construct() {
		parent::__construct();

		//this array for now is just describetive for items fileds, no use FOR NOW. Shows what items means for the selected event eg: postsFK -> post id
		$this->events["like"] = array(
			"data" => "",
			"item1" => "postsFK"
		);
		$this->events["wants"] = array(
			"data" => "",
			"item1" => "postsFK",
			"item2" => "categoriesFK"
		);
		$this->events["thanks"] = array(
			"data" => "",
			"item1" => "tipsFK"
		);
		$this->events["tip"] = array(
			"data" => "",
			"item1" => "postsFK",
			"item2" => "categoriesFK",
			"item3" => "tipsFK"
		);
		$this->events["follow_user"] = array(
			"data" => ""
		);
		$this->events["post_follow_user"] = array(
			"data" => "",
			"item1" => "postsFK"
		);



		$this->anoying["like"] = array(//events thet ones seen cant be trigered again to become anoying. Some events like "like" that could be changed several times
			"to_users_selectorFK", //fields that form unique selector for this event
			"from_users_selectorFK",
			"event_type",
			"item1",
		);
		$this->anoying["wants"] = array(
			"to_users_selectorFK",
			"from_users_selectorFK",
			"event_type",
			"item1",
			"item2",
		);
		$this->anoying["follow_user"] = array(
			"to_users_selectorFK",
			"from_users_selectorFK",
			"event_type"
		);
	}

	public function add_event($data) {
		switch ($data["event_type"]) {
			case "like":
				$this->insert_event($data);
				break;
			case "wants":
				$query = $this->db->query("SELECT users_selectorFK FROM posts WHERE id = " . $data["item1"]);
				if ($query->num_rows() > 0) {
					$result = array();
					foreach ($query->result() as $row) {
						$user_selector = $row->users_selectorFK;
					}
					if ($user_selector != $data["from_users_selectorFK"]) {
						$data["to_users_selectorFK"] = $user_selector;
						$this->insert_event($data);
					}
				}
				break;
			case "thanks":
				$this->insert_event($data);
				break;
			case "tip":

				//fnd post owner first
				$query = $this->db->query("SELECT users_selectorFK FROM posts WHERE id = " . $data["item1"]);
				if ($query->num_rows() > 0) {
					$result = array();
					foreach ($query->result() as $row) {
						$owner_selector = $row->users_selectorFK;
					}
					if ($owner_selector != $data["from_users_selectorFK"]) {
						$data["to_users_selectorFK"] = $owner_selector;
						$this->insert_event($data);
					}
				}

				//fnd who wants this cat feom post
				$query = $this->db->query("SELECT users_selectorFK FROM wants WHERE del = '0' AND postsFK = " . $data["item1"] . " AND categoriesFK = " . $data["item2"]);
				if ($query->num_rows() > 0) {
					$result = array();
					foreach ($query->result() as $row) {
						if ($row->users_selectorFK != $data["from_users_selectorFK"] && $owner_selector != $row->users_selectorFK) {
							$data["to_users_selectorFK"] = $row->users_selectorFK;
							$this->insert_event($data);
						}
					}
				}
				break;
			case "follow_user":
				$this->insert_event($data);
				break;
			case "post_follow_user":
				//find followers
				$query = $this->db->query("SELECT users_selectorFK FROM follow WHERE  follows_users_selectorFK = '" . $data["from_users_selectorFK"] . "'");
				if ($query->num_rows() > 0) {
					$result = array();
					foreach ($query->result() as $row) {
						if ($row->users_selectorFK != $data["from_users_selectorFK"]) {
							$data["to_users_selectorFK"] = $row->users_selectorFK;
							$this->insert_event($data);
						}
					}
				}
				break;
			default:
				break;
		}
	}

	private function clean_same_events($data) {
		if (!isset($data["ui"])) {
			$data["ui"] = "";
		}
		if (!isset($data["item1"])) {
			$data["item1"] = "";
		}
		if (!isset($data["item2"])) {
			$data["item2"] = "";
		}
		if (!isset($data["item3"])) {
			$data["item3"] = "";
		}
		if (!isset($data["from_users_selectorFK"])) {
			$data["from_users_selectorFK"] = "";
		}
		if (!isset($data["to_users_selectorFK"])) {
			$data["to_users_selectorFK"] = "";
		}
		$where_string = "";

		if ($data["ui"] != "") {
			$this->db->select("id");
			$this->db->where("ui = '" . $data["ui"] . "' AND (seen = '1' OR emailed = '1') ");
			$this->db->from("events");
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return false;
			}
		}
		foreach ($data as $d_k => $d_v) {
			if ($d_k != "ui") {
				$where_string.=$d_k . " = '" . $d_v . "' AND ";
			}
		}
		$yesterday = strtotime("-1 day");
		$yesterday = date("Y-m-d H-i-s", $yesterday);
		$where_string.=" date > '" . $yesterday . "' ";

		$this->db->select("id");
		$this->db->where($where_string);
		$this->db->from("events");
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$same = array();
			foreach ($query->result() as $row) {
				$same[] = $row->id;
			}
			$where_update = " id = " . implode(" OR id = ", $same);
			$this->db->where($where_update);
			$this->db->set('del', '"1"', FALSE);
			$this->db->update('events');
		}
		return true;
	}

	private function insert_event($data) {
		//if it is anoying make "ui"\
		$data["ui"] = "";
		if (isset($this->anoying[$data["event_type"]])) {
			foreach ( $this->anoying[$data["event_type"]] as $a_f) {
				$data["ui"] .=$data[$a_f];
			}
			$data["ui"] = md5($data["ui"]);
		}
		$go = $this->clean_same_events($data);
		if ($go) {
			$this->db->insert("events", $data);
		}
	}

}

?>