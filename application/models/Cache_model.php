<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Cache_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get($name) {

		$file = FCPATH . "cache/" . $name;
		$file_add = "";

		$cache_content = "";

		if (!file_exists($file)/* || ((time() - filemtime($file)) / 60 >= 15) */) {

			switch ($name) {
				case "cache.home.blog.html":
					$this->db->limit(3);
					$this->db->order_by("submitted", "desc");
					$blog_items = $this->db->get_where("blog", array("del" => "no"));
					$blogs = array();
					$blogs[] = '<li class="ad-space">' . $this->config->item("my_adsense_responsive_unit") . '</li>';
					foreach ($blog_items->result() as $blog) {
						$blogs[] = $this->load->view("blog/home-single", array("blog" => $blog), true);
					}
					shuffle($blogs);
					$cache_content .= implode("", $blogs);
					break;
				case "cache.footer.blog.html":
					$this->db->limit(3);
					$this->db->order_by("submitted", "desc");
					$blog_items = $this->db->get_where("blog", array("del" => "no"));
					$cache_content .= $this->load->view("blog/footer", array("blogs" => $blog_items->result()), true);
					break;
				case "cache.footer.tags.html":
					$cache_content .= $this->tags_model->make_most_cloud();
					break;
				case "cache.cat_menu.html":
				case "cache.cat_menu_mobile.txt":
					$cats = $this->post_model->get_all_cats(true, false, false, true); //get_all_cats($active = true, $description = false, $simple = false, $no_zero = false)
					$cats_array_mobile = array();
					$cats_array = array();
					foreach ($cats as $cat_groups) {
						$temp_arr = array("title" => $cat_groups["group_name"]);
						$cats_array_mobile[] = $temp_arr;
						$cats_array[] = "";
						$cats_array[] = $temp_arr;

						unset($temp_arr);
						if (isset($cat_groups["data"]) && count($cat_groups["data"]) > 0) {
							foreach ($cat_groups["data"] as $cat_item) {
								$temp_arr = array("title" => $cat_item["cat_name"], "url" => $this->config->item("base_url") . "/cat/" . str_ireplace(" ","%20",$cat_item["cat_name"]));
								$cats_array_mobile[] = $temp_arr;
								$cats_array[] = $temp_arr;
							}
						}
					}
					if ($name == "cache.cat_menu_mobile.txt") {
						$cache_content = serialize($cats_array_mobile);
					} else {
						//create table 4 columns, each with 16 rows, ro height 28px, row width no more than 235px
						$rows = 16;
						unset($cats_array[0]);
//						die(print_r($cats_array));
						$cell_count = 0;
						$table = "<table>";
						for ($rw = 1; $rw <= 16; $rw++) {
							$table .= "<tr>";
							for ($cl = 1; $cl <= 4; $cl++) {
								$arr_index = $rw + ($rows * ($cl - 1));
								$cell_class = "";
								$cell_content = "";
								if (!isset($cats_array[$arr_index])) {
									$cell_class = "empty";
								} else {
									if (isset($cats_array[$arr_index]["title"])) {
										if (isset($cats_array[$arr_index]["url"])) {
											$cell_class = "cat";
											$cell_content = '<a href="'.$cats_array[$arr_index]["url"].'">'.$cats_array[$arr_index]["title"].'</a>';
										} else {
											$cell_class = "cat_group";
											$cell_content = $cats_array[$arr_index]["title"];
										}
									} else {
										$cell_class = "skip";
									}
								}
								$table .= '<td class="'.$cell_class.'">'.$cell_content.'</td>';
							}
							$table .= "</tr>";
						}
						$table .= "</table>";
						$cache_content = $table;
					}
					break;
				default:
					$cache_content = "";
			}
			file_put_contents($file, $cache_content);
		}
		return file_get_contents($file);
	}
}

?>