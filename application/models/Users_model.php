<?php

//Here is your client ID
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		//check if logged
		//check if cookie exists and if yes login
		$this->cookie_login();
		$this->is_active_user();
	}

	public function make_avatar($source, $user_sel = "", $orientation = "") {
		if ($orientation == "") {
			$orientation = 1;
		}
//		if ($this->session->userdata('logged_in') && $source != "") {
			$new_avatar = "";
			if ($source == "male" || $source == "female" || $source == "brand") {
				$new_avatar = "user-" . $source . ".jpg";
			} else {
				//generate
				$uid = uniqid();
				$avatar = $uid . ".jpg";
				$folder = substr($uid, -2);
				if (!is_dir(FCPATH . $this->config->item('my_avatars_folder') . $this->config->item('my_avatars_users_folder') . $folder)) {
					mkdir(FCPATH . $this->config->item('my_avatars_folder') . $this->config->item('my_avatars_users_folder') . $folder, 0777, true);
				};
				$dest = $this->config->item('my_avatars_users_folder') . $folder . "/" . $avatar;

				if (base64_decode($source, true) === false) {
					$image = file_get_contents($source);
					if (file_put_contents(FCPATH . $this->config->item('my_avatars_folder') . $dest, $image)) {
						//social link
						$new_avatar = $dest;
					}
				} else {
					$temp_image = FCPATH . $this->config->item("my_temp_upload_folder") . $uid . ".png";
					list($type, $source) = explode(';', $source);
					list(, $source) = explode(',', $source);
					$source = base64_decode($source);

					file_put_contents($temp_image, $source);
					$this->avatarResize($temp_image, FCPATH . $this->config->item('my_avatars_folder') . $dest, $orientation);
					$new_avatar = $dest;
				}
			}
			if ($new_avatar != "") {
				//update avatar 
				if ($this->session->userdata('selector') != null && $user_sel =="") {
					$user_selector = $this->session->userdata('selector');
				} else {
					$user_selector = $user_sel;
				}
				$this->db->where('selector', $user_selector);
				$this->db->update('users', array("avatar" => $new_avatar));
			}
			return true;
//		} else {
//			return false;
//		}
	}

	private function avatarResize($image, $dest, $orientation) {
		$dest_dir = dirname($dest);
		$new_filename = $dest;
		if (!is_dir($dest_dir)) {
			mkdir($dest_dir, 0755, true);
		}

		$config = array();
		$config['image_library'] = 'gd2';
		$config['source_image'] = $image;
		$this->load->library('image_lib', $config);
		if (isset($orientation) && $orientation != 1) {
			switch ($orientation) {
				case 1: // no need to perform any changes
					break;
				case 2: // horizontal flip
					$oris = 'hor';
					break;
				case 3: // 180 rotate left
					$oris = '180';
					break;
				case 4: // vertical flip
					$oris = 'ver';
					break;
				case 5: // vertical flip + 90 rotate right
					$oris = 'ver';
					$oris = '270';
					break;
				case 6: // 90 rotate right
					$oris = '270';
					break;
				case 7: // horizontal flip + 90 rotate right
					$oris = 'hor';
					$oris = '270';
					break;
				case 8: // 90 rotate left
					$oris = '90';
					break;
				default: break;
			}
			$config['rotation_angle'] = $oris;
			$this->image_lib->initialize($config);
			$this->image_lib->rotate();
			$this->image_lib->clear();
		}
		$config['new_image'] = $dest;
		$config['create_thumb'] = FALSE;
		$config['maintain_ratio'] = TRUE;
		$config['width'] = 300;
		$config['height'] = 300;
		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		unlink($image);
	}

	public function get_user_edit($id = "", $selector = "") {
		//$this->db->select();
		if ($id != "") {
			$this->db->where("id=" . $id . " AND del='0'");
		} else {
			$this->db->where("selector='" . $selector . "' AND del='0'");
		}
		$query = $this->db->get("users");
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				$result = (array) $row;
//				$result["avatar"] = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//				if ($result["alias"] == "") {
//					$result["profile_url"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $result["selector"];
//				} else {
//					$result["profile_url"] = base_url() . $result["alias"];
//				}
				unset($result["password"]);
				unset($result["token"]);
				if (isset($result['birthdate'])) {
					$temp_array = explode("-", $result['birthdate']);
					$result['birth_date_year'] = $temp_array[0];
					$result['birth_date_month'] = $temp_array[1];
					$result['birth_date_day'] = $temp_array[2];
					unset($result['birth_date']);
				} else {
					$result['birth_date_year'] = 0;
					$result['birth_date_month'] = 0;
					$result['birth_date_day'] = 0;
				}
				$result["user_name"] = $result["username"];
				$result["email_address"] = $result["email"];
				unset($result["username"]);
				unset($result["email"]);

				return $result;
			}
		}
		return false;
	}

	public function get_short_user($selector) {
		$this->db->select("username, selector, avatar, alias");
		$this->db->where("selector='" . $selector . "' AND del='0'");
		$query = $this->db->get("users");
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				$result = (array) $row;
				$result["avatar"] = base_url() . $this->config->item("my_avatars_folder") . $result["avatar"];
				if ($result["alias"] == "") {
					$result["profile_url"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $result["selector"];
				} else {
					$result["profile_url"] = base_url() . $result["alias"];
				}
				return $result;
			}
		}
		return false;
	}

	public function get_public_user($id = "", $selector = "") {
		//$this->db->select();
		$forbidden_fields = array(
			"fb_id",
			"fb_image",
			"gm_id",
			"gm_image",
			"password",
			"token",
			"email"
		);
		if ($id != "") {
			$this->db->where("id=" . $id . " AND del='0'");
		} else {
			$this->db->where("selector='" . $selector . "' AND del='0'");
		}
		$query = $this->db->get("users");
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				$row = (array) $row;
				foreach ($row as $row_key => $row_value) {
					if (!in_array($row_key, $forbidden_fields)) {
						$result[$row_key] = $row_value;
					}
				}
				if ($result["birthdate"] != "") {
					$from = new DateTime($result["birthdate"]);
					$to = new DateTime('today');
					$age = $from->diff($to)->y;
					$result["age"] = $age;
				} else {
					$result["age"] = "";
				}
				$result["avatar"] = base_url() . $this->config->item("my_avatars_folder") . $result["avatar"];
//				if ($result["avatar"] != "") {
//					$result["avatar"] = base_url() . $this->config->item("my_avatars_folder") . $result["avatar"];
//				} else {
//					$result["avatar"] = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//				}
				if ($result["alias"] == "") {
					$result["profile_url"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $result["selector"];
				} else {
					$result["profile_url"] = base_url() . $result["alias"];
				}
				$result["followed"] = false;
				if ($this->is_logged()) {
					if ($this->is_followed($result["selector"])) {
						$result["followed"] = true;
					}
				}
				$result["followers_data"] = $this->get_follow_short($result["selector"], true);
				if (count($result["followers_data"]) == 0) {
					unset($result["followers_data"]);
				}
				$result["follows_data"] = $this->get_follow_short($result["selector"], false);
				if (count($result["follows_data"]) == 0) {
					unset($result["follows_data"]);
				}
				return $result;
			}
		}
		return false;
	}

	function login($email, $password) {
		$this->db->where(array("email" => $email, "del" => "0", "state" => "1"));
		$query = $this->db->get("users");
		if ($query->num_rows() > 0) {
			if (password_verify($password, $query->result()[0]->password) && $query->result()[0]->no_pass == "0") {
				if ($this->input->post("remember_me") == "on") {
					$remember = true;
				} else {
					$remember = false;
				}
				foreach ($query->result() as $rows) {
					//add all data to session
					$newdata = array(
						'id' => $rows->id,
						'username' => $rows->username,
						'email' => $rows->email,
						'alias' => $rows->alias,
						'selector' => $query->result()[0]->selector,
						'remember' => ($this->input->post("remember_me") == "on") ? true : false,
						'last_feed' => $rows->last_feed,
						'last_feed_check' => $rows->last_feed_check,
						'sex' => $rows->sex,
						'user_type' => $rows->user_type,
						'user_level' => $rows->user_level,
						'brand_name' => $rows->brand_name
					);
				}
				$this->fill_session_data($newdata);
				return true;
			} else if ($query->result()[0]->no_pass == "1") {
				//promt to log with facebook and change password
			} else {
				return false;
			}
		}
		return false;
	}

	function is_active_user() {
		//check last update from session
		if ($this->is_logged()) {
			if ((time() - $this->session->userdata('last_update')) > $this->config->item("my_user_update_time")) {
				$this->db->where(array("del" => "0", "email" => $this->session->userdata('email')));
				$query = $this->db->get("users");
				if ($query->num_rows() > 0) {
					$this->session->set_userdata(
						array('last_update' => time())
					);
				} else {
					$this->user_logout();
				}
			}
		}
	}

	function facebook_connect($fb_data) {
		//we use email as primary info of facebook profile
		$this->db->where(array("email" => $fb_data["email"]));
		$query = $this->db->get("users");

		$image = "https://graph.facebook.com/" . $fb_data["id"] . "/picture?type=large";
		$sex = "";
		if ($fb_data["gender"] == "male") {
			$sex = "m";
		} else if ($fb_data["gender"] == "female") {
			$sex = "f";
		}
		//MYTODO: facebook connect when user changes email and connects again with facebook it registers him
		//again because it check only against email not id. Same goes for gmail
		if ($query->num_rows() > 0) {
			//already exists
			//check id fb_id is there
			if ($query->result()[0]->fb_id != $fb_data["id"] || ($query->result()[0]->sex == "" && isset($sex) )) {
				//update fb_id
				$sql_data["fb_id"] = $fb_data["id"];
				$sql_data["fb_image"] = $image;
				$sql_data["sex"] = $sex;
				$this->db->where('id', $query->result()[0]->id);
				$this->db->update('users', $sql_data);
			}
			//do the login with rememebr option
			//add all data to session
			$newdata = array(
				'id' => $query->result()[0]->id,
				'username' => $query->result()[0]->username,
				'email' => $query->result()[0]->email,
				'alias' => $query->result()[0]->alias,
				'selector' => $query->result()[0]->selector,
				'remember' => true,
				'last_feed' => $query->result()[0]->last_feed,
				'last_feed_check' => $query->result()[0]->last_feed_check,
				'sex' => $query->result()[0]->sex,
				'user_type' => $query->result()[0]->user_type,
				'user_level' => $query->result()[0]->user_level,
				'brand_name' => $query->result()[0]->brand_name
			);

			$this->fill_session_data($newdata);
			//return true;
		} else {
			//register
			$user_data["password"] = password_hash(time() . rand(12, time()) . $this->config->item("my_username_salt") . $fb_data["id"], PASSWORD_DEFAULT);
			$user_data["no_pass"] = "1";
			$user_data["username"] = $fb_data["name"];
			$user_data["sex"] = $sex;
			$user_data["fb_image"] = $image;
			$user_data["email"] = $fb_data["email"];
			$user_data["fb_id"] = $fb_data["id"];
			$user_data['alias'] = "";
			$this->add_user($user_data, true, true);
			//$this->make_avatar($image);
		}
	}

	function gmail_connect($gmail_data) {

		//we use email as primary info of facebook profile
		$this->db->where(array("email" => $gmail_data["email"]));
		$query = $this->db->get("users");
		$image = "";
		if (isset($gmail_data["picture"])) {
			$image = $gmail_data["picture"];
			$image = str_ireplace("/s96-", "/s200-", $image);
		}
		if ($query->num_rows() > 0) {
			//get bigger image
			if ($query->result()[0]->gm_id != $gmail_data["sub"]) {
				//update fb_id
				$sql_data["gm_id"] = $gmail_data["sub"];
				$sql_data["gm_image"] = $image;
				$this->db->where('id', $query->result()[0]->id);
				$this->db->update('users', $sql_data);
			}
			//do the login with rememebr option
			//add all data to session
			$newdata = array(
				'id' => $query->result()[0]->id,
				'username' => $query->result()[0]->username,
				'email' => $query->result()[0]->email,
				'alias' => $query->result()[0]->alias,
				'selector' => $query->result()[0]->selector,
				'remember' => true,
				'last_feed' => $query->result()[0]->last_feed,
				'last_feed_check' => $query->result()[0]->last_feed_check,
				'sex' => $query->result()[0]->sex,
				'user_type' => $query->result()[0]->user_type,
				'user_level' => $query->result()[0]->user_level,
				'brand_name' => $query->result()[0]->brand_name
			);
			$this->fill_session_data($newdata);
			return true;
		} else {
//REGISTER
			$user_data["password"] = password_hash(time() . rand(12, time()) . $this->config->item("my_username_salt") . $gmail_data["sub"], PASSWORD_DEFAULT);
			$user_data["no_pass"] = "1";
			$user_data["username"] = $gmail_data["name"];
			$user_data["email"] = $gmail_data["email"];
			$user_data["gm_id"] = $gmail_data["sub"];
			$user_data['alias'] = "";
			$user_data["gm_image"] = $image;
			if ($this->add_user($user_data, true, true)) {
//			if ($image != "") {
//				$this->make_avatar($image);
//			}
				return true;
			}
		}
	}

	private function get_max_selector_number() {
		$this->db->select("value");
		$this->db->where("key='selector_max_number'");
		$this->db->from("val_holder");
		$query = $this->db->get();
		$result = $query->result()[0]->value;
		return (integer) $result;
	}

	public function add_user($user_data, $remember = false, $ajax = false) {
		$max_selector_value = $this->get_max_selector_number();
		$max_selector_value = $max_selector_value - (rand(10, 100));
		$user_data["selector"] = base_convert($max_selector_value, 10, 36);

		$this->db->where("key='selector_max_number'");
		$this->db->update('val_holder', array("value" => $max_selector_value));
		if (is_array($user_data) && count($user_data) > 3) {
			$data = $user_data;
			if ($user_data['sex'] == "b") {
				$data["state"] = "0";
			}
			$this->db->insert('users', $data);
			$user_id = $this->db->insert_id();
			$this->db->insert('events_settings', array("usersFK" => $user_id, "users_selectorFK" => $user_data["selector"]));
		}
		$user_data["last_ip"] = $this->input->server('REMOTE_ADDR');
		//user is logged
		if (!isset($user_data['sex']) || $user_data['sex'] != "b") {
			if (!isset($user_data['sex'])) {
				$user_data['sex'] = "";
			}
			$newdata = array(
				'id' => $user_id,
				'username' => $user_data['username'],
				'email' => $user_data['email'],
				'alias' => "",
				'remember' => $remember,
				'selector' => $user_data["selector"],
				'sex' => $user_data['sex'],
				'user_type' => "user",
				'user_level' => "",
				'brand_name' => ""
			);
			$this->fill_session_data($newdata);
		}
		if (isset($user_data["fb_image"]) && $user_data["fb_image"] != "") {
			$this->make_avatar($user_data["fb_image"]);
		} else if (isset($user_data["gm_image"]) && $user_data["gm_image"] != "") {
			$this->make_avatar($user_data["gm_image"]);
		} else {

			$source = "female";
			switch ($user_data['sex']) {
				case "f":
					$source = "female";
					break;
				case "m":
					$source = "male";
					break;
				case "b":
					$source = "brand";
					break;
			}
			$this->make_avatar($source, $user_data["selector"]);
		}

		$this->session->set_flashdata("add_user", "complete");

		//redirect to profile url
		if (!$ajax) {
			if ($user_data['sex'] == "b") {
				redirect(base_url() . "thankyou");
			} else {
				redirect($this->session->userdata(base_url()));
			}
		} else {
			return true;
		}
	}

	public function edit_user($user_data) {
		if (is_array($user_data) && count($user_data) > 3) {
			if (trim($user_data["avatar"]) != "") {
				if ($this->make_avatar($user_data["avatar"], "", $user_data["avatar_orientation"])) {
					//delete old avatar if exists
					if (file_exists(FCPATH . $this->config->item('my_avatars_folder') . $user_data["old_avatar"])) {
						if (!stristr($user_data["old_avatar"], "male")) {
							unlink(FCPATH . $this->config->item('my_avatars_folder') . $user_data["old_avatar"]);
						}
					}
				}
			}
			unset($user_data["avatar"]);
			unset($user_data["old_avatar"]);
			unset($user_data["avatar_orientation"]);
			if (trim($user_data["password"]) == "") {
				unset($user_data["password"]);
			}
			$this->db->where('selector', $this->session->userdata("selector"));
			$this->db->update('users', $user_data);
			$this->db->where('selector', $this->session->userdata("selector"));
			$query = $this->db->get("users");
			$rememeber = $this->session->userdata("remember");
			if ($query->num_rows() > 0) {
				$newdata = array(
					'id' => $query->result()[0]->id,
					'username' => $query->result()[0]->username,
					'email' => $query->result()[0]->email,
					'alias' => $query->result()[0]->alias,
					'selector' => $query->result()[0]->selector,
					'remember' => $rememeber,
					'last_feed' => $query->result()[0]->last_feed,
					'last_feed_check' => $query->result()[0]->last_feed_check,
					'sex' => $query->result()[0]->sex,
					'user_type' => $query->result()[0]->user_type,
					'user_level' => $query->result()[0]->user_level,
					'brand_name' => $query->result()[0]->brand_name
				);
				$this->fill_session_data($newdata);
			}
		}
	}

	public function update_user($user_data, $remember = false) {
		$max_selector_value = $this->get_max_selector_number();
		$max_selector_value = $max_selector_value - (rand(10, 100));
		$user_data["selector"] = base_convert($max_selector_value, 10, 36);

		$this->db->where("key='selector_max_number'");
		$this->db->update('val_holder', array("value" => $max_selector_value));

		if (is_array($user_data) && count($user_data) > 3) {
			$data = $user_data;
			$this->db->insert('users', $data);
		}
		$user_data["last_ip"] = $this->input->server('REMOTE_ADDR');
		//user is logged
		$newdata = array(
			'id' => $this->db->insert_id(),
			'username' => $user_data['username'],
			'email' => $user_data['email'],
			'alias' => $user_data['alias'],
			'remember' => $remember,
			'selector' => $user_data["selector"],
			'last_feed' => $user_data["last_feed"],
			'last_feed_check' => $user_data["last_feed_check"],
			'sex' => $user_data["sex"],
			'user_type' => $user_data["user_type"],
			'user_level' => $user_data["user_level"],
			'brand_name' => $user_data["brand_name"]
		);
		$this->fill_session_data($newdata);
	}

	public function is_logged($redirect = false, $destination = "") {
		if ($this->session->userdata('logged_in')) {
			if ($redirect) {
				if ($destination == "") {
					redirect('/', 'refresh');
				} else {
					redirect($destination, 'refresh');
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public function not_logged($redirect = false, $destination = "") {
		if (!$this->session->userdata('logged_in')) {
			if ($redirect) {
				if ($destination == "") {
					redirect('/', 'refresh');
				} else {
					redirect($destination, 'refresh');
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public function cookie_login() {
		if (!$this->is_logged()) {
			if (!is_null(get_cookie("remember_me"))) {
				//log by cookie
				$cookie_data = explode(":", get_cookie("remember_me"));
				if (count($cookie_data) == 2) {
					$valid = $this->validate_token($cookie_data[0], $cookie_data[1]);
				} else {
					$this->user_logout();
				}
			}
		}
	}

	public function user_logout() {
		$newdata = array(
			'user_id' => '',
			'user_name' => '',
			'user_email' => '',
			'logged_in' => FALSE,
		);
		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		delete_cookie("remember_me");
//		if ($this->session->userdata("last_url") != null && $this->session->userdata("last_url") != "") {
//			redirect($this->session->userdata("last_url"));
//		} else {
		redirect('/', 'refresh');
//		}
	}

	private function generate_token($selector, $email, $expire) {
		$token = md5($selector . $this->config->item("my_username_salt") . $expire);
		$validator = md5($token . $expire . $this->config->item("my_username_salt") . $selector);
		return (array("token" => $token, "validator" => $validator));
	}

	private function fill_session_data($data) {
		$newdata = $data;
		$newdata['logged_in'] = true;
		$newdata['last_update'] = time();
		if (isset($data["last_feed_check"]) && $data["last_feed_check"] != "") {
			$newdata['last_feed_check'] = $data["last_feed_check"];
		}
		if (isset($data["last_feed"]) && $data["last_feed"] != "") {
			$newdata['last_feed'] = $data["last_feed"];
		}
		if ($data['alias'] == "") {
			$newdata["profile_url"] = base_url() . $this->config->item("my_urlparams_user") . "/" . $data["selector"];
		} else {
			$newdata["profile_url"] = base_url() . $data["alias"];
		}
		$this->session->set_userdata($newdata);
		$sql_data = array(
			'ut' => date("Y-m-d H:i:s"),
			'last_ip' => $this->input->server('REMOTE_ADDR')
		);
		if (isset($data['remember']) && $data['remember']) {
			if (!isset($data['selector']) || trim($data['selector']) == "") {
				//get selector
				$this->db->select('selector');
				$this->db->where("id", $data['id']);
				$query = $this->db->get("users");
				$query->result()[0]->selector;
				$data['selector'] = $query->result()[0]->selector;
			}

			//generate token
			$expire = time() + ($this->config->item("my_login_remember") * 24 * 60 * 60);

			$token_data = $this->generate_token($data['selector'], $data['email'], $expire);
			//destroy id cookie if exists just in case
			if (!is_null(get_cookie("remember_me"))) {
				delete_cookie("remember_me");
			}
			set_cookie("remember_me", $data['selector'] . ":" . $token_data['token'], $expire - time());

			$sql_data["token"] = $token_data['validator'];
			$sql_data["expires"] = $expire;
		}
		$this->db->where('id', $data['id']);
		$this->db->update('users', $sql_data);
	}

	private function validate_token($selector, $token) {
		$this->db->where("selector", $selector);
		$query = $this->db->get("users");
		if ($query->num_rows() > 0) {
			//validate
			$validator = md5($token . $query->result()[0]->expires . $this->config->item("my_username_salt") . $selector);
			if ($query->result()[0]->token == $validator) {
				//login
				$data = array(
					'id' => $query->result()[0]->id,
					'username' => $query->result()[0]->username,
					'email' => $query->result()[0]->email,
					'alias' => $query->result()[0]->alias,
					'selector' => $query->result()[0]->selector,
					'remember' => true,
					'last_feed' => $query->result()[0]->last_feed,
					'last_feed_check' => $query->result()[0]->last_feed_check,
					'brand_name' => $query->result()[0]->brand_name,
					'sex' => $query->result()[0]->sex,
					'user_type' => $query->result()[0]->user_type,
					'user_level' => $query->result()[0]->user_level
				);
				$this->fill_session_data($data);
			}
		} else {
			$this->user_logout();
		}
	}

	public function followed_users($selector, $just_count = false) {
		if (!$just_count) {
			$select_string = 'follows_users_selectorFK ';
		} else {
			$select_string = ' COUNT(follows_users_selectorFK) AS count ';
		}
		$this->db->select($select_string);
		$this->db->from('follow');
		$where_string = "users_selectorFK = '" . $selector . "' ";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				if (!$just_count) {
					$result[] = $row->users_selectorFK;
				} else {
					$result["count"] = $row->count;
				}
			}
			return $result;
		}
		return false;
	}

	public function user_followers($selector, $just_count = false, $page = 0, $limit = 0) {
		if (!$just_count) {
			$select_string = 'a.users_selectorFK ';
		} else {
			$select_string = ' COUNT(a.users_selectorFK) AS count ';
		}
		$this->db->select($select_string);
		$this->db->from('follow AS a');
		$where_string = " a.follows_users_selectorFK = '" . $selector . "'";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			$result = array();
			foreach ($query->result() as $row) {
				if (!$just_count) {
					$result[] = $row->users_selectorFK;
				} else {
					$result["count"] = $row->count;
				}
			}
			return $result;
		}
		return false;
	}

	private function is_followed($selector) {
		if ($this->session->userdata('logged_in')) {
			$select_string = 'follows_users_selectorFK';
			$this->db->select($select_string);
			$this->db->from('follow');
			$where_string = "users_selectorFK = '" . $this->session->userdata('selector') . "' AND follows_users_selectorFK='" . $selector . "'";
			$this->db->where($where_string);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function add_follow($selector) {
		if ($this->session->userdata('logged_in') && $this->session->userdata('selector') != $selector) {
			if (!$this->is_followed($selector)) {

				$result = $this->followed_users($this->session->userdata('selector'), true);
				$sql_data = array("usersFK" => $this->session->userdata('id'), "users_selectorFK" => $this->session->userdata('selector'), "follows_users_selectorFK" => $selector);
				$this->db->insert('follow', $sql_data);



				$this->load->model('events_model');
				$event_array = array();
				$event_array["from_users_selectorFK"] = $this->session->userdata('selector');
				$event_array["to_users_selectorFK"] = $selector;
				$event_array["event_type"] = "follow_user";
				$event_array["user_type"] = "user";
				$this->events_model->add_event($event_array);




				//increase following in users
				$this->db->where('id', $this->session->userdata('id'));
				$this->db->set('follows', 'follows +1', FALSE);
				$this->db->update('users');

				//increase followers for followed user
				$this->db->where('selector', $selector);
				$this->db->set('followers', 'followers +1', FALSE);
				$this->db->update('users');

				$this->update_user_rating("", $selector);
				return $result["count"] + 1;
			}
		}
	}

	public function remove_follow($selector) {
		if ($this->session->userdata('logged_in')) {
			if ($this->is_followed($selector)) {

				$result = $this->followed_users($this->session->userdata('selector'), true);

				$this->db->where('usersFK=' . $this->session->userdata('id') . " AND follows_users_selectorFK='" . $selector . "'");
				$this->db->delete('follow');

				//decrease followers in users
				$this->db->where('id', $this->session->userdata('id'));
				$this->db->set('follows', 'follows - 1', FALSE);
				$this->db->update('users');

				//decrease followers for followed user
				$this->db->where('selector', $selector);
				$this->db->set('followers', 'followers - 1', FALSE);
				$this->db->update('users');

				//$this->update_user_rating("", $selector);
				return $result["count"] - 1;
			}
		}
	}

	public function toggle_follow($selector) {
		$result = array();
		$result["selector"] = $selector;
		if ($this->is_followed($selector)) {
			$result["type"] = "remove";
			$result["value"] = $this->remove_follow($selector);
		} else {
			$result["type"] = "add";
			$result["value"] = $this->add_follow($selector);
		}
		return $result;
	}

	public function alias_to_selector($alias) {
		$select_string = 'selector, sex';
		$this->db->select($select_string);
		$this->db->from('users');
		$where_string = "alias = '" . $alias . "' AND del='0'";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return (array) $row;
			}
		} else {
			return false;
		}
	}
	
	public function selector_sex($selector) {
		$select_string = 'sex';
		$this->db->select($select_string);
		$this->db->from('users');
		$where_string = "selector = '" . $selector . "' AND del='0'";
		$this->db->where($where_string);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return (array) $row;
			}
		} else {
			return false;
		}
	}

	public function update_user_rating($user_id = "", $selector = "") {
		if ($selector == "") {
			$where = " WHERE `id` = " . $user_id;
		} else {
			$where = " WHERE `selector` = '" . $selector . "'";
		}

//		$query = "UPDATE `users` SET `rating` = IFNULL((`post_views`/`posts`),0) " .$where;
		$query = "UPDATE `users` SET `rating` = tips+(tips_thanks * " . $this->config->item("my_rating_thnx_increaser") . ")" . $where;
		$query_result = $this->db->query($query);
	}

	private function get_follow_short($selector, $followers = false) {
		$this->db->select("b.id, b.selector, b.alias, b.avatar, b.username");
		$this->db->from('follow AS a');
		if ($followers) {
			$where_string = "a.follows_users_selectorFK = '" . $selector . "'";
			$this->db->join('users AS b', 'a.users_selectorFK = b.selector');
		} else {
			$where_string = "a.users_selectorFK = '" . $selector . "'";
			$this->db->join('users AS b', 'a.follows_users_selectorFK = b.selector');
		}
		$this->db->where($where_string);
		$this->db->limit(4);
		$query = $this->db->get();
		$result = array();
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$users[] = $row->selector;
				$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//				if ($row->avatar != "") {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//				} else {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//				}
				if ($row->alias == "") {
					$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->selector;
				} else if ($row->selector != "") {
					$row->profile_url = base_url() . $row->alias;
				}
				$result[$row->selector] = (array) $row;
			}
		}


		return $result;
	}

	public function get_selector_by_id($id) {
		$query = $this->db->query("SELECT selector FROM users WHERE id = " . $id . " AND del = '0' ");
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return $row->selector;
			}
		}
		return "";
	}

	public function get_id_by_selector($selector) {
		$query = $this->db->query("SELECT id FROM users WHERE selector = '" . $selector . "' AND del = '0' ");
//		die($this->db->last_query());
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				return $row->id;
			}
		}
		return "";
	}

	private function get_follow($selector, $followers = false, $page = 0, $limit = 0) {
		if ($limit == 0) {
			$limit = $this->config->item("my_followers_per_page");
		}
		$page_offset = ($page * $limit);
		$this->db->select("b.id, b.selector, b.alias, b.rating, b.avatar, b.username, b.followers, b.follows");
		$this->db->from('follow AS a');
		if ($followers) {
			$where_string = "a.follows_users_selectorFK = '" . $selector . "'";
			$this->db->join('users AS b', 'a.users_selectorFK = b.selector');
		} else {
			$where_string = "a.users_selectorFK = '" . $selector . "'";
			$this->db->join('users AS b', 'a.follows_users_selectorFK = b.selector');
		}
		$this->db->where($where_string);
		$this->db->limit($limit + 1, $page_offset);
		$query = $this->db->get();
//		die($this->db->last_query());
		$result = array();
		$users = array();
		$following_all = false;
		if ($query->num_rows() > 0) {
			foreach ($query->result() as $row) {
				$users[] = $row->selector;
				$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//				if ($row->avatar != "") {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . $row->avatar;
//				} else {
//					$row->avatar = base_url() . $this->config->item("my_avatars_folder") . "user-female.jpg";
//				}
				if ($row->alias == "") {
					$row->profile_url = base_url() . $this->config->item("my_urlparams_user") . "/" . $row->selector;
				} else if ($row->selector != "") {
					$row->profile_url = base_url() . $row->alias;
				}
				$result[$row->selector] = (array) $row;
				if ($this->session->userdata("selector") != null && $selector == $this->session->userdata("selector") && !$followers) {
					$following_all = true;
					$result[$row->selector]["following"] = true;
				}
			}
//			die(print_r($result));
		}

		if (!$following_all) {
			if ($this->session->userdata("selector") != null) {
				if ($selector != $this->session->userdata("selector") || ($selector == $this->session->userdata("selector") && $followers)) {
					$own_selector = $this->session->userdata("selector");
					$where = "WHERE users_selectorFK = '" . $own_selector . "' AND (follows_users_selectorFK = '" . implode("' OR follows_users_selectorFK='", $users) . "' )";
					$query_string = "SELECT users_selectorFK AS selector FROM `follow` " . $where;

					$query = $this->db->query($query_string);
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $row) {
							$follow_array[$row->selector] = $row->selector;
						}
					}
				}
			}

			foreach ($result as $usr) {
				if (isset($follow_array[$usr["selector"]])) {
					$result[$usr["selector"]]["following"] = true;
				} else {
					$result[$usr["selector"]]["following"] = false;
				}
			}
		}


		if (count($users) > 0) {
			$test = array();
			foreach ($users as $user) {
				$query_string = "
SELECT a.* FROM
	(
		SELECT id, users_selectorFK AS selector, image,
			@currcount := IF( @currvalue = users_selectorFK, @currcount + 1, 1) AS row,
			@currvalue := users_selectorFK AS whatever
		FROM posts
		WHERE  del = '0' AND post_type = 'post' AND users_selectorFK ='" . $user . "'
		ORDER BY users_selectorFK DESC, id DESC
	) AS a
WHERE a.row <=7";
				$this->db->trans_start();
				$this->db->query("SET @currcount = NULL, @currvalue = NULL;");
				$query = $this->db->query($query_string);
//			die($this->db->last_query());
				$this->db->trans_rollback();
				$this->db->trans_complete();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
						unset($row->row);
						unset($row->whatever);
						$row->image = substr($row->image, 0, 2) . "/" . $row->image;
						$row->link = base_url() . $this->config->item("my_urlparams_view") . "/" . $row->id;
						$row->target = '';
						$result[$row->selector]["posts"][] = (array) $row;
					}
				}
			}
		}
		return $result;
	}

	public function alias_profile($selector, $type) {
		if (!$selector) {
			show_404();
		} else {
			$user_data = $this->users_model->get_public_user("", $selector);
			if (!$user_data) {
				show_404();
			} else {
				if ($this->session->userdata("logged_in") != null) {
					$data["current_user_selector"] = $this->session->userdata("selector");
				}
				$data["info"] = $user_data;
				$data["info"]["has_sponsored"] = false;
				if ($this->session->userdata("sex") == "b" && stristr($this->session->userdata("user_level"), ",sponsored,") && $this->session->userdata("selector") == $selector || ($this->session->userdata("user_type") == "admin")) {
					$data["info"]["has_sponsored"] = true;
				}
				$data["selector"] = $selector;
				$data["unique_id"] = uniqid();

				$data["page_type"] = "main";
//				$input_data["explore"] = "";
//				$input_data["search"] = "";
//				$input_data["exclude"] = "";
//				$input_data["category"] = "";
				$input_data["profile"] = $selector;
				$data["profile"] = $selector;
				$input_data["ui"] = $data["unique_id"];
				$data["page_sub_type"] = "";
				if ($type == "") {
					$type = "wants";
				}
				if ($type == "wants") { // || ($type != "followers" && $type != "following" && $type != "helps"  && $type != "sponsored")
					$data["page_sub_type"] = "wants";
				}
				if ($type == "catalog") {
					$data["page_sub_type"] = "catalog";
					$input_data["catalog"] = true;
				}

				if ($type == "likes") {
					$input_data["likes"] = true;
					$data["page_sub_type"] = "likes";
				}
				if ($type == "sponsored") {
					$data["page_sub_type"] = "sponsored";
					$input_data["own_sponsored"] = true;
				}
//				$input_data["my_likes"] = false;
//				$input_data["my_feed"] = false;
//				$input_data["button"] = false;
				$result = $this->post_model->get_msnry($input_data);
				$data["data"] = $result;
				$title = $data["info"]["username"];
				$description = sprintf($this->lang->line("fe_seo_site_user_description"), $data["info"]["username"], $this->lang->line("fe_seo_site_name"));
				if ($data["info"]["about"] != "") {
					if (mb_strlen($data["info"]["about"], "utf8") > 156) {
						$description = mb_substr(strip_tags($data["info"]["about"]), 0, 156, "utf8") . " ... ";
					} else {
						$description = strip_tags($data["info"]["about"]);
					}
				}
				$data_seo = array(
					"image" => $data["info"]["avatar"],
					"title" => $title,
					"description" => $description,
					"image:width" => 300,
					"image:height" => 300
				);
				$header_data["menu"] = $this->menu_seo_model->menu_seo["menu"];
				$seo = $this->seo_model->make_seo($data_seo);
				$header_data = array_merge($header_data, $seo);
				$this->load->view('main_templates/header', $header_data);

				$this->load->view('index_content');
				$this->load->view('users/profile/profile', $data);

				if ($type == "wants" || $type == "likes" || $type == "catalog" || $type == "sponsored") {
					$data["no_header_title"] = true;
					$this->load->view('posts/post_content', $data);
					$this->load->view('main_templates/masonry', $data);
				} else if ($type == "following" || $type == "followers") {
					$result = array();
					$header_title = "";
					if ($type == "followers") {
						$header_title = $this->lang->line("fe_word_followers_header");
						$result = $this->get_follow($selector, true);
					} else {
						$header_title = $this->lang->line("fe_word_follows_header");
						$result = $this->get_follow($selector, false);
					}
					$result_html = "";
					if (count($result) > 0) {
						$result_html = $this->make_followers($result);
					}
					$this->load->view('users/profile/followers', array("header_title" => $header_title, "follow" => $result_html));
				}
				$this->load->view('main_templates/footer');
				$this->load->view('main_templates/common_footer');
			}
		}
	}

	private function make_followers($data) {
//		die(print_r($data));
		$result_html = "";
		foreach ($data as $item) {
			$follow_class = ""; //followed
			if ($item["following"]) {
				$follow_class = " followed";
			}
			$follow_button = "";
			if ($this->session->userdata("selector") == null || $this->session->userdata("selector") != $item["selector"]) {
				$follow_button = '<div class="follower_button">
						<div class="follow_follower' . $follow_class . '" id ="follow_' . $item["selector"] . '" data-follow_selector ="' . $item["selector"] . '"><span class="follow">' . $this->lang->line("fe_word_you_follow") . '</span><span class="is_followed">' . $this->lang->line("fe_word_you_follows") . '</span></div>
					</div>';
			}
			$result_html .=
				'			<div class="follower_row">
				<div class="profile_avatar">
					<a href="' . $item["profile_url"] . '" title="' . $item["username"] . '"><img src="' . $item["avatar"] . '" alt="' . $item["username"] . '"></a>
				</div>
				<div class="follower_data">
					<div class="follower_name"><a href="' . $item["profile_url"] . '">' . $item["username"] . '</a></div>
					<div class="follower_followers">' . $this->lang->line("fe_word_followers") . ': <strong>' . $item["followers"] . '</strong></div>
					<div class="follower_rating">' . $this->lang->line("fe_word_rating") . ': <strong>' . $item["follows"] . '</strong></div>
					' . $follow_button . '
				</div>';
			if (isset($item["posts"])) {
				$result_html .=
					'				<div class="follower_posts">';
				foreach ($item["posts"] as $a_post) {
					$result_html .=
						'					<div class="follower_post"><a href="' . $a_post["link"] . '"' . $a_post["target"] . '><img src="' . base_url() . $this->config->item("my_upload_small_images") . "/" . $a_post["image"] . '"></a></div>';
				}
				$result_html .=
					'				</div>';
			}
			$result_html .=
				'				<div class="clear"></div>
			</div>';
		}
		return $result_html;
	}

	public function access($item, $action, $params = "") {
		if ($this->session->userdata('logged_in') != null && $this->session->userdata('logged_in')) {
			switch ($item) {
				case "tips":
					if ($this->session->userdata("user_type") == "admin" || $this->session->userdata("user_type") == "moderator") {
						return true;
					} else {
						if ($action == "delete") {
							if (isset($params["is_own"]) && $params["is_own"]) {
								$created_before_min = round((time() / 60) - strtotime($params["date"]) / 60);
								if ($created_before_min < $this->config->item("my_time_to_delete_tip")) {
									return true;
								}
							}
						}
					}
					return false;
					//if ( ($row->is_own && $created_before_min < $this->config->item("my_time_to_delete_tip")) || $this->session->userdata("user_type") == "admin" || $this->session->userdata("user_type") == "moderator" ) {
					break;
				case "posts":
					if ($this->session->userdata("user_type") == "admin" || $this->session->userdata("user_type") == "moderator") {
						return true;
					} else {
						if ($action == "delete") {
//							die(print_r( $params ));
							if ($params["user_selector"] == $this->session->userdata("selector")) {
								$created_before_min = round((time() / 60) - strtotime($params["date"]) / 60);
								if ($created_before_min < $this->config->item("my_time_to_delete_tip")) {
									return true;
								}
							}
						}
					}
					return false;
					//if ( ($row->is_own && $created_before_min < $this->config->item("my_time_to_delete_tip")) || $this->session->userdata("user_type") == "admin" || $this->session->userdata("user_type") == "moderator" ) {
					break;
			}
		}
		return false;
	}

	function user_password_reset_init($email = '') {
		$email = trim($email);
		$user = $this->db->get_where("users", array("email" => $email, "no_pass" => '0', "del" => '0'));
//        echo $email.$this->db->last_query()."<pre>";print_r($user->result());die;
		if (!$user->num_rows()) {
			return true;
		}
		$user = $user->row();

		$this->load->model("Mailer_model", "mailer");
		$this->load->model("Encrypt_model", "encr");

		$expires = date("Y-m-d H:i:s", strtotime("NOW +24 HOURS"));
		$qry = "INSERT INTO user_reset (usersFK, email, expires) VALUES (?, ?, ?) 
                ON DUPLICATE KEY UPDATE email=?, expires=?, del=?";
		$this->db->query($qry, array($user->id, $user->email, $expires, $user->email, $expires, 'no'));

		$result = $this->mailer->sendmail("passreset", $email, array('reset_link' => $this->encr->encode($user->email)));
		if (!is_int($result)) {
			return true;
		}

		return false;
	}

	function user_password_reset($userFK, $email, $password) {
		$user_data = array(
			"password" => $password
		);
		$user_data["password"] = password_hash($user_data["password"], PASSWORD_DEFAULT);

		$this->db->where("id", $userFK);
		$this->db->where("email", $email);
		$this->db->update("users", $user_data);

		$this->db->where("usersFK", $userFK);
		$this->db->where("email", $email);
		$this->db->update("user_reset", array("del" => "yes"));

		return true;
	}

}

?>
